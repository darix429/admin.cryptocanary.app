<?php
    require_once "../../config.php";
    $entity_id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
    $db = Database::getInstance()->getConnection();

    $getEntities = $db->prepare("SELECT entity_id, entity_name, symbol FROM entity");
    $getEntities->execute();
    $entities = $getEntities->fetchAll();
    if (!count($entities) > 0) {
        echo "<br>No entities";
        die();
    }
    $searchOptions  = "";
    foreach($entities as $entity) {
        $val = $entity["entity_id"]."-".$entity["entity_name"]."-".$entity["symbol"];
        $searchOptions .= "<option value='$val'>";
    }
    
    if(!isset($entity_id)) {
        $ENTITY = $entities[0];
    } else {
        $getEntity = $db->prepare("SELECT * FROM entity WHERE entity_id = $entity_id");
        $getEntity->execute();
        $ENTITY = $getEntity->fetch();
    }

    if (!isset($ENTITY)) {
        echo "<br><div class='container'>Entity $entity_id not found</div>";
        die();
    }


?><html>
<head>
    <title>Calculator</title>
    <script type="text/javascript"	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        html, body { margin: 0px; padding: 0px; font-family: 'calibri';}
        .container {width: 90vw; margin: 10px auto;}
        table {border: solid 1px; width: 90vw; margin: 10px auto; table-layout: fixed;}
        table th {background: #ddd; text-align: left;}
        table tr:nth-child(odd) {
            background: #f0f0f0;
        }
        table td input{
            width: 50px;
        }
        textarea:disabled { resize: none;}
    </style>
</head>
<body>

<div class="container" style="padding: 10px 0px">
    <datalist id="entities">
        <?php echo $searchOptions ?>
    </datalist>
    Search entity: <input list="entities" id="search">
    <button id="search-btn">Go</button>
</div>


<div class="container"><strong>Entity</strong></div>
<table >
    <tr>
        <td><?php echo $ENTITY["entity_name"] ?></td>
        <td>Overall rating: <strong><?php echo $ENTITY["overall_rating"] ?></strong></td>
        <td>Confidence: <strong id="confidence"><?php echo ((4-$ENTITY["overall_rating"])/4)*100 ?>% </strong> </td>
    </tr>
</table>


<?php

$select = $db->prepare(""
    ."SELECT "
    ."entity.entity_name, "
    ."entity.overall_rating, "
    ."user.username, "
    ."user.score, "
    ."datediff(now(), user.create_time) joined, "
    ."review.review_id, "
    ."review.rating, "
    ."review.upvote_tally, "
    ."review.downvote_tally "
    ."FROM review "
    ."INNER JOIN entity on review.entity_id = entity.entity_id "
    ."INNER JOIN user on review.user_id = user.user_id "
    ."WHERE review.entity_id = $entity_id ");
if ($select->execute()) {
    $reviews = $select->fetchAll();
    if (count($reviews) > 0) {
        $ratings_average = (float) $reviews[0]['overall_rating'];
        $confidence = number_format(((4-$ratings_average)/4) * 100); 
?>
<!--gen values-->
<input type="hidden" name="review_count" value="<?php echo count($reviews) ?>">


<div class="container"><strong>Reviewers' ratings</strong></div>
<table>
    <thead>
        <tr>
            <th>Reviewer</th>
            <th>Reviewer chirps</th>
            <th>Joined</th>
            <th>Review rating (0 to 4)</th>
            <th>Review upvotes</th>
            <th>Review downvotes</th>
        </tr>
    </thead>
    <tbody>
    <?php 
        $total_rating = 0;
        foreach($reviews as $review) {
            $total_rating += $review["rating"];
    ?>
            <tr class="review" id="<?php echo $review["review_id"] ?>">
                <td><?php echo $review["username"] ?></td>
                <td><input type="text" class="score" id="<?php echo $review["review_id"] ?>-score" value="<?php echo $review["score"] ?>"></td>
                <td><input type="text" class="joined" id="<?php echo $review["review_id"] ?>-joined" value="<?php echo $review["joined"] ?>"> days ago</td>
                <td><input type="text" class="rating" id="<?php echo $review["review_id"] ?>-rating" value="<?php echo 4-$review["rating"] ?>"> <label id="<?php echo $review["review_id"] ?>-rating-output"></label></td>
                <td><input type="text" class="upvotes" id="<?php echo $review["review_id"] ?>-upvotes" value="<?php echo $review["upvote_tally"] ?>"></td>
                <td><input type="text" class="downvotes" id="<?php echo $review["review_id"] ?>-downvotes" value="<?php echo $review["downvote_tally"] ?>"></td>
            </tr>
    <?php
        }
    ?>
    </tbody>
</table>

<div class="container">
    <p>
        <strong>Individual reviewer's rating formula</strong><br>
        <textarea style="text-align:left; width: 100%" id="ratings-formula">rating</textarea><br>
        <small><strong>USE: </strong></small>
        <small><i><strong>chirps</strong>=Reviewer chirps</i></small>,
        <small><i><strong>days</strong>=Joined</i></small>,
        <small><i><strong>rating</strong>=Review rating</i></small>,
        <small><i><strong>upvotes</strong>=Review upvotes</i></small>,
        <small><i><strong>downvotes</strong>=Review downvotes</i></small>
    </p>

    <p>
        <strong>Results:</strong><br>
        Ratings total: <strong id="ratings-total"><?php echo $total_rating ?></strong><br>
        Ratings average: <strong id="ratings-average"><?php echo $ratings_average ?></strong><br>
        Calculated ratings total: <strong id="calculated-ratings-total"><?php echo $total_rating ?></strong><br>
        Calculated ratings average: <strong id="calculated-ratings-average"><?php echo $ratings_average ?></strong><br>
    </p>
    <p>
        <strong>Confidence formula</strong><br>
        <textarea style="text-align:left; width: 100%" id="confidence-formula">((calculated_ratings_average)/4) * 100</textarea><br>
        <small><strong>USE: </strong></small>
        <small><i><strong>ratings_total</strong>=Ratings total</i></small>,
        <small><i><strong>ratings_average</strong>=Ratings average</i></small>,
        <small><i><strong>calculated_ratings_total</strong>=Calculated ratings total</i></small>,
        <small><i><strong>calculated_ratings_average</strong>=Calculated ratings average</i></small>
    </p>
    <p>Calculated confidence: <strong id="calculated-confidence"><?php echo $confidence ?>%</strong></p>
    <div style="margin-top: 10px;">
        <button id="calculate-btn">Calculate</button>
        <button id="reset-btn">Reset</button>
    </div>
</div>

<?php
    } else {
        echo "<br><div class='container'>No reviews found </div>";
    }
} else {
    echo "<br>Error: Db failed select";
}
?>

</body>
</html>

<script type="text/javascript">

    var calculated_ratings_total = 0;
    var calculated_ratings_average = 0;

    function calcIndividualRating() {
        calculated_ratings_total = 0;
        $(".review").each(function(){
            const id = this.id;
            const score = $("#" + id + "-score").val();
            const days = $("#" + id + "-joined").val();
            const rating = $("#" + id + "-rating").val();
            const upvote = $("#" + id + "-upvotes").val();
            const downvote = $("#" + id + "-downvotes").val();

            // parse formula
            var formula = $("#ratings-formula").val();
            formula = formula.replace(/chirps/gi, score);
            formula = formula.replace(/days/gi, days);
            formula = formula.replace(/rating/gi, rating);
            formula = formula.replace(/upvotes/gi, upvote);
            formula = formula.replace(/downvotes/gi, downvote);

            // var rating_output = parseInt(rating);
            var rating_output = rating;
            if (formula.length > 0) {
                try {
                    rating_output = eval(formula);
                    calculated_ratings_total += parseInt(rating_output);
                    $("#" + id + "-rating-output").html(rating_output);   
                } catch (e) {
                    calculated_ratings_total += parseInt(rating_output);
                    $("#" + id + "-rating-output").html("formula error");  
                }
            } else {
                calculated_ratings_total += parseInt(rating);
            }
         
        });
        const review_count = parseInt($("[name=review_count]").val());

        calculated_ratings_average = (calculated_ratings_total / review_count);
        
        $("#calculated-ratings-total").html(calculated_ratings_total);
        $("#calculated-ratings-average").html(calculated_ratings_average.toFixed(3));

        calcConfidence();
    }

    function calcConfidence() {
        const ratings_total = parseInt($("#ratings-total").html());
        const ratings_average = parseFloat($("#ratings-average").html());
        const calculated_ratings_total = parseInt($("#calculated-ratings-total").html());
        const calculated_ratings_average = parseFloat($("#calculated-ratings-average").html());

        console.log(ratings_total, ratings_average, calculated_ratings_total, calculated_ratings_average);

        // parse formula
        var formula = $("#confidence-formula").val();
        formula = formula.replace(/calculated_ratings_total/gi, calculated_ratings_total);
        formula = formula.replace(/calculated_ratings_average/gi, calculated_ratings_average);
        formula = formula.replace(/ratings_total/gi, ratings_total);
        formula = formula.replace(/ratings_average/gi, ratings_average);

        console.log(formula);

        var confidence = eval(formula);
        $("#calculated-confidence").html(Math.round(confidence) + "%");
    }

    $("#calculate-btn").click(()=>{
        calcIndividualRating();
    });

    $("#reset-btn").click(()=>{
        location.reload();
    });

    $("#search-btn").click(()=>{
        const keyword = $("#search").val();
        if (keyword.length > 0) {
            const split = keyword.split("-");
            try {
                const id = parseInt(split[0]);
                const to = location.origin + "/test/calculator/?id=" + id;
                console.log("GO TO:", to);

                location.href=to;
            } catch (e) {
                console.error("invalid search keyword");
            }
        }
    });
</script>