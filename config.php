<?php
define ('WEBSITE_URL', 'https://cryptocanary.app/');
define ('__ROOT', "/var/www/admin/");
define ('__SHARED', __ROOT . "/shared/");
define ('__CONTROLLERS', __ROOT . "/controllers/");
define ('__TEMPLATES', __ROOT . "/email_templates/");
define ('entity_img_src', WEBSITE_URL . 'images/entities/');
define ('user_img_src', WEBSITE_URL . 'images/avatars/');

define ('product_img_src', WEBSITE_URL . 'images/products/');
define ('product_img_dir', '/var/www/html/images/products/');


session_start();

$request_uri_split = explode("/", $_SERVER["REQUEST_URI"]);
if(!isset($_SESSION["user"]) && $_SERVER['REQUEST_URI'] != "/" && $request_uri_split[1] !== 'test') {
    header('Location: /');
}

require 'database.php';