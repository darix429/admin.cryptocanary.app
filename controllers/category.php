<?php

use App\Database;

class Category {

    private static $_instance;

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
    }

    public function getCollection() {
        $db = Database::getInstance()->getConnection();
        $select = $db->prepare("SELECT * FROM entity_category WHERE delete_time IS NULL");
        $select->execute();
        Database::close();
        return $select->fetchAll();
    }

    public function getEcosystems() {
        $collection = ["General","Ethereum","Bitcoin","Bitcoin SV","Steem","EOS","Tron","Vechain","Blockstackm", "Other" ];
        return $collection;
    }
}