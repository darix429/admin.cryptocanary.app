<?php

use App\Database;

class Entity {

    private static $_instance;
    private $entity_img_dir;

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
        $this->db = Database::getInstance()->getConnection();
        $this->entity_img_dir = "/var/www/html/images/entities/";
    }

    public function approveAll() {
        try {
            $db = Database::getInstance()->getConnection();
            $update = $db->prepare("UPDATE entity SET approved_flag = 1 WHERE approved_flag IS NULL OR approved_flag = 0");
            if ($update->execute()) {
                Database::close();
                header("Location: /entity/?success=All pending projects approved");
            } else {
                Database::close();
                header("Location: /entity/?error=failed to execute update command");
            }
        } catch(PDOException $e) {
            Database::close();
            header("Location: /entity/?error=db error encountered");
        }
    }

    public function getCollection ($filter = array()) {

        $entityEmails = json_decode(file_get_contents("../data/entityEmails.json"));
        $entityWithEmails = [];
        foreach($entityEmails as $eemails) {
            array_push($entityWithEmails, $eemails->entity_id);
        }

        $select = ""
            . " SELECT "
            . "     e.entity_id, "
            . "     entity_name, "
            . "     username, "
            . "     e.user_id, "
            . "     symbol, "
            . "     image, "
            . "     e.review_count, "
            . "     overall_rating, "
            . "     approved_flag, "
            . "     e.create_time, "
            . "     e.delete_time, "
            . "     (CASE WHEN e.update_time < r.update_time THEN r.update_time ELSE e.update_time END) recent_activity ";
            
        $select .= " FROM entity e "
            . " left join user u on e.user_id = u.user_id "
            . " left join ( ";

        // join select review table for comment_date
        $select .= ''
		. ' select '
        . ' review.entity_id, '
        . ' (case  '
        . ' when max(date_format(from_unixtime(json_extract(comments, concat("$[",(case when json_length(comments)>0 then json_length(comments)-1 else 0 end),"].timestamp"))), "%Y-%m-%d %H:%m:%s")) > max(review.update_time) '
        . ' then max(date_format(from_unixtime(json_extract(comments, concat("$[",(case when json_length(comments)>0 then json_length(comments)-1 else 0 end),"].timestamp"))), "%Y-%m-%d %H:%m:%s")) '
        . ' else max(review.update_time) '
        . ' end) update_time '
        . ' from review group by review.entity_id ';
        
        $select .= ") r on e.entity_id = r.entity_id ";


        $collection = $select . " GROUP BY e.entity_id";
        
        if (!empty($filter)) {
            $page = (int) $filter["page"];
            $size = (int) $filter["size"];
            $sort = $filter["sort"];
            $direction = $filter["direction"];
            $search = $filter["search"];
            $filterEmails = $filter["filter-emails"];

            if (!empty($search)) {
                $select .= " WHERE entity_name LIKE '%$search%' OR symbol LIKE '%$search%' ";
                if ($filterEmails) {
                    $select .= " (AND e.entity_id IN (". (implode(",", $entityWithEmails)) ."))";
                }
            } else if ($filterEmails) {
                $select .= " WHERE e.entity_id IN (". (implode(",", $entityWithEmails)) .")";
                $collection = $select . " GROUP BY e.entity_id";
            }


            $select .= " GROUP BY entity_id ORDER BY $sort $direction ";

            if (empty($search)) {
                $select .= " LIMIT $page, $size ";
            }
        } else {
            $select = "SELECT * FROM entity";
        }


        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute();
            
            $all = $this->db->prepare($collection);
            $all->execute();
            
            $count = $all->rowCount();
            
            Database::close();
            return [
                "max_size" => $count,
                "collection" => $stmt->fetchAll()
            ];
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        
    }

    public function getEntityById($id) {
        $select = "SELECT entity.*, user.username, user.email FROM entity INNER JOIN user ON entity.user_id = user.user_id WHERE entity_id = ?";
        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute([$id]);
            Database::close();
            return $stmt->fetch();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        
    }

    public function getEntitiesByUserId($id) {
        $select = "SELECT * FROM entity WHERE user_id = ? ORDER BY create_time DESC";
        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute([$id]);
            Database::close();
            return $stmt->fetchAll();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        
    }

    public function create($data) {
        $name = trim(($data["name"]));
        $blocklist = explode(",", file_get_contents("/var/www/admin/data/blocklist/entity_names"));
        if(!in_array($name, $blocklist)) {
            $symbol = trim(strtoupper($data["symbol"]));
            $year = $data["year"];
            $industry = $data["industry"];
            $platform = $data["platform"];
            $country = $data["country"];
            $website = trim(strtolower($data["website"]));
            $website = '{"website" : "'.$website.'"}';
            $description = trim($data["description"]);

            $fork = isset($data["fork_flag"]);
            $ico = isset($data["ico_flag"]);
            if($year == 'N/A') {
                $year = null;
            }


            if (empty($year)) { $year = "N/A"; }
            if (empty($industry)) { $industry = "N/A"; }
            if (empty($platform)) { $platform = "N/A"; }
            if (empty($country)) { $country = "N/A"; }

            if (strlen($name) < 3) {
                $er = "Name must be more than three characters";
                // header("Location: /entity/?new&error=$er");
                echo $er;
                exit();
            }
            if (strlen($symbol) < 2) {
                $er = "Symbol must be more than two characters";
                // header("Location: /entity/?new&error=$er");
                echo $er;
                exit();
            }
            
            if(isset($_FILES["image"])) {
                if (!getimagesize($_FILES["image"]["tmp_name"])) {
                    echo "Unknown image type";
                    exit();
                }
                if ($_FILES["image"]["size"] > 500000) {
                    $isLargeImage = true;
                }
            }

            try {
                $insert = ""
                . " INSERT INTO entity ( "
                . "     entity_category_id, "
                . "     entity_name, "
                . "     symbol,  "
                . "     year,  "
                . "     platform,  "
                . "     industry,  "
                . "     country,  "
                . "     links,  "
                . "     entity_desc,  "
                . "     user_id,  "
                . "     fork_flag,  "
                . "     ico_flag,  "
                . "     create_time,  "
                . "     update_time)  "
                . " VALUES ( "
                . "     1,  "
                . "     :name,  "
                . "     :symbol, "
                . "     :year, "
                . "     :platform, "
                . "     :industry,  "
                . "     :country,  "
                . "     :website,  "
                . "     :description,  "
                . "     1 ,  "
                . "     :fork,  "
                . "     :ico ,  "
                . "     NOW(),  "
                . "     NOW()) ";

                $stmt = $this->db->prepare($insert);
                $stmt->bindParam(':name', $name, PDO::PARAM_STR);
                $stmt->bindParam(':symbol', $symbol, PDO::PARAM_STR);
                $stmt->bindParam(':website', $website, PDO::PARAM_STR);
                $stmt->bindParam(':description', $description, PDO::PARAM_STR);
                $stmt->bindParam(':year', $year, PDO::PARAM_INT);
                $stmt->bindParam(':platform', $platform, PDO::PARAM_STR);
                $stmt->bindParam(':industry', $industry, PDO::PARAM_STR);
                $stmt->bindParam(':country', $country, PDO::PARAM_STR);
                $stmt->bindParam(':fork', $fork, PDO::PARAM_BOOL);
                $stmt->bindParam(':ico', $ico, PDO::PARAM_BOOL);
                if ($stmt->execute()) {
                    $new_id = $this->db->lastInsertId();
                } else {
                    $er = "/entity/?new&error=There was an error encountered while inserting data to database";
                    var_dump($this->db->errorInfo());
                    header("Location: /entity/?new&error=$er");
                    exit();
                }

                // CREATE ENTITY IMAGE DIR
                $entity_dir = $this->entity_img_dir . $new_id;
                if (!file_exists($entity_dir)) {
                    mkdir($entity_dir);
                }

            } catch(PDOException $e) {
                echo 'Error: ' . $e->getMessage();
                exit();
            }
            
            // UPLOAD IMAGE
            if(isset($_FILES["image"]) && $new_id != 0) {
                $extension = explode(".", $_FILES["image"]["name"])[1];
                $image_name = "image-" . date("Ymdhis") . "." . $extension;
                $target_file = $entity_dir . '/' . $image_name;

                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

                    // SET ENTITY IMAGE
                    $setEntityImg = $this->db->prepare("UPDATE entity SET image=:image_name WHERE entity_id = :id");
                    $setEntityImg->bindParam(':image_name', $image_name, PDO::PARAM_STR);
                    $setEntityImg->bindParam(':id', $new_id);
                    if (!$setEntityImg->execute()) {
                        $er = "/entity/?new&error=Failed to assign uploaded image to newly created project.";
                        header("Location: /entity/?new&error=$er");
                        exit();
                    }

                } else {
                    $er = "/entity/?new&error=Entity created but an unknown error encountered while uploading image.";
                    var_dump($_FILES["image"]);
                    header("Location: /entity/?new&error=$er");
                    exit();
                }
            }
            Database::close();
            
            echo $new_id;
        } else {
            echo "$name is Blocklisted!";
        }
        
        
    }

    public function update($data) {
        try {
        $select = $this->db->prepare("SELECT * FROM entity WHERE entity_id = :id");
        $select->bindParam(":id", $data["id"], PDO::PARAM_INT);
        $select->execute();
        $entity = $select->fetch();

            if (count($entity) > 0) {
                $name = trim(($data["name"]));
                $symbol = trim(strtoupper($data["symbol"]));
                $year = $data["year"];
                $industry = $data["industry"];
                $platform = $data["platform"];
                $country = $data["country"];
                $website = (!empty($data["website"]))? trim(strtolower($data["website"])) : "";
                $description = trim($data["description"]);
                $seo_title = trim($data["seo_title"]);
                $seo_description = trim($data["seo_description"]);
                $tag1 = trim($data["tag1"]);
                
                $user = explode("-", $data['user'])[0];
                if (empty($user)) {
            
                    header("Location: /entity?id=".$data['id']."&error=Invalid entry for submitted by.");
                    exit();
                }
        
                $fork = isset($data["fork_flag"]);
                $ico = isset($data["ico_flag"]);
                if($year == 'N/A') {
                    $year = null;
                }
        
                if (empty($year)) { $year = "N/A"; }
                if (empty($industry)) { $industry = "N/A"; }
                if (empty($platform)) { $platform = "N/A"; }
                if (empty($country)) { $country = "N/A"; }

                $links = json_decode($entity["links"]);
                $links->website = $website;
                
                $new_links = "";
                foreach($links as $key=>$value) {
                    if(!is_array($value)) {
                        $new_links .= '"'. $key .'":"'. $value .'",';
                    } else {
                        $new_links .= '"'. $key .'":["'. implode('","', $value) .'"],';
                    }
                }
                $new_links = substr($new_links, 0, strlen($new_links)-1);
                $new_links = "{" . $new_links . "}";
        
                $query = ""
                . "UPDATE entity SET "
                . "entity_name = :name, "
                . "symbol = :symbol, "
                . "entity_desc = :description, "
                . "year = :year, "
                . "industry = :industry, "
                . "platform = :platform, "
                . "country = :country, "
                . "links = :links, "
                . "fork_flag = :fork, "
                . "ico_flag = :ico, "
                . "user_id = :user_id, "
                . "seo_title = :seo_title, "
                . "seo_description = :seo_description, "
                . "tag1 = :tag1, "
                . "update_time = NOW() "
                . "WHERE entity_id = :id ";
        
                    $stmt = $this->db->prepare($query);
                    $stmt->bindParam(':name', $name, PDO::PARAM_STR);
                    $stmt->bindParam(':symbol', $symbol, PDO::PARAM_STR);
                    $stmt->bindParam(':links', $new_links, PDO::PARAM_STR);
                    $stmt->bindParam(':description', $description, PDO::PARAM_STR);
                    $stmt->bindParam(':year', $year, PDO::PARAM_INT);
                    $stmt->bindParam(':platform', $platform, PDO::PARAM_STR);
                    $stmt->bindParam(':industry', $industry, PDO::PARAM_STR);
                    $stmt->bindParam(':country', $country, PDO::PARAM_STR);
                    $stmt->bindParam(':seo_title', $seo_title, PDO::PARAM_STR);
                    $stmt->bindParam(':seo_description', $seo_description, PDO::PARAM_STR);
                    $stmt->bindParam(':tag1', $tag1, PDO::PARAM_STR);
                    $stmt->bindParam(':fork', $fork, PDO::PARAM_BOOL);
                    $stmt->bindParam(':ico', $ico, PDO::PARAM_BOOL);
                    $stmt->bindParam(':id', $data['id'], PDO::PARAM_INT);
                    $stmt->bindParam(':user_id', $user, PDO::PARAM_INT);
                    if( $stmt->execute()){
                        
                        Util::getInstance()->generateDatalist();
                        Util::getInstance()->generateSitemap();
                        
        
                        header("Location: /entity?id=".$data['id']."&success=Project information successfully updated.");
                    }else {
                        header("Location: /entity?id=".$data['id']."&error=There was an error encountered while updating database data");
                    }

            } else {
                
                header("Location: /entity?id=".$data['id']."&error=Entity not found");
            }
            
            Database::close();

        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function changeImage($data) {

        $entity_id = $data["entity_id"];
        $entity_dir = $this->entity_img_dir . $entity_id;
        
        if(isset($_FILES["image"])) {
            if (!getimagesize($_FILES["image"]["tmp_name"])) {
                echo "Unknown image type";
                exit();
            }
            if ($_FILES["image"]["size"] > 500000) {
                $isLargeImage = true;
            }
        }

        // UPLOAD IMAGE
        if(isset($_FILES["image"]) && $entity_id != 0) {
            $extension = explode(".", $_FILES["image"]["name"])[1];
            $image_name = "image-" . date("Ymdhis") . "." . $extension;
            $target_file = $entity_dir . '/' . $image_name;

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

                // SET ENTITY IMAGE
                $setEntityImg = $this->db->prepare("UPDATE entity SET image=:image_name WHERE entity_id = :id");
                $setEntityImg->bindParam(':image_name', $image_name, PDO::PARAM_STR);
                $setEntityImg->bindParam(':id', $entity_id);
                if (!$setEntityImg->execute()) {
                    $er = "/entity/?new&error=Failed to assign uploaded image to newly created project.";
                    header("Location: /entity/?new&error=$er");
                    exit();
                } else {
                    echo "success";
                }

            } else {
                $er = "/entity/?new&error=Entity created but an unknown error encountered while uploading image.";
                var_dump($_FILES["image"]);
                header("Location: /entity/?new&error=$er");
                exit();
            }
        } else {
            echo "image not found";
        }
        
        Database::close();
    }

    public function remove($data) {
        $query = "UPDATE entity SET delete_time = NOW() WHERE entity_id = ? ";
        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute(array($data['id']));
            Database::close();
            
            Util::getInstance()->generateDatalist();
            header("Location: /entity");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        
        
    }

    public function permanent_remove($data) {
        try {
            $select = $this->db->prepare("SELECT * FROM entity WHERE entity_id = ?");
            $select->execute(array($data['id']));
            $entity = $select->fetch();

            $img_path = $this->entity_img_dir . $data["id"] . "/" . $entity["image"];
            unlink($img_path);

            $delete = $this->db->prepare("DELETE FROM entity WHERE entity_id = ? ");
            $delete->execute(array($data['id']));

            $blocklistFile = "/var/www/admin/data/blocklist/entity_names";
            $blocklist = explode(",", file_get_contents($blocklistFile));
            array_push($blocklist, $entity["entity_name"]);
            file_put_contents($blocklistFile, implode(",", $blocklist));
            
            Database::close();
            Util::getInstance()->generateDatalist();
            header("Location: /entity");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function restore($data) {
        $query = "UPDATE entity SET delete_time = NULL WHERE entity_id = ? ";
        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute(array($data['id']));
            Database::close();
            
            Util::getInstance()->generateDatalist();
            header("Location: /entity");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        
        
    }

    public function approve($data) {
        $query = "UPDATE entity SET approved_flag = 1 WHERE entity_id = ? ";
        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute(array($data['id']));
            Database::close();
            
            Util::getInstance()->generateDatalist();
            Util::getInstance()->generateSitemap();

            header("Location: /entity?success=Project approved.");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        
        
    }

    public function getIconLink($data) {
        $img = $data['image'];
        $link = entity_img_src . $data['entity_id'] . '/' . $img;
        return $link;
    }

    public function updateLinks($data) {
        if(isset($data["names"])) {
            $links = "{";
            for($i =0; $i<count($data["names"]); $i++) {
                $name = $data["names"][$i];
                $url = $data["urls"][$i];
                $links .= '"'.$name.'":"'.$url.'",';
            }
            $links = substr($links, 0, -1);
            $links .= "}";
            
            $update = $this->db->prepare("UPDATE entity SET links = :links WHERE entity_id = :id");
            $update->bindParam(":id", $data["id"], PDO::PARAM_INT);
            $update->bindParam(":links", $links, PDO::PARAM_STR);
            if($update->execute()) {
                header("Location: /entity/?id=".$data["id"]."&success=Project links updated.");
            } else {
                header("Location: /entity/?id=".$data["id"]."&error=Failed to update project links.");
            }
        } else {
            header("Location: /entity/?id=".$data["id"]."&error=No links found.");
        }

    }

    public function updateInviteFlag($data) {
        $update = $this->db->prepare("UPDATE entity SET invite_flag = :flag WHERE entity_id = :id");
        $update->bindParam(":flag", $data["flag"], PDO::PARAM_INT);
        $update->bindParam(":id", $data["id"], PDO::PARAM_INT);
        $update->execute();

    }
}