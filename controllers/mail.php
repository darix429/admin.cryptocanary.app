<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Database;

// require_once '/var/www/admin/vendor/autoload.php';
require_once __ROOT . '/vendor/autoload.php';

class Mailer {

    private static $_instance;
    
	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
        $this->db = Database::getInstance()->getConnection();

        $this->mail = new PHPMailer();
        $this->mail->SMTPDebug = 0;
        $this->mail->isSMTP();
        $this->mail->Host       = 'email-smtp.us-east-1.amazonaws.com';
        $this->mail->SMTPAuth   = true;
        $this->mail->Username   = 'AKIAJX4KAZZ54CMFZGXA';
        $this->mail->Password   = 'BFSCM0P5xx0KXrGktqMQZ4fSUOPWWMyNB3+Jf128cnHM';
        $this->mail->SMTPSecure = 'tls';
        $this->mail->Port       = 587;           
        $this->mail->setFrom('team@cryptocanary.app', 'CryptoCanary');
        $this->mail->addReplyTo('team@cryptocanary.app', 'CryptoCanary');
        $this->mail->isHTML(true);
    }

    public function sendApprovedEmail($data) {
        
        $entity_id = filter_input(INPUT_POST,'entity_id',FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST,'email',FILTER_SANITIZE_STRING);
        $entity = null;

        try {
            $stmt = $this->db->prepare("SELECT entity.entity_id, entity.entity_name FROM entity WHERE entity_id = :id");
            if (!$stmt->execute(array(":id"=>$entity_id))) {
                // FAILED
                $er = "failed to get entity $entity_id from db";
                header("Location: /entity/?id=$entity_id&error=$er");
            }

            $entity = $stmt->fetch(PDO::FETCH_ASSOC);

        }catch(Exception $e) {
            // FAILED
            echo "failed " . $e->getMessage();
            exit();
        }catch(PDOException $e) {
            // FAILED
            echo "db failed " . $e->getInfo();
            exit();
        }
        Database::close();

        $body = file_get_contents(__TEMPLATES . 'approved-submission.html');
        $link = WEBSITE_URL. 'review/?id=' . $entity_id;
        $body = str_replace('{{link}}', $link, $body);
        $body = str_replace('{{projectName}}', $entity["entity_name"], $body);

        $this->mail->addAddress($email);
        $this->mail->Subject = $entity["entity_name"] . ' has been approved!';
        $this->mail->MsgHTML($body);

        $log = date('Y-m-d H:i:s') . "-";
        if(!$this->mail->send()) {
            $log .= "Failed: [template: 'approved-submission.html', email: '$email']";
            file_put_contents("../logs/email.log", $log . PHP_EOL, FILE_APPEND);

            $er = "failed to send approved email";
            header("Location: /entity/?id=$entity_id&error=$er");
        } else {
            $log .= "Sent: [template: 'approved-submission.html', email: '$email']";
            file_put_contents("../logs/email.log", $log . PHP_EOL, FILE_APPEND);

            $return_msg = "Approved Project Email sent to $email";
            header("Location: /entity/?id=$entity_id&success=$return_msg");
        }

    }

    public function sendWithAttachment($email, $subject, $body, $file) {
        try {
            // $this->mail->isHTML(true);     
            $this->mail->addAddress($email);
            $this->mail->Subject = $subject;
            $this->mail->Body = $body;
            $this->mail->addAttachment($file);
            $this->mail->addBCC("daryl.decoyna@gmail.com");
            $this->mail->send();

            return ["success"=>true, "error"=>null];
        } catch (Exception $e) {
            return ["success"=>false, "error"=>$this->mail->ErrorInfo];
        }
    }

    public function sendProductApprovedEmail($req) {
        $id = $req["id"];
        $name = $req["name"];
        $email = $req["email"];
        $link = WEBSITE_URL . "discover/" . urlencode($name);

        $data = array(
            "email"=>$email,
            "subject"=>"$name has been approved!",
            "template"=>"approved-product.html",
            "data"=>[
                "productName"=>$name,
                "link"=>$link
            ]
        );

        if ($this->sendEmail($data)) {
            $return_msg = "Approved Product Email sent to $email";
            header("Location: /product/?id=$id&success=$return_msg");
        } else {
            $return_msg = "Failed to send approved email.";
            header("Location: /product/?id=$id&error=$return_msg");
        }
    }

    public function sendProductInvitationEmail($req) {
        $id = $req["id"];
        $name = $req["name"];
        $email = $req["email"];
        $link = WEBSITE_URL . "discover/" . urlencode($name);

        $data = array(
            "email"=>$email,
            "subject"=>"You should get people to review $name on CryptoCanary!",
            "template"=>"invite-product.html",
            "data"=>[
                "product_name"=>$name,
                "link"=>$link
            ]
        );

        if ($this->sendEmail($data)) {
            $db = Database::getInstance()->getConnection();

            $update = $db->prepare("UPDATE product SET invite_flag = 1 WHERE product_id = :id");
            $update->bindParam(":id", $id, PDO::PARAM_INT);
            $update->execute();

            Database::close();

            $return_msg = "Invitation Email sent to $email";
            header("Location: /product/?id=$id&success=$return_msg");
        } else {
            $return_msg = "Failed to send invitation email.";
            header("Location: /product/?id=$id&error=$return_msg");
        }
    }

    
	function sendEmail($data) {
        $to = $data["email"];
        $subject = $data["subject"];
        $template = $data["template"];
        $body = file_get_contents("../email_templates/$template");

        $variables = !empty($data["data"])? $data["data"] : [];
        foreach($variables as $key=>$value) {
            $body = str_replace("{{".$key."}}", $value, $body);
        }

        // $this->mailerInit();
        $this->mail->addAddress($to);
        $this->mail->Subject = $subject;
        $this->mail->MsgHTML($body);

        $res = false;
        $log = date('Y-m-d H:i:s') . "-";
        if(!$this->mail->send()) {
            $log .= "Failed: " . json_encode($data);
        } else {
            $log .= "Sent: " . json_encode($data);
            $res = true;
        }
        file_put_contents("../logs/email.log", $log . PHP_EOL, FILE_APPEND);

        return $res;
    }
}