<?php
/*
API KEY: f2a8c6fa8730a275f4ee5ae6741df8c0-us16

CryptoCanary users: ae2a8da42e
Bitcoin for Beginners Website Subscribers: d2965d37d9
Main Newsletter List: a94682165d
CC Microsite:  d4322e8c1b

-----------------------
DTD TEST
key: dee74042d463f4bd22849d1d801c0c81-us20
id: a1529ee598
*/

class Mailchimp {

    private static $_instance;

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
        $this->authToken = 'f2a8c6fa8730a275f4ee5ae6741df8c0-us16';
        $this->list_id = 'ae2a8da42e';
    }

    public function addEmailToList($email) {
        if (!empty($email)) {
            $authToken = $this->authToken;
            $list_id = $this->list_id;
            
            $postData = array(
                "email_address" => "$email", 
                "status" => "subscribed"
            );
        
            $dc = explode("-",$authToken)[1];
            $ch = curl_init('https://'.$dc.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
        
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: apikey '.$authToken,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));
        
            $response = curl_exec($ch);
            curl_close($ch);

            var_dump($response);
        } else {
            echo "email not set";
        }
    }
}