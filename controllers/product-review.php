<?php
use App\Database;

class ProductReview {

	private static $_instance;

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
        $this->db = Database::getInstance()->getConnection();
    }

    public function getAll() {
        $db = Database::getInstance()->getConnection();
        try {
            $get = $db->prepare("SELECT * FROM product_review");
            $get->execute();

            return $get->fetchAll();

        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        Database::close();
    }

    public function getCollection($filter) {
    }

    public function getReviewsByProductId($id) {
        $select = ""
        . " SELECT user.username, product.product_name, product_review.* "
        . " FROM product_review  "
        . " INNER JOIN product ON product_review.product_id = product.product_id  "
        . " INNER JOIN user ON product_review.user_id = user.user_id  "
        . " WHERE product_review.product_id = ? AND product_review.delete_time IS NULL ORDER BY product_review.update_time DESC ";
        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute([$id]);
            Database::close();
            return $stmt->fetchAll();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getReviewsByUserId($id) {
        
        $select = " "
        . " SELECT product.product_name, user.username, user.email, user.avatar, product_review.* "
        . " FROM product_review  "
        . " INNER JOIN user ON product_review.user_id = user.user_id  "
        . " INNER JOIN product ON product_review.product_id = product.product_id "
        . " WHERE product_review.user_id = ? AND product_review.delete_time IS NULL"
        . " ORDER BY update_time DESC ";

        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute([$id]);
            Database::close();
            return $stmt->fetchAll();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getReviewById($id) {
        $select = ""
        . " SELECT user.username, product.product_name, product.logo as product_img, product_review.* "
        . " FROM product_review  "
        . " LEFT JOIN product ON product_review.product_id = product.product_id  "
        . " LEFT JOIN user ON product_review.user_id = user.user_id  "
        . " WHERE product_review.review_id = ? AND product_review.delete_time IS NULL ORDER BY product_review.update_time DESC ";
        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute([$id]);
            Database::close();
            return $stmt->fetch();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function remove($data) {
        $reviewId = $data["review_id"];
        if (isset($data["sendNotification"])) {
            $reviewer = $this->db->prepare("SELECT email FROM user WHERE user_id = (SELECT user_id FROM product_review WHERE review_id = :review_id)");
            $reviewer->bindParam(":review_id", $reviewId, PDO::PARAM_INT);
            $reviewer->execute();
            $user = $reviewer->fetch();

            $reviewed = $this->db->prepare("SELECT product_name FROM product WHERE product_id = (SELECT product_id FROM product_review WHERE review_id = :review_id)");
            $reviewed->bindParam(":review_id", $reviewId, PDO::PARAM_INT);
            $reviewed->execute();
            $product = $reviewed->fetch();

            $mailData = array(
                "email"=>$user["email"],
                "subject"=>"Heads up: your review for ". $product["product_name"] ." has been removed.",
                "template"=>"review-deleted.html",
                "data"=>[
                    "entity_name"=>$product["product_name"]
                ]
            );

            $mailer = Mailer::getInstance();
            $mailer->sendEmail($mailData);
        }

        $this->db->beginTransaction();
        try {
            $delReview = $this->db->prepare("DELETE FROM product_review WHERE review_id = ? ");
            if (!$delReview->execute(array($data['review_id']))) {
                $this->db->rollBack();
            }
        }
        catch(PDOException $e) {
            $this->db->rollBack();
        }
        $this->db->commit();
        

        Database::close();
        $referer = "/product";
        if (isset($data["redirect"])) {
            $referer = $data["redirect"];
        } else if (isset($_SERVER["HTTP_REFERER"])){
            $referer = $_SERVER["HTTP_REFERER"];
        }

        header("Location: $referer");
        
    }

    public function restore($data) {
        $query = "UPDATE review SET delete_time = NULL WHERE review_id = ? ";
        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute(array($data['id']));
            Database::close();
            echo "success";
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        
    }

    public function update($data) {
        try {
            $update = $this->db->prepare("UPDATE product_review SET review=:review, rating=:rating, update_time=NOW() WHERE review_id = :id");
            $update->bindParam(":review", $data["review"], PDO::PARAM_STR);
            $update->bindParam(":id", $data["id"], PDO::PARAM_INT);
            $update->bindParam(":rating", $data["rating"], PDO::PARAM_INT);

            if ($update->execute()) {
                $msg = "Review is successfully updated.";
                header("Location: /review/?id=" . $data["id"] . "&type=product&success=$msg");
            } else {
                $msg = "Failed to update review.";
                header("Location: /review/?id=" . $data["id"] . "&type=product&error=$msg");
            }
            Database::close();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        

    }

    public function remove_comment($data) {
        try {
            $review = $this->db->prepare("SELECT comments FROM product_review WHERE review_id = :review_id");
            $review->bindParam(":review_id", $data["review_id"], PDO::PARAM_INT);
            $review->execute();
            $fetch = $review->fetch();

            $comments = json_decode($fetch["comments"]);


            $i = 0;
            $new_data = [];
            foreach($comments as $comment) {
                if ($comment->id == $data["id"]){
                    unset($comments[$i]);
                } else {
                    array_push($new_data, [
                        'id'=> $comment->id,
                        'user_id'=> $comment->user_id,
                        'username'=> $comment->username,
                        'comment'=> $comment->comment,
                        'timestamp'=> $comment->timestamp
                    ]);
                }
                $i++;
            }

            $comments_update = json_encode($new_data);

            $update = $this->db->prepare("UPDATE product_review SET comments = :comments WHERE review_id = :review_id");
            $update->bindParam(":comments", $comments_update);
            $update->bindParam(":review_id", $data["review_id"], PDO::PARAM_INT);
            if ($update->execute()) {
                echo "SUCCESS";
            } else {
                echo "FAILED";
            }
            Database::close();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        

    }

}