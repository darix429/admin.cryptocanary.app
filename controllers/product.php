<?php
require_once "../bin/simple_html_dom.php";
use App\Database;

class Product {

    private static $_instance;
    private $product_img_dir;
    private $img_upload_dir;

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
        $this->product_img_dir = product_img_src;
        $this->img_upload_dir = product_img_dir;

        $this->createProductImageUploadDir();
    }

    public function approveAll() {
        try {
            $db = Database::getInstance()->getConnection();
            $update = $db->prepare("UPDATE product SET approved_flag = 1 WHERE approved_flag IS NULL OR approved_flag = 0");
            if ($update->execute()) {
                Database::close();
                header("Location: /product/?success=All pending products approved");
            } else {
                Database::close();
                header("Location: /product/?error=failed to execute update command");
            }
        } catch(PDOException $e) {
            Database::close();
            header("Location: /product/?error=db error encountered");
        }
    }

    private function createProductImageUploadDir() {
        if (!file_exists($this->img_upload_dir)) {
            mkdir($this->img_upload_dir);
        }
    }

    public function getCollection($filter = array()) {

        $stmt = ""
            ." SELECT "
            ." p.product_id,  "
            ." p.logo,  "
            ." p.product_name,  "
            ." p.delete_time,  "
            ." p.create_time,  "
            ." p.update_time,  "
            ." p.approved_flag,  "
            ." (select count(review_id) from product_review where product_review.product_id = p.product_id) review_count,  "
            ." ec.category category_name , "
            ." (case when "
            ."     (select create_time from product_review pr where pr.product_id = p.product_id order by create_time desc limit 1) > p.create_time "
            ."     then (select create_time from product_review pr where pr.product_id = p.product_id order by create_time desc limit 1) "
            ."     else "
            ."     p.create_time "
            ." end) recent_activity "
            ." FROM product p  "
            ." LEFT JOIN entity_category ec ON p.category=ec.entity_category_id ";
            
        $allstmt = $stmt;
        
        if (!empty($filter)) {
            $page = (int) $filter["page"];
            $size = (int) $filter["size"];
            $sort = $filter["sort"];
            $direction = $filter["direction"];
            $search = $filter["search"];
            if (!empty($search)) {
                $stmt .= " WHERE (product_name LIKE '%$search%') ";
            }

            $stmt .= " ORDER BY $sort $direction LIMIT $page, $size";
        }

        $db = Database::getInstance()->getConnection();
        try {
            $select = $db->prepare($stmt);
            $select->execute();
            $collection = $select->fetchAll();
            
            $all = $db->prepare($allstmt);
            $all->execute();
            $max_size = $all->rowCount();
        } catch(PDOException $e) {
            echo $e->getInfo();
        }
        Database::close();

        return array(
            "max_size"=>$max_size,
            "collection"=>$collection
        );

    }

    public function getCollectionByUserId($id) {
        $db = Database::getInstance()->getConnection();
        $select = $db->prepare("SELECT * FROM product WHERE user_id = :id");
        $select->bindParam(":id", $id, PDO::PARAM_INT);
        $select->execute();
        $collection = $select->fetchAll();
        Database::close();
        return $collection;
    }

    public function getProductById($id) {
        $db = Database::getInstance()->getConnection();
        $select = $db->prepare("SELECT product.*, user.username, user.email FROM product LEFT JOIN user ON product.user_id = user.user_id WHERE  product_id = :id");
        $select->bindParam(":id", $id, PDO::PARAM_INT);
        $select->execute();

        $product = $select->fetch();

        Database::close();
        return $product;

    }

    public function getProductByUserId($req) {
        $db = Database::getInstance()->getConnection();
        $select = $db->prepare("SELECT * FROM product WHERE delete_time IS NULL AND user_id = :user_id");
        $select->bindParam(":user_id", $req["user_id"], PDO::PARAM_INT);
        $select->execute();

        $product = $select->fetch();

        Database::close();
        return $product;
    }

    public function create($req) {
        $name = trim($req["name"]);
        $BLOCKEDNames = explode(",", file_get_contents("/var/www/admin/data/blocklist/product_names"));
        if (!in_array($name, $BLOCKEDNames)) {
            $short_desc = trim($req["short_desc"]);
            $desc = trim($req["desc"]);
            $tag1 = trim($req["tag1"]);
            $seo_title = trim($req["seo_title"]);
            $seo_description = trim($req["seo_description"]);
            $website = trim($req["website"]);
    
    
            $links = "{";
            // $links .= (!empty($req["website"]))? '"website":"'.(trim($req["website"])).'",':'';
            $links .= (!empty($req["facebook"]))? '"facebook":"'.(trim($req["facebook"])).'",':'';
            $links .= (!empty($req["twitter"]))? '"twitter":"'.(trim($req["twitter"])).'",':'';
            $links .= (!empty($req["youtube"]))? '"youtube":"'.(trim($req["youtube"])).'",':'';
            $links .= (!empty($req["medium"]))? '"medium":"'.(trim($req["medium"])).'",':'';
            $links .= (!empty($req["telegram"]))? '"telegram":"'.(trim($req["telegram"])).'",':'';
            $links .= (!empty($req["reddit"]))? '"reddit":"'.(trim($req["reddit"])).'",':'';
            $links = substr($links, 0, -1);
            $links .= "}";
            if (empty($req["facebook"]) && empty($req["twitter"]) && empty($req["youtube"]) && empty($req["medium"]) && empty($req["telegram"]) && empty($req["reddit"])) {
                $links = "{}";
            }
    
            // company logo
            if(isset($_FILES["companyLogo"])) {
                if (!getimagesize($_FILES["companyLogo"]["tmp_name"])) {
                    echo "Unknown image type";
                    exit();
                }
                if ($_FILES["companyLogo"]["size"] > 500000) {
                    $isLargeImage = true;
                }
                $extension = explode(".", $_FILES["companyLogo"]["name"])[1];
                $companyLogo = "logo-". time() . "." . $extension;
            }
    
            // product photo
            if(isset($_FILES["productPhoto"])) {
                if (!getimagesize($_FILES["productPhoto"]["tmp_name"])) {
                    echo "Unknown image type";
                    exit();
                }
                if ($_FILES["productPhoto"]["size"] > 500000) {
                    $isLargeImage = true;
                }
                $extension = explode(".", $_FILES["productPhoto"]["name"])[1];
                $productPhoto = "image-". time() . "." . $extension;
            }
    
            $db = Database::getInstance()->getConnection();
            $stmt = "INSERT INTO product (user_id, product_name, short_desc, product_desc, category, ecosystem, website, image, logo, tag1, seo_title, seo_description, product_links "
            .") VALUES (1, :name, :short_desc, :desc, :category, :ecosystem, :website, :image, :logo, :tag1, :seo_title, :seo_description, :links) ";
    
            try {
                $insert = $db->prepare($stmt);
                $insert->bindParam(":name", $name, PDO::PARAM_STR);
                $insert->bindParam(":short_desc", $short_desc, PDO::PARAM_STR);
                $insert->bindParam(":desc", $desc, PDO::PARAM_STR);
                $insert->bindParam(":category", $req["category"], PDO::PARAM_INT);
                $insert->bindParam(":ecosystem", $req["ecosystem"], PDO::PARAM_STR);
                $insert->bindParam(":website", $website, PDO::PARAM_STR);
                $insert->bindParam(":image", $productPhoto, PDO::PARAM_STR);
                $insert->bindParam(":logo", $companyLogo, PDO::PARAM_STR);
                $insert->bindParam(":tag1", $tag1, PDO::PARAM_STR);
                $insert->bindParam(":seo_title", $seo_title, PDO::PARAM_STR);
                $insert->bindParam(":seo_description", $seo_description, PDO::PARAM_STR);
                $insert->bindParam(":links", $links, PDO::PARAM_STR);
                $insert->execute();
    
            } catch (PDOException $e) {
                echo "DB Error: ". $e->getInfo();
            }
    
            $newId = $db->lastInsertId();
    
            // create product dir
            $productDir = $this->img_upload_dir . $newId;
            if(!file_exists($this->img_upload_dir . $newId))
            mkdir($this->img_upload_dir . $newId);
    
            // upload images
            move_uploaded_file($_FILES["companyLogo"]["tmp_name"], $productDir . '/' . $companyLogo);
            move_uploaded_file($_FILES["productPhoto"]["tmp_name"], $productDir . '/' . $productPhoto);
    
            Database::close();
            echo $newId;
        } else {
            echo "$name is Blocked!";
        }

    }

    public function importCSV($data) {
            if(isset($_FILES["csv"])) {
                $validExt = ["csv", "CSV"];
                if ($_FILES["csv"]["error"] > 0) {
                    //file error
                    header("Location: /product/?error=Invalid file");
                    die();
                }
                $file = "../uploads/" . time() . ".csv";
                $ext = explode(".", $_FILES["csv"]["name"]);
                $ext = end($ext);

                if (!in_array($ext, $validExt)) {
                    //file error
                    header("Location: /product/?error=Invalid file");
                    die();
                }

                if (!file_exists("../uploads")) {
                    mkdir("../uploads");
                }

                if (file_exists($file)) {
                    if (!unlink($file)) {
                        header("Location: /product/?error=File already exists");
                        die();
                    }
                }
                
                if (move_uploaded_file($_FILES["csv"]["tmp_name"], $file)) {
                    //upload success > parse
                    $csv = array_map('str_getcsv', file($file));
                    $db = Database::getInstance()->getConnection();

                    $select = $db->prepare("SELECT entity_category_id, category FROM entity_category");
                    $select->execute();
                    $categories = $select->fetchAll();

                    $ecosystems = ["General","Ethereum","Bitcoin","Bitcoin SV","Steem","EOS","Tron","Vechain","Blockstack", "Other"];
                    $BLOCKEDNames = explode(",", file_get_contents("/var/www/admin/data/blocklist/product_names"));

                    $db->beginTransaction();
                    try {
                        for($i=1; $i<count($csv); $i++){
                            $data           = $csv[$i];
                            $user_id        = 0;
                            $category       = trim($data[1]);
                            $ecosystem      = trim($data[2]);
                            $name           = trim($data[3]);
                            $desc           = trim($data[4]);
                            $email          = trim($data[5]);
                            $website        = trim($data[6]);
                            $facebook       = trim($data[7]);
                            $twitter        = trim($data[8]);
                            $youtube        = trim($data[9]);
                            $medium         = trim($data[10]);
                            $telegram       = trim($data[11]);
                            $reddit         = trim($data[12]);
                            $product_photo_url  = isset($data[13])? trim($data[13]) : "";
                            $company_logo_url   = isset($data[14])? trim($data[14]) : "";

                            if (!in_array($name, $BLOCKEDNames)) {
                                    
                                $duplicate = $db->prepare("SELECT product_name FROM product WHERE product_name = :name");
                                $duplicate->bindParam(":name", $name, PDO::PARAM_STR);
                                $duplicate->execute();
                                if ($duplicate->rowCount() == 0) {
                                    // get user_id
                                    if (!empty($data[0])) {
                                        $checkUser = $db->prepare("SELECT * FROM user WHERE user_id = :id");
                                        $checkUser->bindParam(":id", $data[0], PDO::PARAM_INT);
                                        $checkUser->execute();
                                        if ($checkUser->rowCount() > 0) {
                                            $user_id = $data[0];
                                        }
                                    }

                                    // get category
                                    $categoryId = null;
                                    foreach ($categories as $cat) {
                                        if (strtolower($category) == strtolower($cat["category"])) {
                                            $categoryId = $cat["entity_category_id"];
                                        }
                                    }
                                    if (!$categoryId) {
                                        header("Location: /product/?error=Category error $category");
                                        $db->rollBack();
                                        Database::close();
                                        die();
                                    }

                                    // get ecosystem.
                                    foreach ($ecosystems as $eco) {
                                        if (strtolower($eco) == strtolower($ecosystem))
                                            $ecosystem = $eco;
                                    }

                                    $links = "{";
                                    $links.= (!empty($facebook)) ? '"facebook":"'.$facebook.'",':"";
                                    $links.= (!empty($twitter)) ? '"twitter":"'.$twitter.'",':"";
                                    $links.= (!empty($youtube)) ? '"youtube":"'.$youtube.'",':"";
                                    $links.= (!empty($medium)) ? '"medium":"'.$medium.'",':"";
                                    $links.= (!empty($telegram)) ? '"telegram":"'.$telegram.'",':"";
                                    $links.= (!empty($reddit)) ? '"reddit":"'.$reddit.'",':"";
                                    if ($links != "{")
                                    $links = substr($links,0,-1);
                                    $links.="}";

                                    // company logo & product photo
                                    $productPhotoFilename = "image-".time();
                                    $companyLogoFilename = "logo-".time();
                                    $validExtensions = ["jpg","jpeg","png","gif"];
                                    if(!empty($product_photo_url)) {
                                        $photoExt = explode(".", $product_photo_url);
                                        $photoExt = end($photoExt);
                                        if (in_array($photoExt, $validExtensions)) {
                                            $productPhotoFilename .= "." . $photoExt;
                                        }
                                    }
                                    if(!empty($company_logo_url)) {
                                        $logoExt = explode(".", $company_logo_url);
                                        $logoExt = end($logoExt);
                                        if (in_array($logoExt, $validExtensions)) {
                                            $companyLogoFilename .= "." . $logoExt;
                                        }
                                    }

                                    $insert = $db->prepare("INSERT INTO product (user_id, category, ecosystem, product_name, short_desc, email, website, product_links, approved_flag, image, logo) VALUES (:user_id, :category, :ecosystem, :name, :desc, :email, :website, :links, 0, :image, :logo)");
                                    $insert->bindParam(":user_id", $user_id, PDO::PARAM_INT);
                                    $insert->bindParam(":category", $categoryId, PDO::PARAM_INT);
                                    $insert->bindParam(":ecosystem", $ecosystem, PDO::PARAM_STR);
                                    $insert->bindParam(":name", $name, PDO::PARAM_STR);
                                    $insert->bindParam(":desc", $desc, PDO::PARAM_STR);
                                    $insert->bindParam(":email", $email, PDO::PARAM_STR);
                                    $insert->bindParam(":website", $website, PDO::PARAM_STR);
                                    $insert->bindParam(":links", $links, PDO::PARAM_STR);
                                    $insert->bindParam(":image", $productPhotoFilename, PDO::PARAM_STR);
                                    $insert->bindParam(":logo", $companyLogoFilename, PDO::PARAM_STR);
                                    $insert->execute();

                                    $uploadDir = $this->img_upload_dir . $db->lastInsertId();
                                    if(!file_exists($uploadDir)) {
                                        mkdir($uploadDir);
                                    }

                                    // grab photos
                                    $productPhotoPath = $uploadDir . "/". $productPhotoFilename; 
                                    $companyLogoPath = $uploadDir . "/". $companyLogoFilename; 

                                    if ((empty($product_photo_url) || empty($company_logo_url)) && !empty($twitter)) {
                                        $twitter = str_replace("mobile.","", $twitter);
                                        $grab = $this->grabTwitterImage($twitter);
                                        if (empty($product_photo_url) && isset($grab["product_photo"]))
                                        $product_photo_url = $grab["product_photo"];
                                        if (empty($company_logo_url) && isset($grab["company_logo"]))
                                        $company_logo_url = $grab["company_logo"];
                                    }

                                    if (!empty($product_photo_url)) {
                                        $product_photo = $this->grabImage($product_photo_url);
                                        file_put_contents($productPhotoPath, $product_photo);
                                        $this->imageResize($productPhotoPath, 730);
                                    }

                                    if (!empty($company_logo_url)) {
                                        $company_logo = $this->grabImage($company_logo_url);
                                        file_put_contents($companyLogoPath, $company_logo);
                                        $this->imageResize($companyLogoPath, 125);
                                    }
                                }
                            }

                        }
                    } catch (PDOException $e) {
                        $db->rollBack();
                        header("Location: /product/?error=PDO Error");
                    }

                    $db->commit();
                    Database::close();

                    unlink($file);

                    header("Location: /product/?success=Import success");
                } else {
                    //upload error
                    header("Location: /product/?error=Upload error");
                    die();
                }
            }
    }

    public function grabImage($url) {
        $ch = curl_init(); 
      
        curl_setopt($ch, CURLOPT_HEADER, 0); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_URL, $url); 
      
        $data = curl_exec($ch); 
        curl_close($ch); 
      
        return $data; 
    }

    public function grabTwitterImage($url) {
        try {
            $html = file_get_html($url);
            $result = array();
            foreach($html->find("img") as $el) {
                if (strpos($el->src, "1500x500") !== false) {
                    $result["product_photo"] = $el->src;
                }
                if (strpos($el->src,"400x400") !== false) {
                    $result["company_logo"] = $el->src;
                }
            }
            return $result;
        } catch (Exception $e) {
            return array();
        }
    }

    public function approve($data) {
        $db = Database::getInstance()->getConnection();
        $query = "UPDATE product SET approved_flag = 1 WHERE product_id = ? ";
        try {
            $stmt = $db->prepare($query);
            $stmt->execute(array($data['id']));
            Database::close();
            
            Util::getInstance()->generateDatalist();
            Util::getInstance()->generateSitemap();

            // header("Location: /product?success=Project approved.");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        Database::close();
    }

    public function restore($data) {
        $db = Database::getInstance()->getConnection();
        $query = "UPDATE product SET delete_time = NULL WHERE product_id = ? ";
        try {
            $stmt = $db->prepare($query);
            $stmt->execute(array($data['id']));
            Database::close();
            
            Util::getInstance()->generateDatalist();
            header("Location: /product");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        Database::close();
    }


    public function update($req) {
        $db = Database::getInstance()->getConnection();
        $name = trim($req["product_name"]);
        $website = trim($req["website"]);
        $short_desc = trim($req["short_desc"]);
        $product_desc = trim($req["product_desc"]);
        $seo_title = trim($req["seo_title"]);
        $seo_description = trim($req["seo_description"]);
        $tag1 = trim($req["tag1"]);

        $user = explode("-", $req['user'])[0];
        if (empty($user)) {
            header("Location: /product?id=".$data['id']."&error=Invalid entry for submitted by.");
            exit();
        }

        try {
            $stmt = "UPDATE product SET "
            ." product_name = :name, "
            ." website = :website, "
            ." short_desc = :short_desc, "
            ." product_desc = :product_desc, "
            ." seo_title = :seo_title, "
            ." seo_description = :seo_description, "
            ." tag1 = :tag1, "
            ." category = :category, "
            ." ecosystem = :ecosystem, "
            ." user_id = :user, "
            ." update_time = now() "
            ." WHERE product_id = :id ";

            $update = $db->prepare($stmt);
            $update->bindParam(":name",$name,PDO::PARAM_STR);
            $update->bindParam(":website",$website,PDO::PARAM_STR);
            $update->bindParam(":short_desc",$short_desc,PDO::PARAM_STR);
            $update->bindParam(":product_desc",$product_desc,PDO::PARAM_STR);
            $update->bindParam(":seo_title",$seo_title,PDO::PARAM_STR);
            $update->bindParam(":seo_description",$seo_description,PDO::PARAM_STR);
            $update->bindParam(":category",$req["category"],PDO::PARAM_STR);
            $update->bindParam(":ecosystem",$req["ecosystem"],PDO::PARAM_STR);
            $update->bindParam(":tag1",$tag1,PDO::PARAM_STR);
            $update->bindParam(":id",$req["id"],PDO::PARAM_INT);
            $update->bindParam(":user",$user,PDO::PARAM_INT);

            if($update->execute())
            header("Location: /product/?id=".$req["id"]."&success=Product information updated");
            else
            header("Location: /product/?id=".$req["id"]."&error=Failed to update product information");

        } catch(PDOException $e) {
            echo "DB Error: " . $e->getInfo();
        }

        Database::close();
    }

    public function remove($data) {
        $db = Database::getInstance()->getConnection();
        $query = "UPDATE product SET delete_time = NOW() WHERE product_id = :id ";
        try {
            $stmt = $db->prepare($query);
            $stmt->bindParam(":id", $data["id"], PDO::PARAM_INT);
            $stmt->execute();
            Database::close();
            
            // Util::getInstance()->generateDatalist();
            header("Location: /product");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
            Database::close();
        }
    }

    public function permanent_remove($data) {
        $db = Database::getInstance()->getConnection();
        try {
            $select = $db->prepare("SELECT * FROM product WHERE product_id = ?");
            $select->execute(array($data['id']));
            $product = $select->fetch();

            $img_path = product_img_dir . $data["id"] . "/" . $product["image"];
            unlink($img_path);

            $delete_reviews = $db->prepare("DELETE FROM product_review WHERE product_id = ? ");
            $delete_reviews->execute(array($data['id']));
            $delete = $db->prepare("DELETE FROM product WHERE product_id = ? ");
            $delete->execute(array($data['id']));
            
            $blocklist = explode(",", file_get_contents("/var/www/admin/data/blocklist/product_names"));
            array_push($product["product_name"]);
            $blocklist = implode(",", $blocklist);
            file_put_contents("/var/www/admin/data/blocklist/product_names", $blocklist);
            Database::close();
            
            // Util::getInstance()->generateDatalist();
            // header("Location: /product");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function updateLinks($req) {
        $db = Database::getInstance()->getConnection();
        try {
            $names = $req["names"];
            $urls = $req["urls"];
            $links = "{";
            for($i=0; $i<count($names); $i++) {
                $name = trim($names[$i]);
                $url = trim($urls[$i]);
                
                if (!empty($name) && !empty($url)) {
                    $links .= '"'.$name.'":"'.$url.'",';
                }
            }
            $links = substr($links,0,-1);
            $links.="}";

            $update = $db->prepare("UPDATE product SET product_links = :links WHERE product_id = :id");
            $update->bindParam(":links", $links, PDO::PARAM_STR);
            $update->bindParam(":id", $req["id"], PDO::PARAM_INT);
            
            $success = false;
            if($update->execute()) {
                $success = true;
            }
            Database::close();

            if($success) {
                header("Location: /product/?id=". $req["id"] . "&success=Product links updated");
            } else {
                header("Location: /product/?id=". $req["id"] . "&error=Failed to update product links");
            }
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    
    public function changeImage($data) {

        try {
            $db = Database::getInstance()->getConnection();
            $id = $data["product_id"];
            $type = $data["image_type"];
            $dir = $this->img_upload_dir . $id;
            var_dump($_FILES);

            
            if(!file_exists($this->img_upload_dir . $id))
            mkdir($this->img_upload_dir . $id);
            
            if(isset($_FILES["image"])) {
                if (!getimagesize($_FILES["image"]["tmp_name"])) {
                    echo "Unknown image type";
                    exit();
                }
                if ($_FILES["image"]["size"] > 500000) {
                    $isLargeImage = true;
                }
            }

            // UPLOAD IMAGE
            if(isset($_FILES["image"]) && $id != 0) {
                $extension = explode(".", $_FILES["image"]["name"])[1];
                $image_name = ($type=="company-logo"? "logo-":"image-") . time() . "." . $extension;
                $target_file = $dir . '/' . $image_name;

                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

                    // SET ENTITY IMAGE
                    $query = "UPDATE product SET ". ($type=="company-logo"? "logo":"image") ."=:image_name WHERE product_id = :id";
                    $setCompanyLogo = $db->prepare($query);
                    $setCompanyLogo->bindParam(':image_name', $image_name, PDO::PARAM_STR);
                    $setCompanyLogo->bindParam(':id', $id);
                    if (!$setCompanyLogo->execute()) {
                        $er = "/product/?id=$id&error=Failed to update image";
                        header("Location: /product/?id=$id&error=$er");
                        exit();
                    } else {
                        echo "success";
                    }

                } else {
                    $er = "/product/?id=$id&error=Failed to update image";
                    var_dump($_FILES["image"]);
                    header("Location: /product/?id=$id&error=$er");
                    exit();
                }
            } else {
                echo "image not found";
            }
            
            Database::close();
        } catch(Exception $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }

    public function imageResize($file, $max) {
        if (file_exists($file) && $max > 10) {
            $props = getimagesize($file);
            $mime = $props["mime"];
            // calculate new size
            $w = $props[0];
            $h = $props[1];
            $ref = $w>$h? $w : $h;
            if ($ref > $max) {
                $scale = $max / $ref;
                $w *= $scale;
                $h *= $scale;
            }
            // resize
            $image = null; $newImage = null; $filename = "";
            if ($mime == "image/png") {
                $image = imagecreatefrompng($file);
                $newImage = imagecreatetruecolor($w,$h);
                imagealphablending($newImage, false);
                imagesavealpha($newImage, true);
                $transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
                imagefilledrectangle($newImage, 0, 0, $w, $h, $transparent);
                imagecopyresampled($newImage, $image, 0,0,0,0, $w, $h, imagesx($image), imagesy($image));
                $image = $newImage;
                $image = imagepng($image, $file);
            } else if ($mime == "image/jpeg") {
                $image = imagecreatefromjpeg($file);
                $newImage = imagecreatetruecolor($w,$h);
                imagecopyresampled($newImage, $image, 0,0,0,0, $w, $h, imagesx($image), imagesy($image));
                $image = $newImage;
                $image = imagejpeg($image, $file);
            } else if ($mime == "image/gif") {
                $image = imagecreatefromgif($file);
                $newImage = imagecreatetruecolor($w,$h);
                imagecopyresampled($newImage, $image, 0,0,0,0, $w, $h, imagesx($image), imagesy($image));
                $image = $newImage;
                $image = imagejpeg($image, $file);
            }
            if (file_exists($image))
            imagedestroy($image);
        }
    }
}