<?php

class Rating {

	private static $_instance;

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
        // $this->db = parent::getInstance()->getConnection();
    }

    public function ratingToStars($rating) {
        $starRating = "";
        $rating = (4-$rating) + 1;
        for ($i=0; $i < $rating; $i++) {
            $starRating .= '&#9733;';
        }

        $color = "confidence-". ($rating*20);
        
        return "<span class='$color'>$starRating</span>";
    }

    public function ratingToLabel($rating) {
        $starRating = "";
        $rating = (4-$rating) + 1;
        switch($rating) {
            case 1: 
                $label = 'Very shady'; break;
            case 2:
                $label = 'Lean shady'; break;
            case 3:
                $label = 'Neutral'; break;
            case 4:
                $label = 'Lean legit'; break;
            case 5:
                $label = 'Very legit'; break;
            default:
                $label = ''; break;
        }

        $color = "confidence-". ($rating*20);
        
        return "<span class='$color'><strong>$label</strong></span>";
    }

}

?>