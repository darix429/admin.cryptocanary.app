<?php
use App\Database;

class Review {

	private static $_instance;

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
        $this->db = Database::getInstance()->getConnection();
    }

    public function getAll() {
        try {
            $get = $this->db->prepare("SELECT * FROM review");
            $get->execute();

            return $get->fetchAll();

        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        
    }

    public function getCollection($filter) {

        $select = ""
             . " SELECT "
             . " review_id, "
             . " entity.entity_id, "
             . " entity.entity_name, "
             . " entity.symbol, "
             . " entity.overall_rating, "
             . " user.user_id, "
             . " user.username, "
             . " review, "
             . " IF(`comments` NOT LIKE '%[]%', (JSON_EXTRACT(`comments`, CONCAT('$[', JSON_LENGTH(`comments` ->> '$')-1,'].timestamp'))), NULL) comment_date, "
             . " review.create_time "
             . " FROM review "
             . " LEFT JOIN entity ON review.entity_id = entity.entity_id "
             . " LEFT JOIN user ON review.user_id = user.user_id ";

        $collection = $select;


        if (!empty($filter)) {
            $page = (int) $filter["page"];
            $size = (int) $filter["size"];
            $sort = $filter["sort"];
            $direction = $filter["direction"];
            $search = $filter["search"];
            if (!empty($search)) {
                $select .= " WHERE  "
                    ." review LIKE '%$search%'";
            }

            $select .= " ORDER BY $sort $direction ";

            if (empty($search)) {
                $select .= " LIMIT $page, $size ";
            }
        }


        try {
            $get = $this->db->prepare($select);
            $get->execute();

            $all = $this->db->prepare($collection);
            $all->execute();
            $count = $all->rowCount();
            Database::close();

            return [
                "max_size" => $count,
                "collection" => $get->fetchAll()
            ];

        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        
    }

    public function getReviewsByEntityId($id) {
        
        $select = ""
        . " SELECT user.username, user.email, user.avatar, entity.entity_name, review.* "
        . " FROM review  "
        . " INNER JOIN entity ON review.entity_id = entity.entity_id  "
        . " INNER JOIN user ON review.user_id = user.user_id  "
        . " WHERE review.entity_id = ? AND review.delete_time IS NULL ORDER BY update_time DESC ";
        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute([$id]);
            Database::close();
            return $stmt->fetchAll();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        

    }

    public function getReviewsByUserId($id) {
        
        $select = " "
        . " SELECT entity.entity_name, user.username, user.email, user.avatar, review.* "
        . " FROM review  "
        . " INNER JOIN user ON review.user_id = user.user_id  "
        . " INNER JOIN entity ON review.entity_id = entity.entity_id "
        . " WHERE review.user_id = ? AND review.delete_time IS NULL"
        . " ORDER BY update_time DESC ";

        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute([$id]);
            Database::close();
            return $stmt->fetchAll();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        

    }

    public function getReviewById($id) {

        $select = ""
            . " SELECT"
            . " review.*,"
            . " entity.entity_name,"
            . " entity.image,"
            . " user.username,"
            . " user.avatar"
            . " FROM review"
            . " LEFT JOIN entity ON review.entity_id = entity.entity_id"
            . " LEFT JOIN user ON review.user_id = user.user_id"
            . " WHERE review.review_id = :id";
        try {
            $stmt = $this->db->prepare($select);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            Database::close();
            return $stmt->fetch();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
    }

    public function remove($data) {
        $reviewId = $data["review_id"];
        if (isset($data["sendNotification"])) {
            $reviewer = $this->db->prepare("SELECT email FROM user WHERE user_id = (SELECT user_id FROM review WHERE review_id = :review_id)");
            $reviewer->bindParam(":review_id", $reviewId, PDO::PARAM_INT);
            $reviewer->execute();
            $user = $reviewer->fetch();

            $reviewed = $this->db->prepare("SELECT entity_name FROM entity WHERE entity_id = (SELECT entity_id FROM review WHERE review_id = :review_id)");
            $reviewed->bindParam(":review_id", $reviewId, PDO::PARAM_INT);
            $reviewed->execute();
            $entity = $reviewed->fetch();

            $mailData = array(
                "email"=>$user["email"],
                "subject"=>"Heads up: your review for ". $entity["entity_name"] ." has been removed.",
                "template"=>"review-deleted.html",
                "data"=>[
                    "entity_name"=>$entity["entity_name"]
                ]
            );

            $mailer = Mailer::getInstance();
            $mailer->sendEmail($mailData);
        }

        $this->db->beginTransaction();
        try {
            $delVotes = $this->db->prepare("DELETE FROM vote WHERE review_id = ?");
            if (!$delVotes->execute(array($data['review_id']))) {
                $this->db->rollBack();
            }

            $delReview = $this->db->prepare("DELETE FROM review WHERE review_id = ? ");
            if (!$delReview->execute(array($data['review_id']))) {
                $this->db->rollBack();
            }
        }
        catch(PDOException $e) {
            $this->db->rollBack();
        }
        $this->db->commit();
        

        Database::close();
        $referer = "/review";
        if (isset($data["redirect"])) {
            $referer = $data["redirect"];
        } else if (isset($_SERVER["HTTP_REFERER"])){
            $referer = $_SERVER["HTTP_REFERER"];
        }


        header("Location: $referer");
        
    }

    public function restore($data) {
        $query = "UPDATE review SET delete_time = NULL WHERE review_id = ? ";
        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute(array($data['id']));
            Database::close();
            echo "success";
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        
    }

    public function update($data) {
        try {
            $update = $this->db->prepare("UPDATE review SET review=:review, team_quality=:team_quality, info_quality=:info_quality, track_record=:track_record, update_time=NOW() WHERE review_id = :id");
            // $update->bindParam(":rating", $data["rating"], PDO::PARAM_STR);
            $update->bindParam(":review", $data["review"], PDO::PARAM_STR);
            $update->bindParam(":id", $data["review_id"], PDO::PARAM_INT);
            $update->bindParam(":team_quality", $data["team_quality"], PDO::PARAM_INT);
            $update->bindParam(":info_quality", $data["info_quality"], PDO::PARAM_INT);
            $update->bindParam(":track_record", $data["track_record"], PDO::PARAM_INT);

            if ($update->execute()) {
                $msg = "Review is successfully updated.";
                header("Location: /review/?id=" . $data["review_id"] . "&success=$msg");
            } else {
                $msg = "Failed to update review.";
                header("Location: /review/?id=" . $data["review_id"] . "&error=$msg");
            }
            Database::close();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        

    }

    public function remove_comment($data) {
        try {
            $review = $this->db->prepare("SELECT comments FROM review WHERE review_id = :review_id");
            $review->bindParam(":review_id", $data["review_id"], PDO::PARAM_INT);
            $review->execute();
            $fetch = $review->fetch();

            $comments = json_decode($fetch["comments"]);


            $i = 0;
            $new_data = [];
            foreach($comments as $comment) {
                if ($comment->id == $data["id"]){
                    unset($comments[$i]);
                } else {
                    array_push($new_data, [
                        'id'=> $comment->id,
                        'user_id'=> $comment->user_id,
                        'username'=> $comment->username,
                        'comment'=> $comment->comment,
                        'timestamp'=> $comment->timestamp
                    ]);
                }
                $i++;
            }

            $comments_update = json_encode($new_data);

            $update = $this->db->prepare("UPDATE review SET comments = :comments WHERE review_id = :review_id");
            $update->bindParam(":comments", $comments_update);
            $update->bindParam(":review_id", $data["review_id"], PDO::PARAM_INT);
            if ($update->execute()) {
                echo "SUCCESS";
            } else {
                echo "FAILED";
            }
            Database::close();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        

    }

}