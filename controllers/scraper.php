<?php
// require_once "/var/www/admin/vendor/autoload.php";
        
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use JonnyW\PhantomJs\Client as PhantomJS;

class Scraper {
    
    private static $_instance;

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
        $this->client = new Client();
        $this->guzzleClient = new  GuzzleClient(array(
            'timeout'=>10
        ));
        $this->client->setClient($this->guzzleClient);
    }

    public function getWebsiteEmails($website) {
        $crawler = $this->client->request('GET', $website);
        $this->emails = array();
        $crawler->filter("a")->each(function($a){
            $href = $a->attr("href");
            if (strpos($href, "mailto:") !== false) {
                $href = str_replace("mailto:","",$href);

                $split = explode("?", $href);
                $href = $split[0];
                if (filter_var($href, FILTER_VALIDATE_EMAIL)) {
                    if (!in_array($href, $this->emails)) {
                        array_push($this->emails, $href);
                    }
                }
            }
        });
        return $this->emails;
    }

    public function getAjaxWebsiteEmails($website, $phantomPath) {
        try {
            $client = PhantomJS::getInstance();
            $client->getEngine()->setPath($phantomPath);
            $client->isLazy();

            $request = $client->getMessageFactory()->createRequest($website, "GET", 5000);
            $response = $client->getMessageFactory()->createResponse();

            $client->send($request, $response);
    
            echo "<br>RESPONSE STATUS: " . $response->getStatus();
            $page = $response->getContent();
            if (!empty($page)) {
                
                $doc = new DOMDocument();
                $doc->loadHtml($page);
        
                $links = $doc->getElementsByTagName("a");
                $emails = array();
                foreach($links as $link) {
                    $href = $link->getAttribute("href");
                    if (strpos($href,"mailto") !== false) {
                        $target = str_replace("mailto:", "", $href);
                        $email = explode("?", $target)[0];
        
                        if (!in_array($email, $emails)) {
                            array_push($emails, $email);
                        }
                    }
                }
                return $emails;
            } else {
                return null;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return null;
        }
    }

    public function getAllLinks($website, $phantomPath, $timeout) {
        $client = PhantomJS::getInstance();
        $client->getEngine()->setPath($phantomPath);
        $client->isLazy();

        $request = $client->getMessageFactory()->createRequest($website, "GET");
        $request->setTimeout($timeout);
        $response = $client->getMessageFactory()->createResponse();
        $client->send($request, $response);

        $page = $response->getContent();
        $doc = new DOMDocument();
        $doc->loadHtml($page);

        $links = $doc->getElementsByTagName("a");
        $result = array();
        foreach($links as $link) {
            $href = $link->getAttribute("href");
            if (!in_array($href, $result)) {
                array_push($result, $href);
            }
        }

        return $result;
    }
}