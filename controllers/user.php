<?php

use App\Database;


class User {

	private static $_instance;
	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
        $this->db = Database::getInstance()->getConnection();
    }

    public function getCollection ($filter = array()) {

        $select = " SELECT * FROM user WHERE user_id != 1 ";
        $collection = $select;

        if (!empty($filter)) {
            $page = (int) $filter["page"];
            $size = (int) $filter["size"];
            $sort = $filter["sort"];
            $direction = $filter["direction"];
            $search = $filter["search"];
            if (!empty($search)) {
                $select .= " AND (username LIKE '%$search%' OR email LIKE '%$search%') ";
            }

            $select .= " ORDER BY $sort $direction LIMIT $page, $size";
        }
        
        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute();

            $all = $this->db->prepare($collection);
            $all->execute();
            
            $count = $all->rowCount();
            
            Database::close();
            return [
                "max_size" => $count,
                "collection" => $stmt->fetchAll()
            ];
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        
    }

    public function getUserById($id) {
        $select = "SELECT * FROM user WHERE user_id = ?";
        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute([$id]);
            Database::close();
            return $stmt->fetch();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        
    }

    public function getAvatarLink ($user) {
        $avatar = $user['avatar'];
        if (empty($avatar)) {
            return "/assets/images/default-user.png";
        } else {
            $link = user_img_src . $user['user_id'] . '/' . $avatar;
            $split = explode("-", $avatar);
            if ($split[0] === 'random') {
                $link = user_img_src . 'random/avatar'. $split[2] .'.png';
            }
            return $link;
        }
    }


    public function create($data) {
        $name = strtolower($data["name"]);
        $email = trim(strtolower($data["email"]));
        $username = trim(strtolower($data["username"]));
        $password = trim(strtolower($data["password"]));

        if (strlen($name) < 3) {
            $er = "Name must be more than 2 characters";
            header("Location: /user/?new&error=$er");
            exit();
        }
        if (strlen($username) < 4) {
            $er = "Username must be more than three characters";
            header("Location: /user/?new&error=$er");
            exit();
        }
        if (strlen($password) < 6) {
            $er = "Password must be more than five characters";
            header("Location: /user/?new&error=$er");
            exit();
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $er = "Invalid email format"; 
            header("Location: /user/?new&error=$er");
            exit();
        }

        $userExists = 0;
        try {
            $check  = $this->db->prepare("SELECT username FROM user WHERE username = :username OR email = :email");
            $check->bindParam(':username', $username, PDO::PARAM_STR);
            $check->bindParam(':email', $email, PDO::PARAM_STR);
            $check->execute();
            $userExists = $check->rowCount();
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
        

        if ($userExists > 0) {
            $er = "User already exists"; 
            header("Location: /user/?new&error=$er");
            exit();
        } else {
            try {
                $hash = password_hash($password, PASSWORD_BCRYPT);
                $avatar = "random-avatar-" . mt_rand(1, 19);
                $stmt = $this->db->prepare("INSERT INTO user(name, email, username, password, avatar) VALUES (:name, :email, :username, :password, :avatar) ");
                $stmt->bindParam(':name', $name, PDO::PARAM_STR);
                $stmt->bindParam(':username', $username, PDO::PARAM_STR);
                $stmt->bindParam(':email', $email, PDO::PARAM_STR);
                $stmt->bindParam(':password', $hash, PDO::PARAM_STR);
                $stmt->bindParam(':avatar', $avatar, PDO::PARAM_STR);
                $stmt->execute();
                $new_id = $this->db->lastInsertId();

                
                

                Mailchimp::getInstance()->addEmailToList($email);

                header("Location: /user/?id=$new_id");
            }
            catch(PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        }
        
        Database::close();
        
    }

    public function login($data) {
        if (strtolower($data['username']) == 'admin') {
            $select = "SELECT password FROM user WHERE username = 'admin'";
            try {
                $stmt = $this->db->prepare($select);
                $stmt->execute();
                $admin = $stmt->fetch();
                

                if(password_verify($data['password'], $admin["password"])) {
                    $_SESSION["user"] = "admin";
                    header('Location: /');
                } else {
                    header('Location: /?error=invalid password');
                }
                
                Database::close();
            }
            catch(PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
            
        } else {
            header('Location: /?error=invalid username');
        }
    }

    public function logout() {
        
        session_destroy();
        header('Location: /');
    }

    
    public function remove($data) {
        $query = "UPDATE user SET delete_time = NOW() WHERE user_id = ? ";
        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute(array($data['id']));
            Database::close();
            header("Location: /user");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
    }

    public function permanent_remove($data) {
        try {
            $this->db->beginTransaction();
            $vote = $this->db->prepare("DELETE FROM vote WHERE user_id = :id ");
            $vote->execute(array(":id"=>$data['id']));
            $review = $this->db->prepare("DELETE FROM review WHERE user_id = :id ");
            $review->execute(array(":id"=>$data['id']));
            $entity = $this->db->prepare("DELETE FROM entity WHERE user_id = :id ");
            $entity->execute(array(":id"=>$data['id']));
            $product_vote = $this->db->prepare("DELETE FROM product_vote WHERE user_id = :id ");
            $product_vote->execute(array(":id"=>$data['id']));
            $product_review = $this->db->prepare("DELETE FROM product_review WHERE user_id = :id ");
            $product_review->execute(array(":id"=>$data['id']));
            $product = $this->db->prepare("DELETE FROM product WHERE user_id = :id ");
            $product->execute(array(":id"=>$data['id']));
            $user = $this->db->prepare("DELETE FROM user WHERE user_id = :id ");
            $user->execute(array(":id"=>$data['id']));
            $this->db->commit();
            
            Database::close();
        }
        catch(PDOException $e) {
            $this->db->rollBack();
            echo 'Error: ' . $e->getMessage();
        }        
    }

    
    public function restore($data) {
        $query = "UPDATE user SET delete_time = NULL WHERE user_id = ? ";
        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute(array($data['id']));
            Database::close();
            header("Location: /user");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
    }

    public function block($data) {
        $query = "UPDATE user SET block_flag = 1 WHERE user_id = ? ";
        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute(array($data['id']));
            Database::close();
            header("Location: /user");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
    }

    public function unblock($data) {
        $query = "UPDATE user SET block_flag = NULL WHERE user_id = ? ";
        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute(array($data['id']));
            Database::close();
            header("Location: /user");
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
    }

    public function getUserList() {
        try {
            $stmt = $this->db->prepare("SELECT user_id, username, email FROM user");
            $stmt->execute();
            Database::close();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
    }
}