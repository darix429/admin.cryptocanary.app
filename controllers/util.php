<?php

use App\Database;

class Util{
    
    private static $_instance;

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
        $this->db = Database::getInstance()->getConnection();
    }
    public function getPlatforms() {
        return array("N/A","Ethereum","EOS","NEO","Stellar","Tron","NEM","Waves","Other");
    }
    public function getIndustries() {
        return array("N/A","Gambling","Games","Finance","Social","Exchanges","Development","Media","Wallet","Privacy/Security","Governance","Property","Storage","Identity","Energy","Insurance","Health","Other");
    }
    public function getCountries() {
        return array("Global","Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
    }
    public function getYears() {
        $years = array("N/A");
        for($year = 2009; $year <= (int) date("Y"); $year++) {
            array_push($years, $year);
        }
        return $years;
    }
    public function generateDatalist() {
        $this->httpRequest("GET", "https://cryptocanary.app/data/jsonGenerator.php", null, null);
    }

    public function generateSitemap() {
        $this->httpRequest("GET", "https://cryptocanary.app/data/sitemapGenerator.php", null, null);
    }

    public function getEntityStats() {
        
        try {
            $select = ""
            . " SELECT "
            . " COUNT(*) total, "
            . " SUM(case when not delete_time is null THEN 1 else 0 end) deleted, "
            . " SUM(case when approved_flag is null or approved_flag = 0 then 1 else 0 end) pending "
            . " FROM entity; ";
            
            $stmt = $this->db->prepare($select);
            $stmt->execute();
            Database::close();
            return $stmt->fetch();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getProductStats() {
        try {
            $db = Database::getInstance()->getConnection();
            $select = ""
            . " SELECT "
            . " COUNT(*) total, "
            . " SUM(case when not delete_time is null THEN 1 else 0 end) deleted, "
            . " SUM(case when approved_flag is null or approved_flag = 0 then 1 else 0 end) pending "
            . " FROM product; ";
            
            $stmt = $db->prepare($select);
            $stmt->execute();
            Database::close();
            return $stmt->fetch();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getUserStats() {
        
        try {
            $select = ""
            . " SELECT "
            . " COUNT(*) total, "
            . " SUM(case when not delete_time is null then 1 else 0 end) deleted, "
            . " SUM(case when block_flag = true then 1 else 0 end) blocked "
            . " FROM user ";
            
            $stmt = $this->db->prepare($select);
            $stmt->execute();
            Database::close();
            return $stmt->fetch();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getEntityDateEntries($start, $end) {
        $exclude = ["'20190604'","'20190605'", "'20190826'", "'20190901'"];
        try { 
            $select = ""
            . " SELECT "
            . " DATE(create_time) date, "
            . " COUNT(*) entity "
            . " FROM entity "
            . " WHERE create_time >= :start AND create_time <= :end"
            . " AND DATE_FORMAT(create_time, '%Y%m%d') NOT IN (". implode(",", $exclude) .")"
            . " GROUP BY DATE(create_time) ORDER BY DATE(create_time) ASC ";

            $selectAll = ""
            . " SELECT "
            . " DATE(create_time) date, "
            . " COUNT(*) entity "
            . " FROM entity "
            . " WHERE create_time >= :start AND create_time <= :end"
            . " GROUP BY DATE(create_time) ORDER BY DATE(create_time) ASC ";

            $stmt = $this->db->prepare($select);
            $stmt->bindParam(':start', $start, PDO::PARAM_STR);
            $stmt->bindParam(':end', $end, PDO::PARAM_STR);
            $stmt->execute();
            $filtered = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = $this->db->prepare($selectAll);
            $stmt->bindParam(':start', $start, PDO::PARAM_STR);
            $stmt->bindParam(':end', $end, PDO::PARAM_STR);
            $stmt->execute();
            $all = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            Database::close();
            return ["filtered"=>$filtered, "all"=>$all];

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getProductDateEntries($start, $end) {
        $exclude = ["'20190604'","'20190605'", "'20190826'", "'20190901'"];
        try { 
            $select = ""
            . " SELECT "
            . " DATE(create_time) date, "
            . " COUNT(*) product "
            . " FROM product "
            . " WHERE create_time >= :start AND create_time <= :end "
            . " GROUP BY DATE(create_time) ORDER BY DATE(create_time) ASC; ";
            $stmt = $this->db->prepare($select);
            $stmt->bindParam(':start', $start, PDO::PARAM_STR);
            $stmt->bindParam(':end', $end, PDO::PARAM_STR);
            $stmt->execute();
            Database::close();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getUserDateEntries($start, $end) {
        try { 
            $select = ""
            . " SELECT "
            . " DATE(create_time) date, "
            . " COUNT(*) user "
            . " FROM user "
            . " WHERE create_time BETWEEN :start AND :end"
            . " GROUP BY DATE(create_time) ORDER BY DATE(create_time) ASC ";
            $stmt = $this->db->prepare($select);
            $stmt->bindParam(':start', $start, PDO::PARAM_STR);
            $stmt->bindParam(':end', $end, PDO::PARAM_STR);
            $stmt->execute();
            Database::close();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getReviewDateEntries($start, $end) {
        try { 
            $select = ""
            . " SELECT "
            . " DATE(create_time) date, "
            . " COUNT(*) review "
            . " FROM review "
            . " WHERE create_time BETWEEN :start AND :end"
            . " GROUP BY DATE(create_time) ORDER BY DATE(create_time) ASC ";
            $stmt = $this->db->prepare($select);
            $stmt->bindParam(':start', $start, PDO::PARAM_STR);
            $stmt->bindParam(':end', $end, PDO::PARAM_STR);
            $stmt->execute();
            Database::close();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getProductReviewDateEntries($start, $end) {
        try { 
            $select = ""
            . " SELECT "
            . " DATE(create_time) date, "
            . " COUNT(*) product_review "
            . " FROM product_review "
            . " WHERE create_time BETWEEN :start AND :end "
            . " GROUP BY DATE(create_time) ORDER BY DATE(create_time) ASC; ";

            $stmt = $this->db->prepare($select);
            $stmt->bindParam(':start', $start, PDO::PARAM_STR);
            $stmt->bindParam(':end', $end, PDO::PARAM_STR);
            $stmt->execute();
            Database::close();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function getAccumTotalFromDate($table, $date) {
        try { 
            $select = ""
            ." SELECT * "
            ." FROM $table WHERE create_time <= DATE(:date) ";

            $stmt = $this->db->prepare($select);
            $stmt->bindParam(':date', $date, PDO::PARAM_STR);
            $stmt->execute();
            Database::close();
            return $stmt->rowCount();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function exportCsv($array, $filename = "export.csv", $delimiter=";") {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
    
        $f = fopen('php://output', 'w');
        foreach ($array as $line) {
            fputcsv($f, $line, $delimiter);
        }
    }  
    
    public function getEmailsByEntityId($id) {
        try { 
            $select = $this->db->prepare("SELECT email FROM entity WHERE entity_id = :id");
            $select->bindParam(":id", $id, PDO::PARAM_INT);
            $select->execute();
            Database::close();
            if ($select->rowCount() > 0) {
                $entity = $select->fetch();
                $res = explode(",", $entity["email"]);
                return $res;
            } else {
                return [];
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    function httpRequest($method, $target, $header, $body) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $target);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    
        if (strtolower($method) == "post")
        curl_setopt($ch, CURLOPT_POST, 1);
        else
        curl_setopt($ch, CURLOPT_POST, 0);
    
        if (isset($header))
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    
        if (isset($body))
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    
        $result = curl_exec($ch);

        $json = json_decode($result);
        curl_close($ch);
    
        return $json;
    }
}