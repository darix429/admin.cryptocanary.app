<?php

use App\Database;

class Vote {

	private static $_instance;

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
    }
    
    private function __construct() {
        $this->db = Database::getInstance()->getConnection();
    }

    public function getTotalVotesById($id) {
        
        $select = " "
        . "SELECT "
        . "review_id, "
        . "count(vote_id) total, "
        . "count(case when vote_type = 1 then 1 else null end) upvote, "
        . "count(case when vote_type = 0 then 1 else null end) downvote "
        . "FROM vote "
        . "WHERE review_id = ? "
        . "GROUP BY review_id; ";
        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute([$id]);
            Database::close();
            return $stmt->fetch();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        

    }

    public function getReviewUpvotesById($id) {
        
        $select = " "
        . " SELECT review_id, vote_id, user.username, vote_type "
        . " FROM vote INNER JOIN user ON vote.user_id = user.user_id "
        . " WHERE review_id = $id AND vote_type = 1 ";
        try {
            $stmt = $this->db->prepare($select);
            $stmt->execute([$id]);
            Database::close();
            return $stmt->fetchAll();
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        

    }

}