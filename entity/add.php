<?php 
    $Util = Util::getInstance();
    $countries = $Util->getCountries();
    $platforms = $Util->getPlatforms();
    $industries = $Util->getIndustries();
    $years = $Util->getYears();
?>


<a href="/entity"><strong>Back</strong></a>
<div class="row">
    <div class="col">
    <h2>Add new project</h2>
    </div>
    <div class="col text-right">
        <button class="btn bg-1 color-white submit-btn" form="new-entity-form">Submit</button>
    </div>
</div>

<br>


<?php if(isset($_GET['success']) || isset($_GET['error'])) { ?>
    <a href="/entity/?new" style="text-decoration: none;">
        <div class="status-msg <?php echo isset($_GET['success'])? 'very_legit':'very_shady' ?>">
            <?php echo isset($_GET['success'])? "<strong>Project created successfully</strong>": "<strong>Error:</strong> ".$_GET['error'] ?>
        </div>
    </a>
    <br>
<?php } ?>

<div class="alert alert-danger" hidden></div>

<div class="add-form">
<form id="new-entity-form" action="/entity/index.php?action=create" method="post" enctype="multipart/form-data" autocomplete="off">
    <div class="form-group row">
        <div class="col">Fork: <input type="checkbox" name="fork_flag"></div>
        <div class="col">ICO: <input type="checkbox" name="ico_flag"></div>
    </div>
    <div class="form-group">
        <label>Name: *</label>
        <input class="form-control" type="text" name="name" min-length=3 required>
    </div>
    <div class="form-group">
        <label>Symbol: *</label>
        <input class="form-control" type="text" name="symbol" min-length=2 required>
    </div>
    <div class="form-group">
        <label>Website: *</label>
        <input class="form-control" type="text" name="website" minlength=6 required>
    </div>
    <div class="form-group">
        <label>Description: </label>
        <input class="form-control" type="text" name="description">
    </div>
    <div class="form-group">
        <label>Year: </label>
        <select name="year" class="form-control">
            <?php 
                foreach($years as $val) {
                    echo "<option value='$val'>$val</option>";
                }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label>Industry: </label>
        <select name="industry" class="form-control">
            <?php 
                foreach($industries as $val) {
                    echo "<option value='$val'>$val</option>";
                }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label>Platform: </label>
        <select name="platform" class="form-control">
            <?php 
                foreach($platforms as $val) {
                    echo "<option value='$val'>$val</option>";
                }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label>Country: </label>
        <select name="country" class="form-control">
            <?php 
                foreach($countries as $val) {
                    echo "<option value='$val'>$val</option>";
                }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label>Image: * </label>
        <input type="file" name="image" accept="image/*" onchange="previewImage(event)">
    </div>
</form>
<div class="text-center img-preview-container">
    <img id='preview' alt="preview" style="height: 500px; display: none;">
</div>
</div>

<script type='text/javascript'>
    var cropper = null;
    var posX, posY;

    // image selection
    var previewImage = function(event) {
        var image = document.getElementById("preview");
        image.src = URL.createObjectURL(event.target.files[0]);
        
        if (cropper != null) {
            cropper.destroy();
        }
        cropper = new Cropper(image, {
            viewMode: 2,
            aspectRatio: 2/2,
            movable: false,
            zoomable: false,
            minCropBoxHeight: 30,
            minCropBoxWidth: 30,
            autoCropArea: 1,
            crop(event) {
                console.log(event.detail.x);
                console.log(event.detail.y);
                posX = event.detail.x;
                posY = event.detail.y;
            }
        });

        $(image).show();
    };

    // submit
    $(".submit-btn").on("click", (e)=> {
        e.preventDefault();
        $(this).attr("disabled", true);

        const form = $("#new-entity-form");
        const formData = form.serializeArray();
        const postUrl = form.attr("action");
        var error = "";

        var postDataString = "{";
        formData.forEach(data => {
            postDataString += '"'+data.name+'":"'+data.value+'",';
        });
        postDataString = postDataString.slice(0,-1);
        postDataString += "}";

        const testData = JSON.parse(postDataString);
        if (!testData.name) {
            error += "<li>Project name is required</li>";
        }
        if (!testData.symbol) {
            error += "<li>Project symbol is required</li>";
        }
        if (!testData.website) {
            error += "<li>Project website is required</li>";
        }
        if (!cropper) {
            error += "<li>Project image is required</li>";
        } else {
            // scale down large image
            const cropWidth = cropper.getData().width;
            console.log(cropWidth);
            cropper.moveTo(posX,posY);

            if (cropWidth > 500) {
                var scale = 100/cropWidth;
                cropper.scaleX(scale);
                cropper.scaleY(scale);
            }
        }

        if(error == "") {
            try {
                cropper.getCroppedCanvas().toBlob(function(blob) {
                    var postData = new FormData(document.getElementById("new-entity-form"));
                    postData.delete("image");
                    postData.append("image", blob, "image.png");
                    
                    // submit form
                    $.ajax({
                        url: postUrl,
                        type: "post",
                        data: postData,
                        processData: false,
                        contentType: false
                    }).done(function(res,status,xhr) {
                        var newId = res;
                        location.href = "/entity/?id=" + newId + "&success=New project added";
                        // console.log("SUBMITTED", res);
                    }).fail(function(xhr, status, er) {
                        console.log(action,er);
                    });
                });
            } catch (er) {
                console.log("ERROR", er);
            }
        } else {
            // alert(error);
            $(".alert").html("<ul>" +error+ "</ul>").attr("hidden",false);
        }
    });
</script>