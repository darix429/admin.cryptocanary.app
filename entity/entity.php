<?php
    $entity = Entity::getInstance()->getEntityById($id);
    if (empty($entity)) {
        header("Location: /entity");
    }
    $Util = Util::getInstance();
    $countries = $Util->getCountries();
    $platforms = $Util->getPlatforms();
    $industries = $Util->getIndustries();
    $years = $Util->getYears();

    require_once __CONTROLLERS . 'user.php';
    $userlist = User::getInstance()->getUserList();


    $links = json_decode($entity['links']);
    $website = $links->website;

    $status = "Pending";
    if (!empty($entity["delete_time"])) {
        $status = "Deleted";
    } else if ($entity["approved_flag"]) {
        $status = "Approved";
    }

    // confidence
    $confidence = "N/A";
    if ($entity["overall_rating"] != null)
        $confidence = number_format($entity["overall_rating"]); 
    $color = 'very_legit';
    if ($confidence <= 25) { $color = 'very_shady'; }
    else if ($confidence > 25 && $confidence <=50 ) { $color = 'lean_shady'; }
    else if ($confidence > 50 && $confidence <=75 ) { $color = 'neutral'; }
    else if ($confidence > 75) { $color = 'lean_legit'; }

    $avg_team_quality = $entity["average_team_quality"];
    $avg_info_quality = $entity["average_info_quality"];
    $avg_track_record = $entity["average_track_record"];

    
    require_once __CONTROLLERS . "review.php";
    $reviews = Review::getInstance()->getReviewsByEntityId($id);
    $list_from = 'entity';
?>

<div class="row">
    <div class="col">
        <a href="/entity" class="color-0"><strong>&#x2a1e; Back to project list</strong></a>
    </div>
    <?php if($entity['approved_flag']){ ?>
    <div class="col text-center">
        <button data-toggle="modal" data-target="#sendApprovedEmail">Send approved email</button>
        <button data-toggle="modal" data-target="#sendInvitationEmail" >Send invitation email</button>
    </div>
    <?php } ?>
    <div class="col text-right">
        <?php if ($status == 'Pending') { ?>
            <button class="entity-action-btn" action="approve" id="<?php echo $entity['entity_id'] ?>">Approve</button>
            <button class="entity-action-btn" action="permanent_remove" id="<?php echo $entity['entity_id'] ?>">Remove permanently</button>
        <?php } else if ($status == 'Deleted') { ?>
            <button class="entity-action-btn" action="restore" id="<?php echo $entity['entity_id'] ?>">Restore</button>
            <button class="entity-action-btn" action="permanent_remove" id="<?php echo $entity['entity_id'] ?>">Remove permanently</button>
        <?php } else if ($status == 'Approved') { ?>
            <button class="entity-action-btn" action="remove" id="<?php echo $entity['entity_id'] ?>">Remove</button>
            <a href="<?php echo WEBSITE_URL . "review/?id=" . $entity['entity_id'] ?>" target="_blank"><button>View on website</button></a>
        <?php } ?>
    </div>
</div>
<hr>
<h3>Project information</h3><br>

<!-- RETURN MESSAGES -->
<?php if(isset($_GET['success'])) { ?>
    <a href="/entity/?id=<?php echo $entity['entity_id'] ?>" style="text-decoration: none;">
        <div class="status-msg very_legit">
            <?php echo "<strong>". $_GET['success'] ."</strong>" ?>
        </div>
    </a>
    <br>
<?php } ?>
<?php if(isset($_GET['error'])) { ?>
    <a href="/entity/?id=<?php echo $entity['entity_id'] ?>" style="text-decoration: none;">
        <div class="status-msg very_shady">
            <?php echo "<strong>". $_GET['error'] ."</strong>" ?>
        </div>
    </a>
    <br>
<?php } ?>
<!-- /RETURN MESSAGES -->

<form action="/entity/index.php?action=update" method="post" id="updateForm">
    <input type="hidden" name="id" value="<?php echo $entity['entity_id'] ?>">
    <div class="info-section flex-container">
        <div class="text-center" style="width: 200px; ">
            <div class="progress" style="width: 150px; margin: 0 auto 10px auto;">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: <?php echo $confidence ?>%"><?php echo $confidence=="N/A"? "": "$confidence%" ?></div>
            </div>
            
            <img style="width: 150px;" src="<?php echo Entity::getInstance()->getIconLink($entity) ?>" alt="entity_icon">
            <button type="button" data-toggle="modal" data-target="#changeImage">change</button>

            <hr>
            <?php if (count($reviews) > 0) { ?>
                <div class="text-left">
                    <p style="font-size: 12px;"><label>Average Team Quality</label>
                        <span id="average_team_quality"></span> (<?php echo number_format($avg_team_quality, 2) ?>)
                    </p>
                    <p style="font-size: 12px;"><label>Average Info Quality</label>
                        <span id="average_info_quality"></span> (<?php echo number_format($avg_info_quality, 2) ?>)
                    </p>
                    <p style="font-size: 12px;"><label>Average Track Record</label>
                        <span id="average_track_record"></span> (<?php echo number_format($avg_track_record, 2) ?>)
                    </p>
                </div>
                <script type="text/javascript">
                $(function () {
                    $("#average_team_quality").rateYo({
                        rating: <?php echo $avg_team_quality ?>, readOnly: true, ratedFill: "#007bff", normalFill: "#ddd", starWidth: "16px"
                    });
                    $("#average_info_quality").rateYo({
                        rating: <?php echo $avg_info_quality ?>, readOnly: true, ratedFill: "#007bff", normalFill: "#ddd", starWidth: "16px"
                    });
                    $("#average_track_record").rateYo({
                        rating: <?php echo $avg_track_record ?>, readOnly: true, ratedFill: "#007bff", normalFill: "#ddd", starWidth: "16px"
                    });
                });
                </script>
            <?php }?>
        </div>
        <div class="grow">
            <div class="flex-container">
                <div style="padding: 0 10px; width: 33%">
                    <div class="form-group">
                        <label>Name: </label>
                        <input class="form-control" type="text" name="name" value="<?php echo ($entity['entity_name']) ?>">
                    </div>
                    <div class="form-group">
                        <label>Symbol</label>
                        <input class="form-control" type="text" name="symbol" value="<?php echo $entity['symbol'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Year</label>
                        <select name="year" class="form-control">
                            <?php 
                                foreach($years as $val) {
                                    echo "<option value='$val' ". (($entity['year'] == $val)? 'selected':'') .">$val</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div style="padding: 0 10px; width: 33%">
                    <div class="form-group">
                        <label>Industry: </label>
                        <select name="industry" class="form-control">
                            <?php 
                                foreach($industries as $val) {
                                    echo "<option value='$val' ". (($entity['industry'] == $val)? 'selected':'') .">$val</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Platform</label>
                        <select name="platform" class="form-control">
                            <?php 
                                foreach($platforms as $val) {
                                    echo "<option value='$val' ". (($entity['platform'] == $val)? 'selected':'') .">$val</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Country</label>
                        <select name="country" class="form-control">
                            <?php 
                                foreach($countries as $val) {
                                    echo "<option value='$val' ". (($entity['country'] == $val)? 'selected':'') .">$val</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div style="padding: 0 10px;" class="grow">
                    <div class="form-group">
                        <label>Submitted by:</label>
                        <input type="text" placeholder="User ID" class="form-control" name="user" list="userlist" value="<?php echo $entity['user_id'] .'-'. $entity['username'] . '-'.$entity['email'] ?>" required>
                        <datalist id="userlist">
                            <?php 
                            foreach($userlist as $user) {?>
                                <option value="<?php echo join("-",$user) ?>"></option>
                            <?php } ?>
                        </datalist>
                    </div>
                    <div class="form-group">
                        <label>Website</label>
                        <input class="form-control" type="text" name="website" value="<?php echo $website ?>">
                        <p class="text-right">
                        <?php if (!empty($links)) { ?>
                            <a class="color-0" style="text-decoration: underline;" href="<?php echo $website ?>" target="_blank">
                                <small>Visit</small>
                            </a>
                        <?php } ?>
                        </p>
                    </div>
                    <div class="form-group text-center">
                        <button type="button" data-toggle="modal" data-target="#links">Update Links</button>
                    </div>
                    <div class="form-group flex-container">
                        <div style="padding-right: 10px;"><label>Fork:</label></div>
                        <div class="grow"><input type="checkbox" name="fork_flag" <?php echo $entity['fork_flag']? "checked" : "" ?>></div>
                    </div>
                    <div class="form-group flex-container">
                        <div style="padding-right: 10px;"><label>ICO:</label></div>
                        <div class="grow"><input type="checkbox" name="ico_flag" <?php echo $entity['ico_flag']? "checked" : "" ?>></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div style="padding: 0 10px;">
                    <label>H1 Tag:</label>
                    <input class="form-control" type="text" name="tag1" value="<?php echo $entity['tag1'] ?>">
                </div>
            </div>
            <div class="form-group">
                <div style="padding: 0 10px;">
                    <label>Description:</label>
                    <input class="form-control" type="text" name="description" value="<?php echo $entity['entity_desc'] ?>">
                </div>
            </div>
            <div class="form-group">
                <div style="padding: 0 10px;">
                    <label>SEO Title:</label>
                    <input class="form-control" type="text" name="seo_title" value="<?php echo $entity['seo_title'] ?>">
                </div>   
                <div style="padding: 0 10px;">
                    <label>SEO Description:</label>
                    <input class="form-control" type="text" name="seo_description" value="<?php echo $entity['seo_description'] ?>">
                </div>   
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-primary" form="updateForm">Save project info</button>
    </div>
</form>
<hr>
<h3>Reviews</h3>
<div class="reviews-section">
    <?php 
        include_once "../review/card-list.php";
    ?>
</div>

<?php
    include_once "modals.php";
?>

<script type="text/javascript">
// email input
$(document).on("change", ".sendApprovedEmailInput", function(){
    var email = $(this).val();
    var value = email.split("-");
    if (value.length > 2) {
        value.splice(0,2);
        email = value.join('-');
    }

    $(this).val(email);    
});

// invite email selection
$(document).on("change", "input[name=emailSelect]", function(){
    var email = $(this).val();
    $(".sendInviteEmailInput").val(email);
});

var cropper;

// image selection
var previewImage = function(event) {
    var image = document.getElementById("preview");
    image.src = URL.createObjectURL(event.target.files[0]);
    
    if (cropper != null) {
        cropper.destroy();
    }
    cropper = new Cropper(image, {
        viewMode: 2,
        aspectRatio: 2/2,
        movable: false,
        zoomable: false,
        minCropBoxHeight: 30,
        minCropBoxWidth: 30,
        autoCropArea: 1,
        crop(event) {
            console.log(event.detail.x);
            console.log(event.detail.y);
            posX = event.detail.x;
            posY = event.detail.y;
        }
    });

    $(image).show();
};

// change image submit
var submitNewImage = function(event) {
    event.preventDefault();
    console.log("handle submit");
    try {
        const form = $("#change-image-form");
        const postUrl = form.attr("action");

        cropper.getCroppedCanvas().toBlob(function(blob) {
            var postData = new FormData(document.getElementById("change-image-form"));
            postData.delete("image");
            postData.append("image", blob, "image.png");
            
            // submit form
            $.ajax({
                url: postUrl,
                type: "post",
                data: postData,
                processData: false,
                contentType: false
            }).done(function(res,status,xhr) {
                var newId = res;
                location.href = window.location.href + "&success=Project image changed!";
            }).fail(function(xhr, status, er) {
                console.log(action,er);
            });
        });
    } catch (er) {
        console.log("ERROR", er);
    }
}
</script>