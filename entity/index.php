<?php

    require_once '../config.php';
    require_once __CONTROLLERS . 'entity.php';   
    require_once __CONTROLLERS . "util.php";
	require_once __CONTROLLERS . 'mail.php';

    
    $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
    $new = filter_input(INPUT_GET,'new',FILTER_SANITIZE_STRING);
    $action = filter_input(INPUT_GET,'action',FILTER_SANITIZE_STRING);

    if (!empty($action)) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if ($action == 'sendApprovedEmail') {
				Mailer::getInstance()->$action($_POST);
			} else if ($action == 'sendInvitationEmail') {

				$to = $_POST["email"];
				$entity_id = $_POST["entity_id"];
				$entity_name = $_POST["entity_name"];
				$entityIdname = strtolower($entity_name);
				$entityIdname = str_replace(" ", "+", $entityIdname);
				$link = "https://cryptocanary.app/review/$entityIdname";
				$emailData = array(
					"email"=>$to,
					"template"=>"invite.html",
					"subject"=>"You should get people to review $entity_name on CryptoCanary!",
					"data" => array(
						"entity_name"=>$entity_name,
						"link"=>$link
					)
				);
				if(Mailer::getInstance()->sendEmail($emailData)){
					$return_msg = "Invitation Email sent to $to";

					Entity::getInstance()->updateInviteFlag(array("id"=>$entity_id,"flag"=>1));

					header("Location: /entity/?id=$entity_id&success=$return_msg");
				} else {
					$er = "failed to send invite email";
					header("Location: /entity/?id=$entity_id&error=$er");
				}
			} else {
				Entity::getInstance()->$action($_POST);
			}
		} else {
			Entity::getInstance()->$action();
		}

		exit();
    }
?>

<?php include __SHARED . "head.php" ?>

<!-- cropper -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.js"></script>

<body>
    
	<div id="header" class="bg-orange">
		<div class="container">
				<div><img src="https://cryptocanary.app/img/logo.svg" alt="logo" draggable=false></div>
				<div class="text-right">
					<?php include __SHARED . "usermenu.php" ?>
				</div>
			</div>
		</div>
	</div>

	<div id="nav">
		<div class="container"><?php include __SHARED . "nav.php" ?></div>
	</div>

	<div id="body">
		<div class="container">
            <?php
            
                $_view = './list.php';
                if (!empty($id)) {
                    $_view = './entity.php';
                } else if (isset($_GET["new"])) {
					$_view = './add.php';
				}
                include $_view;
            
            ?>
		</div>
	</div>

	<div id="footer">

	</div>
    
</body>
</html>
<?php

include_once "../shared/modals.php";
include_once "../shared/end.php";

?>