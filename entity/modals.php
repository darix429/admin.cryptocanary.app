
<!-- Send Approved Email Modal -->
<div class="modal fade" id="sendApprovedEmail" tabindex="-1" role="dialog" aria-labelledby="sendApprovedEmail" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    <form action="/entity/index.php?action=sendApprovedEmail" method="post" id="emailForm">
      <div class="modal-header">
        <h5 class="modal-title" id="sendApprovedEmailLabel">Send Project Approved Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label>Email Address</label>
                <input type="email" name="email" class="form-control sendApprovedEmailInput" list="userlist" required value="<?php echo $entity['email'] ?>">
                <input type="hidden" name="entity_id" value="<?php echo $entity['entity_id'] ?>">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" form="emailForm">Send</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- Send Invitation Email Modal -->
<div class="modal fade" id="sendInvitationEmail" tabindex="-1" role="dialog" aria-labelledby="sendInvitationEmail" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    <form action="/entity/index.php?action=sendInvitationEmail" method="post" id="inviteForm">
      <div class="modal-header">
        <h5 class="modal-title" id="sendInvitationEmail">Send Invitation Email 
                <?php echo $entity["invite_flag"]? "(<span style='color:#00e33d; font-weight: bolder;'>**invited**</span>)":"" ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <?php
                
                    // $emailsDataListFile = "../data/entityEmails.json";
                    // $emailsCollection = file_exists($emailsDataListFile)? json_decode(file_get_contents($emailsDataListFile)) : [];
                    // if (!is_array($emailsCollection)) {
                    //     $emailsCollection = [$emailsCollection];
                    // }
                    // $emails = [];

                    // for ($i=0; $i<count($emailsCollection);$i++) {
                    //     $emailData = $emailsCollection[$i];
                    //     if ($emailData->entity_id == $entity["entity_id"]) {
                    //         $emails = $emailData->emails;
                    //         break;
                    //     }
                    // }

                    $emails = $Util->getEmailsByEntityId($entity["entity_id"]);
                    foreach($emails as $emailadd) {
                        $emailadd = str_replace('"',"", $emailadd);
                        echo "<label><input type='radio' name='emailSelect' value='$emailadd'> $emailadd</label><br>";
                    }
                    if( count($emails) == 0) {
                        echo "<i><small>'empty'</small></i>";
                    }

                ?>
            </div>
            <div class="form-group">
                <label>Email Address</label>
                <input type="email" name="email" class="form-control sendInviteEmailInput" list="userlist" required value="<?php echo $entity['email'] ?>">
                <input type="hidden" name="entity_name" value="<?php echo $entity['entity_name'] ?>">
                <input type="hidden" name="entity_id" value="<?php echo $entity['entity_id'] ?>">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" form="inviteForm">Send</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- Change Image Modal -->
<div class="modal fade" id="changeImage" tabindex="-1" role="dialog" aria-labelledby="changeImage" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
    <form action="/entity/index.php?action=changeImage" method="post" id="change-image-form" onsubmit="submitNewImage(event)">
      <div class="modal-header">
        <h5 class="modal-title" id="changeImageLabel">Change Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
        <div>
            <input type="file" name="newImage" id="newImageInp"  onchange="previewImage(event)">
            <input type="hidden" name="entity_id" value="<?php echo $entity["entity_id"] ?>">
        </div>
        <div class="preview" style="max-width: 720px; max-height: 600px;">
            <img src="" id="preview" alt="preview" style="display: none;">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary change-image-submit" form="change-image-form">Save</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- Update Links Modal -->
<div class="modal fade" id="links" tabindex="-1" role="dialog" aria-labelledby="links" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form action="/entity/index.php?action=updateLinks" method="post" id="links-form">
                <div class="modal-header">
                    <h5 class="modal-title">Update Links</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="<?php echo $entity["entity_id"] ?>">
                    <table class="table table-sm links-table">
                        <thead>
                            <tr>
                            <th style="width: 20%;">Name</th>
                            <th>Url</th>
                            <th style="width: 20%;">Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $links = json_decode($entity["links"]);
                                foreach($links as $link=>$url) {
                            ?>
                            <tr>
                                <td><input type="text" class="form-control" name="names[]" value="<?php echo $link ?>"></td>
                                <td><input type="text" class="form-control" name="urls[]" value="<?php echo $url ?>"></td>
                                <td><button class="close del-row-btn" type="button"><span aria-hidden="true">&times;</span></button></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                            <td colspan="3">
                            <button type="button" class="add-row-btn" style="border: none; background: #ddd; color: #555; font-size: 13px; font-weight: 1000; outline: none; width: 100%; padding: 10px 0px;">
                                ADD
                            </button>
                            </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary submit-btn" form="links-form">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).on("click", ".add-row-btn", function() {
        $(".links-table tbody tr:last").after("<tr><td><input type='text' name='names[]' class='form-control'></td><td><input type='text' name='urls[]' class='form-control'></td><td><button class='close del-row-btn' type='button'><span aria-hidden='true'>&times;</span></button></td></tr>");
    });
    $(document).on("click", ".del-row-btn", function() {
        $(this).closest("tr").remove();
    });

</script>