<?php
	require './config.php';
	include __SHARED . "head.php" 
?>
<body>
<?php 

	if (isset($_SESSION["user"])) {
		header("Location: /summary");
	}
	$error = filter_input(INPUT_GET,'error',FILTER_SANITIZE_STRING);

	if ($error) {
		echo "<div class='login-error'>Error: $error</div>";
	}
?>

<div class="login">
	<div class="content">
		<div class="text-center"><img src="/assets/images/logo.png" alt="logo"></div>
		<div class="text-center"><h3>Admin login</h3></div>
		<br><br>
		<form action="/user/?action=login" method="POST" role="form">
			<div class="form-group">
				<label class="form-control-label" for="email">Username</label>
				<input type="text" name="username" id="email" class="form-control" placeholder="username" autofocus>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="password">Password</label>
				<input type="password" name="password" id="password" class="form-control" placeholder="Password">
			</div>
			<div class="row">
				<div class="col"><button type="submit" class="btn btn-primary">Login</button></div>
				<div class="col text-right"><a class="color-1" href="<?php echo WEBSITE_URL ?>">Go to website</a></div>
			</div>
		</form>
	</div>
</div>

</body>
</html>
