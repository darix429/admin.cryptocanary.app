<div class="text-right">
</div>
    <?php 

        foreach($reviews as $review){ 
            $deleted = !empty($review["delete_time"]) ? true : false;
            $reviewer = $review["username"];
            $reviewed = $review["product_name"];
            // $vote = Vote::getInstance()->getTotalVotesById($review["review_id"]);
            $upvote = !empty($review["upvote_tally"])? $review["upvote_tally"] : 0;
            $downvote = !empty($review["downvote_tally"])? $review["downvote_tally"] : 0;
            $list_from="product";

        ?>
            <div class="review-card <?php echo $deleted? 'deleted-bg': ''?>" <?php echo $deleted? 'hidden':'' ?>>
                <div class="flex-container card-head">
                    <div class="">
                        <strong><?php echo $reviewer ?></strong>
                        <i>rated</i>
                        <strong><?php echo $reviewed ?></strong>
                        <?php 
                            include "../review/ratings-product.php";
                        ?>
                        <i>on</i>
                        <?php echo date_format(date_create($review['update_time']), "M d, Y") ?>
                    </div>
                    <div class="grow text-right">
                        <a href="/review/?id=<?php echo $review["review_id"] ?>&type=<?php echo $list_from ?>"><strong style="color: #00a2ee">Edit</strong></a> |
                        <a class="delete-review-btn" data-toggle="modal" data-target="#deleteReview" id="<?php echo $review["review_id"] ?>" review-type="<?php echo $list_from ?>"><strong style="color: #f55; cursor:pointer;">Delete</strong></a>
                    </div>
                </div>
                <div class="card-body review">
                    <?php echo $review["review"] ?>
                </div>
                <div class="flex-container card-foot">
                    <div style="width: 100px;">Upvote: <strong><?php echo $upvote ?></strong></div>
                    <div>Downvote: <strong><?php echo $downvote ?></strong></div>
                </div>
                <div>
                    <?php
                        include "../review/comment-card-list.php";
                    ?>
                </div>
            </div>
        <?php } ?>

        <?php if (count($reviews) == 0) { ?>
            <div class="review-card deleted-bg">
                This project has zero reviews
            </div>
        <?php } ?>