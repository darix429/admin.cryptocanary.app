<?php 
    require_once '../config.php';
    require_once __CONTROLLERS . 'mail.php';   
    require_once __CONTROLLERS . 'product-review.php';  
    
    $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
    $action = filter_input(INPUT_GET,'action',FILTER_SANITIZE_STRING);

    if (!empty($action)) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        	ProductReview::getInstance()->$action($_POST);
        else
            ProductReview::getInstance()->$action();
		exit();
    }
?>