<?php 
    $Util = Util::getInstance();
    $countries = $Util->getCountries();
    $platforms = $Util->getPlatforms();
    $industries = $Util->getIndustries();
    $years = $Util->getYears();

    $Category = Category::getInstance();
    $categories = $Category->getCollection();
    $ecosystems = $Category->getEcosystems();
?>


<a href="/product"><strong>Back</strong></a>
<div class="row">
    <div class="col">
    <h2>Add new product</h2>
    </div>
    <div class="col text-right">
        <button class="btn bg-1 color-white submit-btn" form="new-product-form">Submit</button>
    </div>
</div>

<br>


<?php if(isset($_GET['success']) || isset($_GET['error'])) { ?>
    <a href="/product/?new" style="text-decoration: none;">
        <div class="status-msg <?php echo isset($_GET['success'])? 'very_legit':'very_shady' ?>">
            <?php echo isset($_GET['success'])? "<strong>Product created successfully</strong>": "<strong>Error:</strong> ".$_GET['error'] ?>
        </div>
    </a>
    <br>
<?php } ?>

<div class="alert alert-danger" hidden></div>

<div style="width: 100%; border: solid 1px #dadada; border-radius: 10px; padding: 10px; background: #fefefe">
<form id="new-product-form" action="/product/index.php?action=create" method="post" enctype="multipart/form-data" autocomplete="off">
    <div class="flex-container">
        <div class="col">
            <h4>Info</h4>
            <div class="form-group">
                <label>Category: *</label>
                <select name="category" class="form-control">
                    <?php 
                        foreach($categories as $val) {
                            echo "<option value='".$val["entity_category_id"]."'>".$val["category"]."</option>";
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>Ecosystem: *</label>
                <select name="ecosystem" class="form-control">
                    <?php 
                        foreach($ecosystems as $val) {
                            echo "<option value='".$val."'>".$val."</option>";
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>Name: *</label>
                <input class="form-control" type="text" name="name" min-length=3 required>
            </div>
            <div class="form-group">
                <label>Short Description: </label>
                <input class="form-control" type="text" name="short_desc">
            </div>
            <div class="form-group">
                <label>Description: </label>
                <input class="form-control" type="text" name="desc">
            </div>
            <h4>SEO</h4>
            <div class="form-group">
                <label>H1 Tag: </label>
                <input class="form-control" type="text" name="tag1">
            </div>
            <div class="form-group">
                <label>SEO title: </label>
                <input class="form-control" type="text" name="seo_title">
            </div>
            <div class="form-group">
                <label>SEO description: </label>
                <input class="form-control" type="text" name="seo_description">
            </div>
        </div>
        <div class="col">
            <h4>Links</h4>
            <div class="form-group">
                <label>Website: *</label>
                <input class="form-control" type="text" name="website" minlength=6 required>
            </div>
            <div class="form-group">
                <label>Facebook</label>
                <input class="form-control" type="text" name="facebook" minlength=6 required>
            </div>
            <div class="form-group">
                <label>Twitter</label>
                <input class="form-control" type="text" name="twitter" minlength=6 required>
            </div>
            <div class="form-group">
                <label>Youtube</label>
                <input class="form-control" type="text" name="youtube" minlength=6 required>
            </div>
            <div class="form-group">
                <label>Medium</label>
                <input class="form-control" type="text" name="medium" minlength=6 required>
            </div>
            <div class="form-group">
                <label>Telegram</label>
                <input class="form-control" type="text" name="telegram" minlength=6 required>
            </div>
            <div class="form-group">
                <label>Reddit</label>
                <input class="form-control" type="text" name="reddit" minlength=6 required>
            </div>
        </div>
    </div>
    <hr>
    <h4>Images</h4>
    <div class="flex-container">
    <div class="form-group col">
        <label>Company Logo: * </label><br>
        <input type="file" name="companyLogo" accept="image/*" onchange="previewImage(0,event)">
        <div class="text-center img-preview-container">
            <img id='companyLogoPreview' alt="preview" style="width: 100%; display: none;">
        </div>
    </div>
    <div class="form-group col">
        <label>Product Photo: * </label><br>
        <input type="file" name="productPhoto" accept="image/*" onchange="previewImage(1,event)">
        <div class="text-center img-preview-container">
            <img id='productPhotoPreview' alt="preview" style="width: 100%; display: none;">
        </div>
    </div>
    </div>
</form>
</div>

<script type='text/javascript'>
    var cropper=[];
    var posX=[], posY=[];

    // image selection
    var previewImage = function(idx, event) {
        var image;
        if (idx == 0) {
            image = document.getElementById("companyLogoPreview");
        } else if (idx == 1) {
            image = document.getElementById("productPhotoPreview")
        }
        image.src = URL.createObjectURL(event.target.files[0]);

        console.log(idx, image.src);
        
        if (cropper[idx] != null) {
            cropper[idx].destroy();
        }
        cropper[idx] = new Cropper(image, {
            viewMode: 2,
            movable: false,
            zoomable: false,
            minCropBoxWidth: 30,
            autoCropArea: 1,
            crop(event) {
                posX[idx] = event.detail.x;
                posY[idx] = event.detail.y;
            }
        });

        $(image).show();
    };

    // submit
    $(".submit-btn").on("click", (e)=> {
        $("#loading").show();
        e.preventDefault();
        $(this).attr("disabled", true);

        const form = $("#new-product-form");
        const formData = form.serializeArray();
        const postUrl = form.attr("action");
        var error = "";

        var postDataString = "{";
        formData.forEach(data => {
            postDataString += '"'+data.name+'":"'+data.value+'",';
        });
        postDataString = postDataString.slice(0,-1);
        postDataString += "}";

        const testData = JSON.parse(postDataString);
        if (!testData.name) {
            error += "<li>Product name is required</li>";
        }
        if (!testData.website) {
            error += "<li>Product website is required</li>";
        }
        if (!cropper[0]) {
            error += "<li>Company logo is required</li>";
        } else {
            // scale down large image
            const cropWidth = cropper[0].getData().width;
            cropper[0].moveTo(posX,posY);

            if (cropWidth > 500) {
                var scale = 100/cropWidth;
                cropper[0].scaleX(scale);
                cropper[0].scaleY(scale);
            }
        }
        if (!cropper[1]) {
            error += "<li>Product photo is required</li>";
        } else {
            // scale down large image
            const cropWidth = cropper[1].getData().width;
            cropper[1].moveTo(posX,posY);

            if (cropWidth > 500) {
                var scale = 100/cropWidth;
                cropper[1].scaleX(scale);
                cropper[1].scaleY(scale);
            }
        }

        if(error == "") {
            try {
                var postData = new FormData(document.getElementById("new-product-form"));
                cropper[0].getCroppedCanvas().toBlob(function(blob) {
                    postData.delete("companyLogo");
                    postData.append("companyLogo", blob, "image.png");
                });
                cropper[1].getCroppedCanvas().toBlob(function(blob) {
                    postData.delete("productPhoto");
                    postData.append("productPhoto", blob, "image.png");
                });                    
                // submit form
                $.ajax({
                    url: postUrl,
                    type: "post",
                    data: postData,
                    processData: false,
                    contentType: false
                }).done(function(res,status,xhr) {
                    var newId = res;
                    console.log(res);
                    location.href = "/product/?id=" + newId + "&success=New Product added";
                }).fail(function(xhr, status, er) {
                    console.log("POST ERROR",er);
                    $("#loading").hide();
                    switch (xhr.status) {
                        case 413: {
                            alert("image/s are too large.");
                            break;
                        }
                        default: {
                            alert("failed to submit new product");
                            break;
                        }
                    }
                });
            } catch (er) {
                console.log("ERROR", er);
            }
        } else {
            // alert(error);
            $(".alert").html("<ul>" +error+ "</ul>").attr("hidden",false);
        }
    });
</script>