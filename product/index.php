<?php
    
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
    require_once '../config.php';
    require_once __CONTROLLERS . 'product.php';   
    require_once __CONTROLLERS . 'category.php';   
    require_once __CONTROLLERS . "user.php";
    require_once __CONTROLLERS . "util.php";
	require_once __CONTROLLERS . 'mail.php';


    $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
    $new = filter_input(INPUT_GET,'new',FILTER_SANITIZE_STRING);
	$action = filter_input(INPUT_GET,'action',FILTER_SANITIZE_STRING);
	
	
    if (!empty($action)) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if ($action == 'sendProductApprovedEmail') {
				Mailer::getInstance()->sendProductApprovedEmail($_POST);
			} else if ($action == 'sendInvitationEmail') {
				Mailer::getInstance()->sendProductInvitationEmail($_POST);
			} else {
				Product::getInstance()->$action($_POST);
			}
		} else {
			Product::getInstance()->$action();
		}
		exit();
	}

?>


<?php include __SHARED . "head.php" ?>

<!-- cropper -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.js"></script>

<body>
    
	<div id="header" class="bg-orange">
		<div class="container">
				<div><img src="https://cryptocanary.app/img/logo.svg" alt="logo" draggable=false></div>
				<div class="text-right">
					<?php include __SHARED . "usermenu.php" ?>
				</div>
			</div>
		</div>
	</div>

	<div id="nav">
		<div class="container"><?php include __SHARED . "nav.php" ?></div>
	</div>

	<div id="body">
		<div class="container">
            <?php
            
                $_view = './list.php';
                if (!empty($id)) {
                    $_view = './product.php';
                } else if (isset($_GET["new"])) {
					$_view = './add.php';
				}
                include $_view;
            
            ?>
		</div>
	</div>

	<div id="footer">

	</div>

</body>

<?php

include_once "../shared/modals.php";
include_once "../shared/end.php";

?>