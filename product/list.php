<?php 

    $filter = [
        "page"=>0,
        "size"=>15,
        "sort"=>'update_time',
        "direction"=>"desc",
        "search"=>""
    ];
    $page = filter_input(INPUT_GET,'page',FILTER_SANITIZE_STRING);
    $size = filter_input(INPUT_GET,'size',FILTER_SANITIZE_STRING);
    $sort = filter_input(INPUT_GET,'sort',FILTER_SANITIZE_STRING);
    $direction = filter_input(INPUT_GET,'direction',FILTER_SANITIZE_STRING);
    $search = filter_input(INPUT_GET,'search',FILTER_SANITIZE_STRING);
    $filterEmails = filter_input(INPUT_GET,'filter-emails',FILTER_SANITIZE_STRING);

    
    $toggle_direction = $filter["direction"];
    $pagination = "/product?";
    if(!empty($page)) { 
        $page --;
        if ($page > 0) { $page = $page * $filter["size"]; }
        $filter['page'] = $page;
    }
    if(!empty($size)) { 
        $filter['size'] = $size;
        $pagination .= "&size=$size";
    }
    if(!empty($sort)) { 
        $filter['sort'] = $sort; 
        $pagination .= "&sort=$sort";
    }
    if(!empty($direction)) { 
        $filter['direction'] = $direction; 
        $pagination .= "&direction=$direction";

        if (strtolower($direction) == 'asc')
            $toggle_direction = "desc";
        else
            $toggle_direction = "asc";
    }
    if(!empty($search)) { 
        $filter['search'] = $search; 
    }
    if(!empty($filterEmails)) {
        $filter["filter-emails"] = $filterEmails;
    } else {
        $filter["filter-emails"] = false;
    }

    function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close($ch);
        if($result !== FALSE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    $datasource = Product::getInstance()->getCollection($filter);
    $collection = $datasource["collection"];
?>
<h2>Products</h2>
<br>

<!-- RETURN MESSAGES -->
<?php if(isset($_GET['success'])) { ?>
    <a href="/product/" style="text-decoration: none;">
        <div class="status-msg very_legit">
            <?php echo "<strong>". $_GET['success'] ."</strong>" ?>
        </div>
    </a>
    <br>
<?php } ?>
<?php if(isset($_GET['error'])) { ?>
    <a href="/product/" style="text-decoration: none;">
        <div class="status-msg very_shady">
            <?php echo "<strong>". $_GET['error'] ."</strong>" ?>
        </div>
    </a>
    <br>
<?php } ?>
<!-- /RETURN MESSAGES -->
<div class="row">
    <div class="col">
        <form action="/product">
            <div class="search-group">
                <input type="text" name="search" autocomplete="off" value="<?php echo $search ?>"><button type="submit">Search</button>
            </div>
            
        </form>
    </div>
    <div class="col text-right">
        <a href="/product/?action=approveAll"><button>Approve All</button></a> :: 
        <button data-toggle="modal" data-target="#importCSV">Import CSV</button>
        <a href="/product/?new"><button>Add Product</button></a>
    </div>
</div>
<div class="text-center">
    <?php 
        if (!empty($search)) { 
            if (count($collection) > 0)
            echo "<i>". count($collection) ." results found for '<span class='color-1'>$search</span>'</i>";
            else 
            echo "<i>nothing found for '<span class='color-1'>$search</span>'</i>";

            echo !empty($search)? "<br><a href='/product' class='color-0'><strong>clear results</strong></a>": "";
        }  
    ?>
</div>
<br>
<table class="table">
    <thead>
        <tr>
            <th><a href="?sort=product_id&direction=<?php echo $toggle_direction ?>" draggable=false>ID</a></th>
            <th>Icon</th>
            <th><a href="?sort=product_name&direction=<?php echo $toggle_direction ?>" draggable=false>Name</a></th>
            <th><a href="?sort=category_name&direction=<?php echo $toggle_direction ?>" draggable=false>Category</a></th>
            <th><a href="?sort=review_count&direction=<?php echo $toggle_direction ?>" draggable=false>Review Count</a></th>
            <th><a href="?sort=create_time&direction=<?php echo $toggle_direction ?>" draggable=false>Date Added</a></th>
            <th><a href="?sort=recent_activity&direction=<?php echo $toggle_direction ?>" draggable=false>Recent Activity</a></th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($collection as $data) {
            $status = "Pending";
            if (!empty($data["delete_time"])) {
                $status = "Deleted";
            } else if ($data["approved_flag"]) {
                $status = "Approved";
            }
        ?>
            <tr>
                <td><?php echo $data["product_id"] ?></td>
                <td><a href="/product/?id=<?php echo $data["product_id"] ?>"><img src="<?php echo product_img_src . $data["product_id"] ."/". $data["logo"] ?>" alt="icon"></a></td>
                <td><a href="/product/?id=<?php echo $data["product_id"] ?>"><?php echo $data["product_name"] ?></a></td>
                <td><?php echo $data["category_name"] ?></td>
                <td><?php echo $data["review_count"] ?></td>
                <td><?php echo date_format(date_create($data['create_time']), "M d, Y") ?></td>
                <td><?php echo date_format(date_create($data['recent_activity']), "M d, Y") ?></td>
                <td class="text-center">
                    <?php if ($status == 'Pending') { ?>
                        <button class="product-action-btn" id="<?php echo $data['product_id'] ?>" action="approve" title="Approve" style="font-size: 20px; color: #32BA7C"><i class="fas fa-thumbs-up"></i></button>
                        <button class="product-action-btn" id="<?php echo $data['product_id'] ?>" action="permanent_remove" title="Hard delete" style="font-size: 20px; color: #C40606"><i class="fas fa-trash-alt"></i></button>
                        
                    <?php } else if ($status == 'Approved') { ?>
                        <button class="product-action-btn" id="<?php echo $data['product_id'] ?>" action="remove" title="Soft delete" style="font-size: 20px; color: #E76E54"><i class="fas fa-backspace"></i></button>
                    <?php } else if ($status == 'Deleted') { ?>
                        
                        <button class="product-action-btn" id="<?php echo $data['product_id'] ?>" action="restore" title="Restore" style="font-size: 20px; color: #05BEA6"><i class="fas fa-redo"></i></button>
                        <button class="product-action-btn" id="<?php echo $data['product_id'] ?>" action="permanent_remove" title="Hard delete" style="font-size: 20px; color: #C40606"><i class="fas fa-trash-alt"></i></button>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<hr>
<div class="pages text-center">
    <?php 


        $pagewidth = 10;
        $pagemax = ceil($datasource["max_size"] / $filter["size"]);

        $selected = ($page / $filter["size"]) + 1;
        $pagestart = 1;
        $pageend = $pagewidth;
        
        if ($pageend > $pagemax)
        $pageend = $pagemax;

        if ($pageend != 0) {
            while ($pageend < $selected) {
                $pagestart = $pageend + 1;
                $pageend = ($pagestart + ($pagewidth-1));
                if ($pageend > $pagemax) {
                    $pageend = $pagemax;
                }
            }
        }


        if ($selected > $pagewidth) {
            $startlink = $pagination . "&page=1";
            $backlink = $pagination . "&page=" . ($pagestart-1);
            if (!empty($filterEmails)) {
                $startlink .= "&filter-emails=true";
                $backlink .= "&filter-emails=true";
            }

            echo "<a href='$startlink'><button title='Back to start'>&lt;&lt;</button></a>";
            echo "<a href='$backlink'><button title='Back'>&lt;</button></a>  ";
        }

        if (empty($search)) {
            for($i = $pagestart; $i<=$pageend; $i++) {
                $page_link = $pagination . "&page=$i";
                if (!empty($filterEmails)) {
                    $page_link .= "&filter-emails=true";
                }
                echo "<a href='$page_link'><button ".(($selected == $i )? 'disabled' : '').">$i</button></a>";
            }
        }
        
        if ($pageend < $pagemax && empty($search)) {
            $forwardlink = $pagination . "&page=" . ($pageend+1);
            $endlink = $pagination . "&page=$pagemax";
            if (!empty($filterEmails)) {
                $forwardlink .= "&filter-emails=true";
                $endlink .= "&filter-emails=true";
            }

            echo "  <a href='$forwardlink'><button title='Next'>&gt;</button></a>";
            echo "<a href='$endlink'><button title='To end'>&gt;&gt;</button></a>";
        }

    ?>
</div>

<?php
    include_once(__ROOT . "/shared/tools.php");
?>

<script type="text/javascript">
    $(document).on("change","#filter-withemails", function(e){
        var url = location.href;
        if(this.checked) {
            location.href = location.protocol + "//" + location.host + "/product/?filter-emails=true";
        } else {
            var target = url.replace("&filter-emails=true", "");
            target = target.replace("?filter-emails=true", "");
            location.href = target;
        }
    });
</script>