

<!-- Send Approved Email Modal -->
<div class="modal fade" id="sendApprovedEmail" tabindex="-1" role="dialog" aria-labelledby="sendApprovedEmail" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    <form action="/product/index.php?action=sendProductApprovedEmail" method="post" id="emailForm">
      <div class="modal-header">
        <h5 class="modal-title" id="sendApprovedEmailLabel">Send Product Approved Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label>Email Address</label>
                <input type="email" name="email" class="form-control sendApprovedEmailInput" list="userlist" required value="<?php echo $product['email'] ?>">
                <input type="hidden" name="id" value="<?php echo $product['product_id'] ?>">
                <input type="hidden" name="name" value="<?php echo $product['product_name'] ?>">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" form="emailForm">Send</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- Links Modal -->
<div class="modal fade" id="productLinks" tabindex="-1" role="dialog" aria-labelledby="productLinks" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    <form action="/product/?index.php&action=updateLinks" method="post" id="linksForm">
      <input type="hidden" name="id" value="<?php echo $id ?>">
      <div class="modal-header">
        <h5 class="modal-title" id="productLinksLabel">Links</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
              <table class="table table-sm links-table">
                <thead>
                  <th>Name</th>
                  <th>Url</th>
                  <th style="width: 10px;">&nbsp;</th>
                </thead>
                <tbody>
                <?php
                    $links = json_decode($product["product_links"]);
                    if (!empty($links)) {
                    foreach($links as $k=>$v) { ?>
                    <tr>
                      <td><input type="text" name="names[]" value="<?php echo $k?>"></td>
                      <td><input type="text" name="urls[]" value="<?php echo $v?>"></td>
                      <td><button class='close del-row-btn' type='button'><span aria-hidden='true'>&times;</span></button></td>
                    </tr>
                <?php }} ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="3"><button type="button" class="add-row-btn" style="width: 100%; background: #ddd; font-weight: 1000; font-size: 12px; padding: 10px;">Add</button></td>
                  </tr>
                </tfoot>
              </table>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" form="linksForm">Send</button>
      </div>
    </form>
    </div>
  </div>
</div>


<!-- Change Image Modal -->
<div class="modal fade" id="changeImage" tabindex="-1" role="dialog" aria-labelledby="changeImage" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
    <form action="/product/index.php?action=changeImage" method="post" id="change-image-form" onsubmit="submitNewImage(event)" enctype="multipart/form-data">
      <div class="modal-header">
        <h5 class="modal-title" id="changeImageLabel">Change Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
            <input type="file" name="image" id="newImageInp"  onchange="previewImage(event)">
            <input type="hidden" name="product_id" value="<?php echo $product["product_id"] ?>">
            <input type="hidden" name="image_type">
        </div>
        <div class="preview" style="max-width: 720px; max-height: 600px;">
            <img src="" id="preview" alt="preview" style="display: none;">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary change-image-submit" form="change-image-form">Save</button>
      </div>
    </form>
    </div>
  </div>
</div>


<!-- Send Invitation Email Modal -->
<div class="modal fade" id="sendInvitationEmail" tabindex="-1" role="dialog" aria-labelledby="sendInvitationEmail" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    <form action="/product/index.php?action=sendInvitationEmail" method="post" id="inviteForm">
      <div class="modal-header">
        <h5 class="modal-title" id="sendInvitationEmail">Send Invitation Email 
                <?php echo $product["invite_flag"]? "(<span style='color:#00e33d; font-weight: bolder;'>**invited**</span>)":"" ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <?php
                
                    $emailsDataListFile = "../scrape/getProductEmails/collection.json";
                    $emailsCollection = file_exists($emailsDataListFile)? json_decode(file_get_contents($emailsDataListFile)) : [];
                    if (!is_array($emailsCollection)) {
                        $emailsCollection = [$emailsCollection];
                    }
                    $emails = [];

                    for ($i=0; $i<count($emailsCollection);$i++) {
                        $emailData = $emailsCollection[$i];
                        if ($emailData->product_id == $product["product_id"]) {
                            $emails = $emailData->emails;
                            break;
                        }
                    }
                    foreach($emails as $emailadd) {
                        $emailadd = str_replace('"',"", $emailadd);
                        echo "<label><input type='radio' name='emailSelect' value='$emailadd'> $emailadd</label><br>";
                    }
                    if( count($emails) == 0) {
                        echo "<i><small>'empty'</small></i>";
                    }

                ?>
            </div>
            <div class="form-group">
                <label>Email Address</label>
                <input type="email" name="email" class="form-control sendInviteEmailInput" list="userlist" required value="<?php echo $product['email'] ?>">
                <input type="hidden" name="name" value="<?php echo $product['product_name'] ?>">
                <input type="hidden" name="id" value="<?php echo $product['product_id'] ?>">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" form="inviteForm">Send</button>
      </div>
    </form>
    </div>
  </div>
</div>


<script>
    $(document).on("click", ".add-row-btn", function() {
        $(".links-table tbody tr:last").after("<tr><td><input type='text' name='names[]'></td><td><input type='text' name='urls[]'></td><td><button class='close del-row-btn' type='button'><span aria-hidden='true'>&times;</span></button></td></tr>");
    });
    $(document).on("click", ".del-row-btn", function() {
        $(this).closest("tr").remove();
    });

</script>