<?php
    $product = Product::getInstance()->getProductById($id);
    if (empty($product)) {
        header("Location: /product");
    }

    $status = "Pending";
    if (!empty($product["delete_time"])) {
        $status = "Deleted";
    } else if ($product["approved_flag"]) {
        $status = "Approved";
    }

    // 
    require_once __CONTROLLERS . "product-review.php";
    $reviews = ProductReview::getInstance()->getReviewsByProductId($id);
    $list_from = 'product';

    //
    $userlist = User::getInstance()->getUserList();
?>

<div class="row">
    <div class="col">
        <a href="/product" class="color-0"><strong>&#x2a1e; Back to product list</strong></a>
    </div>
    <?php if($product['approved_flag']){ ?>
    <div class="col text-center">
        <button data-toggle="modal" data-target="#sendApprovedEmail">Send approved email</button>
        <button data-toggle="modal" data-target="#sendInvitationEmail" >Send invitation email</button>
    </div>
    <?php } ?>
    <div class="col text-right">
        <?php if ($status == 'Pending') { ?>
            <button class="product-action-btn" action="approve" id="<?php echo $product['product_id'] ?>">Approve</button>
            <button class="product-action-btn" action="permanent_remove" id="<?php echo $product['product_id'] ?>">Remove permanently</button>
        <?php } else if ($status == 'Deleted') { ?>
            <button class="product-action-btn" action="restore" id="<?php echo $product['product_id'] ?>">Restore</button>
            <button class="product-action-btn" action="permanent_remove" id="<?php echo $product['product_id'] ?>">Remove permanently</button>
        <?php } else if ($status == 'Approved') { ?>
            <button class="product-action-btn" action="remove" id="<?php echo $product['product_id'] ?>">Remove</button>
            <a href="<?php echo WEBSITE_URL . "discover/". str_replace(" ","+",$product["product_name"]) ?>" target="_blank"><button>View on website</button></a>
        <?php } ?>
    </div>
</div>
<hr>
<h3>Product information</h3><br>

<!-- RETURN MESSAGES -->
<?php if(isset($_GET['success'])) { ?>
    <a href="/product/?id=<?php echo $product['product_id'] ?>" style="text-decoration: none;">
        <div class="status-msg very_legit">
            <?php echo "<strong>". $_GET['success'] ."</strong>" ?>
        </div>
    </a>
    <br>
<?php } ?>
<?php if(isset($_GET['error'])) { ?>
    <a href="/product/?id=<?php echo $product['product_id'] ?>" style="text-decoration: none;">
        <div class="status-msg very_shady">
            <?php echo "<strong>". $_GET['error'] ."</strong>" ?>
        </div>
    </a>
    <br>
<?php } ?>
<!-- /RETURN MESSAGES -->

<form action="/product/index.php?action=update" method="post" id="updateForm">
<input type="hidden" name="id" value="<?php echo $id ?>">
<div class="flex-container">
    <div style="width: 200px;">
        <label>Company Logo</label>
        <?php
            $logoSplit = explode(".",$product["logo"]);
            $logo = $logoSplit[0]."-orig". ((count($logoSplit)>1)? ".".$logoSplit[1]:"");
        ?>
        <img src="<?php echo product_img_src . $id . "/". $logo ?>" alt="product-image" style="width: 100%; max-height: 200px;" class="change-prod-img-btn" img-type="company-logo" data-toggle="modal" data-target="#changeImage">
        <hr>
        <label>Product Photo</label>
        <img src="<?php echo product_img_src . $id . "/". $product["image"] ?>" alt="product-logo" style="width: 100%; max-height: 200px;" class="change-prod-img-btn" img-type="product-photo" data-toggle="modal" data-target="#changeImage">
    </div>
    <div style="width: 100%;">
        <div class="flex-container">
            <div style="padding: 0 10px;">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="product_name" value="<?php echo $product["product_name"] ?>">
                </div>
            </div>
            <div style="padding: 0 10px;">
                <div class="form-group">
                    <label>Website <small>(<a href="<?php echo $product["website"] ?>" style="text-decoration: underline" target="_blank">visit</a> )</small></label>
                    <input type="text" class="form-control" name="website" value="<?php echo $product["website"] ?>">
                </div>
            </div>
            <div style="padding: 0 10px;">
                <div class="form-group">
                    <label>Submitted By</label>
                    <input type="text" class="form-control" placeholder="User ID" name="user" list="userlist" value="<?php echo $product['user_id'] .'-'. $product['username'] . '-'.$product['email'] ?>" required>
                    <datalist id="userlist">
                        <?php 
                        foreach($userlist as $user) {?>
                            <option value="<?php echo join("-",$user) ?>"></option>
                        <?php } ?>
                    </datalist>
                </div>
            </div>
            <div style="padding: 0 10px;">
                <div class="form-group">
                    <label>Links</label><br>
                    <button type="button" data-toggle="modal" data-target="#productLinks">Update links</button>
                </div>
            </div>
        </div>

        <div class="flex-container">
            <div style="padding: 0 10px;">
                <div class="form-group">
                    <label>Category</label>
                    <select name="category" id="category" class="form-control">
                        <?php 
                            $Category = Category::getInstance();
                            $categories = $Category->getCollection();
                            foreach($categories as $category) {
                                echo "<option value='". $category["entity_category_id"] ."' ".($category["entity_category_id"]==$product["category"]? 'selected':'').">". $category["category"] ."</option>";
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div style="padding: 0 10px;">
                <div class="form-group">
                    <label>Ecosystem</label>
                    <select name="ecosystem" id="ecosystem" class="form-control">
                        <?php 
                            $ecosystems = $Category->getEcosystems();
                            foreach($ecosystems as $ecosystem) {
                                echo "<option value='". $ecosystem."' ".($ecosystem==$product["ecosystem"]? 'selected':'').">". $ecosystem ."</option>";
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>


        <hr>
        <div style="padding: 0 10px;">
            <div class="form-group">
                <label>Short Description</label>
                <input type="text" class="form-control" name="short_desc" value="<?php echo $product["short_desc"] ?>">
            </div>
            <div class="form-group">
                <label>Description</label>
                <input type="text" class="form-control" name="product_desc" value="<?php echo $product["product_desc"] ?>">
            </div>
            <hr>
            <div class="form-group">
                <label>H1 Tag</label>
                <input type="text" class="form-control" name="tag1" value="<?php echo $product["tag1"] ?>">
            </div>
            <div class="form-group">
                <label>SEO Title</label>
                <input type="text" class="form-control" name="seo_title" value="<?php echo $product["seo_title"] ?>">
            </div>
            <div class="form-group">
                <label>SEO Description</label>
                <input type="text" class="form-control" name="seo_description" value="<?php echo $product["seo_description"] ?>">
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary" form="updateForm">Save information</button>
            </div>
        </div>
    </div>
</div>
</form>


<hr>
<h3>Reviews</h3>
<div class="reviews-section">
    <?php 
        include_once "../review/card-list.php";
    ?>
</div>

<?php
    include_once "modals.php";
?>

<script type="text/javascript">
    $(document).on("click",".change-prod-img-btn", function(){
        var type = $(this).attr("img-type");
        $("#change-image-form input[name='image_type']").val(type);
        if(cropper!=null) {
            $("#newImageInp").val("");
            $("#preview").hide();
            cropper.destroy();
        }
    });
    
    // invite email selection
    $(document).on("change", "input[name=emailSelect]", function(){
        var email = $(this).val();
        $(".sendInviteEmailInput").val(email);
    });

    var cropper, posX, posY;
    // image selection
    var previewImage = function(event) {
        var image = document.getElementById("preview");
        image.src = URL.createObjectURL(event.target.files[0]);
        
        if (cropper != null) {
            cropper.destroy();
        }
        cropper = new Cropper(image, {
            viewMode: 2,
            movable: false,
            zoomable: false,
            minCropBoxHeight: 30,
            minCropBoxWidth: 30,
            autoCropArea: 1,
            dragMode: "none",
            crop(event) {
                posX = event.detail.x;
                posY = event.detail.y;
            }
        });

        $(image).show();
    };

    // change image submit
    var submitNewImage = function(event) {
        event.preventDefault();
        console.log("handle submit");
        try {
            const form = $("#change-image-form");
            const postUrl = form.attr("action");

            cropper.getCroppedCanvas().toBlob(function(blob) {
                var postData = new FormData(document.getElementById("change-image-form"));
                postData.delete("image");
                postData.append("image", blob, "image.png");
                
                // submit form
                $.ajax({
                    url: postUrl,
                    type: "post",
                    data: postData,
                    processData: false,
                    contentType: false
                }).done(function(res,status,xhr) {
                    console.log(res);
                    location.href = window.location.href + "&success=Image changed!";
                }).fail(function(xhr, status, er) {
                    alert("Image too large");
                });
            });
        } catch (er) {
            console.log("ERROR", er);
        }
    }
</script>