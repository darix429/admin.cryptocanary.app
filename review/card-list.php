<div class="text-right">
    <!-- <input type="checkbox" id="hidedeleted" checked> <span><small>Hide deleted</small></span> -->
</div>
    <?php 
        require_once __CONTROLLERS . "rating.php";

        foreach($reviews as $review){ 
            $deleted = !empty($review["delete_time"]) ? true : false;
            $upvote = !empty($review["upvote_tally"])? $review["upvote_tally"] : 0;
            $downvote = !empty($review["downvote_tally"])? $review["downvote_tally"] : 0;

            $review_type = "entity";

            switch($list_from) {
                case 'user': {
                    $reviewer = $review["username"];
                    $reviewed = "<a class='color-0' href='/entity/?id=".$review['entity_id']."'>".$review['entity_name']."</a>";
                    $team_quality = $review["team_quality"];
                    $info_quality = $review["info_quality"];
                    $track_record = $review["track_record"];
                    $review_ratings = "ratings-entity.php";
                    break;
                }
                case 'entity': {
                    $reviewer = "<a class='color-0' href='/user/?id=".$review['user_id']."'>".$review['username']."</a>";
                    $reviewed = $review["entity_name"];
                    $team_quality = $review["team_quality"];
                    $info_quality = $review["info_quality"];
                    $track_record = $review["track_record"];
                    $review_ratings = "ratings-entity.php";
                    break;
                }
                case 'product': {
                    $reviewer = "". $review["username"];
                    $reviewed = "". $review["product_name"];
                    $review_type = "product";
                    $review_ratings = "ratings-product.php";
                    break;
                }
                default: {
                    $reviewer = $review["username"];
                    $reviewed = $review["entity_name"];
                    $team_quality = $review["team_quality"];
                    $info_quality = $review["info_quality"];
                    $track_record = $review["track_record"];
                    $review_ratings = "ratings-entity.php";
                    break;
                }
            }

            if ($list_from == "entity") {
            }
        ?>
            <div class="review-card <?php echo $deleted? 'deleted-bg': ''?>" <?php echo $deleted? 'hidden':'' ?>>
                <div class="flex-container card-head">
                    <div class="">
                        <strong><?php echo $reviewer ?></strong>
                        <i>rated</i>
                        <strong><?php echo $reviewed ?></strong>
                        <?php 
                            include $review_ratings;
                        ?>
                        <i>on</i>
                        <?php echo date_format(date_create($review['update_time']), "M d, Y") ?>
                    </div>
                    <div class="grow text-right">
                        <a href="/review/?id=<?php echo $review["review_id"] ?>&type=<?php echo $review_type ?>"><strong style="color: #00a2ee">Edit</strong></a> |
                        <a href="" class="delete-review-btn" data-toggle="modal" data-target="#deleteReview" id="<?php echo $review["review_id"] ?>" review-type="<?php echo $list_from ?>"><strong style="color: #f55; cursor:pointer;">Delete</strong></a>
                    </div>
                </div>
                <div class="card-body review">
                    <?php echo $review["review"] ?>
                </div>
                <div class="flex-container card-foot">
                    <div style="width: 100px;">Upvote: <strong><?php echo $upvote ?></strong></div>
                    <div>Downvote: <strong><?php echo $downvote ?></strong></div>
                </div>
                <div>
                    <?php
                        include "comment-card-list.php";
                    ?>
                </div>
            </div>
        <?php } ?>

        <?php if (count($reviews) == 0) { ?>
            <div class="review-card deleted-bg">
                This project has zero reviews
            </div>
        <?php } ?>