<?php 
$comments = json_decode($review["comments"]);
if (!empty($comments)) {
?>
    <br>
    <strong>comments:</strong>
    <?php
    foreach($comments as $comment) {
        $commenter = User::getInstance()->getUserById($comment->user_id);
        $avatar = User::getInstance()->getAvatarLink($commenter);
    ?>
        <div style="background: #f0f0f0; padding: 10px; font-size: 12px; margin-top: 5px; ">
            <div class="flex-container">
                <div>
                    <strong>
                    <a href="/user/?id=<?php echo $commenter["user_id"] ?>">
                        <?php echo $commenter["username"] ?>
                    </a>
                    </strong>
                    <i>on</i>
                    <?php echo gmdate("M-d-Y" , $comment->timestamp) ?>
                </div>
                <div class="text-right grow">
                    <button class="comment-action-btn" id="<?php echo $comment->id?>" review_id="<?php echo $review["review_id"] ?>" review_type="<?php echo $list_from ?>" action="remove_comment">delete</button>
                </div>
            </div>
            <div style="padding-left: 10px;">
                <?php echo $comment->comment ?>
            </div>
        </div>
    <?php
    }
            
}
?>