<?php
    $rating = $review["rating"];
?>
<form id="update-review-form" action="/product-review/index.php?action=update" method="post">
<input type="hidden" name="id" value="<?php echo $id ?>">
<div class="row">
    <div class="col">
        <span id="rating"></span>
        <input type="hidden" name="rating" value="<?php echo $rating ?>">
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $("#rating").rateYo({
            rating: <?php echo $rating ?>, 
            fullStar: true,
            ratedFill: "#007bff", 
            normalFill: "#ddd", 
            starWidth: "16px",
            onSet: function(rating, instance) {
                $("[name=rating]").val(rating);
            }
        });
    });
</script>
<br>
<div>
    <textarea name="review" class="form-control" style="height: 300px;"><?php echo $review["review"] ?></textarea>
</div>
</form>