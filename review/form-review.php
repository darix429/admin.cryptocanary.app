
<form action="/review/index.php?action=update" method="post" id='update-review-form'>
<input type="hidden" name="review_id" value="<?php echo $id ?>">
<div class="row">
    <div class="col">Team Quality</div>
    <div class="col">Info Quality</div>
    <div class="col">Track Record</div>
</div>
<div class="row">
    <div class="col">
        <span id="team_quality"></span>
        <input type="hidden" name="team_quality" value="<?php echo $team_quality ?>">
    </div>
    <div class="col">
        <span id="info_quality"></span>
        <input type="hidden" name="info_quality" value="<?php echo $info_quality ?>">
    </div>
    <div class="col">
        <span id="track_record"></span>
        <input type="hidden" name="track_record" value="<?php echo $track_record ?>">
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $("#team_quality").rateYo({
            rating: <?php echo $team_quality ?>, 
            fullStar: true,
            ratedFill: "#007bff", 
            normalFill: "#ddd", 
            starWidth: "16px",
            onSet: function(rating, instance) {
                $("[name=team_quality]").val(rating);
            }
        });
        $("#info_quality").rateYo({
            rating: <?php echo $info_quality ?>, 
            fullStar: true,
            ratedFill: "#007bff", 
            normalFill: "#ddd", 
            starWidth: "16px",
            onSet: function(rating, instance) {
                $("[name=info_quality]").val(rating);
            }
        });
        $("#track_record").rateYo({
            rating: <?php echo $track_record ?>, 
            fullStar: true,
            ratedFill: "#007bff",
            normalFill: "#ddd", 
            starWidth: "16px",
            onSet: function(rating, instance) {
                $("[name=track_record]").val(rating);
            }
        });
    });
</script>
<br>
<div>
    <textarea name="review" class="form-control" style="height: 300px;"><?php echo $review["review"] ?></textarea>
</div>
</form>