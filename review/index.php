<?php
    require_once '../config.php';
    require_once __CONTROLLERS . 'mail.php';   
    require_once __CONTROLLERS . 'review.php';   
    require_once __CONTROLLERS . 'rating.php';  
    require_once __CONTROLLERS . 'product-review.php';  

    
    $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
    $review_type = filter_input(INPUT_GET,'type',FILTER_SANITIZE_STRING);
    $action = filter_input(INPUT_GET,'action',FILTER_SANITIZE_STRING);

    if (!empty($action)) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        	Review::getInstance()->$action($_POST);
        else
            Review::getInstance()->$action();

		exit();
    }
?>

<?php include __SHARED . "head.php" ?>


<body>

<div id="header" class="bg-orange">
    <div class="container">
            <div><img src="https://cryptocanary.app/img/logo.svg" alt="logo" draggable=false></div>
            <div class="text-right">
                <?php include __SHARED . "usermenu.php" ?>
            </div>
        </div>
    </div>
</div>

<div id="nav">
    <div class="container"><?php include __SHARED . "nav.php" ?></div>
</div>

<div id="body">
    <div class="container">
    
    <?php
        
        $_view = './list.php';
        if (!empty($id)) {
            $_view = './review.php';
        }
        include $_view;
    
    ?>

    </div>
</div>

<div id="footer">

</div>

</body>    
</html>
<?php

include_once "../shared/modals.php";
include_once "../shared/end.php";

?>