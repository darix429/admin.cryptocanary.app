<?php 

    $filter = [
        "page"=>0,
        "size"=>15,
        "sort"=>'create_time',
        "direction"=>"desc",
        "search"=>""
    ];
    $page = filter_input(INPUT_GET,'page',FILTER_SANITIZE_STRING);
    $size = filter_input(INPUT_GET,'size',FILTER_SANITIZE_STRING);
    $sort = filter_input(INPUT_GET,'sort',FILTER_SANITIZE_STRING);
    $direction = filter_input(INPUT_GET,'direction',FILTER_SANITIZE_STRING);
    $search = filter_input(INPUT_GET,'search',FILTER_SANITIZE_STRING);

    
    $toggle_direction = $filter["direction"];
    $pagination = "/review?";
    if(!empty($page)) { 
        $page --;
        if ($page > 0) { $page = $page * $filter["size"]; }
        $filter['page'] = $page;
    }
    if(!empty($size)) { 
        $filter['size'] = $size;
        $pagination .= "&size=$size";
    }
    if(!empty($sort)) { 
        $filter['sort'] = $sort; 
        $pagination .= "&sort=$sort";
    }
    if(!empty($direction)) { 
        $filter['direction'] = $direction; 
        $pagination .= "&direction=$direction";

        if (strtolower($direction) == 'asc')
            $toggle_direction = "desc";
        else
            $toggle_direction = "asc";
    }
    if(!empty($search)) { 
        $filter['search'] = $search; 
    }

    function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close($ch);
        if($result !== FALSE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    $datasource = Review::getInstance()->getCollection($filter);
    $collection = $datasource["collection"];
?>
<h2>Reviews</h2>
<br>
<div class="row">
    <div class="col">
        <form action="/review">
            <div class="search-group">
                <input type="text" name="search" autocomplete="off" value="<?php echo $search ?>"><button type="submit">Search</button>
            </div>
        </form>
    </div>
</div>
<div class="text-center">
    <?php 
        if (!empty($search)) { 
            if (count($collection) > 0)
            echo "<i>". count($collection) ." results found for '<span class='color-1'>$search</span>'</i>";
            else 
            echo "<i>nothing found for '<span class='color-1'>$search</span>'</i>";

            echo !empty($search)? "<br><a href='/review' class='color-0'><strong>clear results</strong></a>": "";
        }  
    ?>
</div>
<br>
<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Project</th>
            <th>Reviewer</th>
            <th>Review</th>
            <th><a href="?sort=create_time&direction=<?php echo $toggle_direction ?>" draggable=false>Date</a></th>
            <th style="font-size: 12px"><a href="?sort=comment_date&direction=<?php echo $toggle_direction ?>" draggable=false>Comment Date</a></th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    <?php 
        foreach($collection as $data) {

            $review = $data["review"];
            if (strlen($review) > 100) {
                $review = substr($review, 0, 100) . "...";
            }

            $rating = (float) $data['overall_rating'];
            $confidence = number_format(((4-$rating)/4) * 100); 
            $color = 'very_legit';
            if ($confidence <= 25) { $color = 'very_shady'; }
            else if ($confidence > 25 && $confidence <=50 ) { $color = 'lean_shady'; }
            else if ($confidence > 50 && $confidence <=75 ) { $color = 'neutral'; }
            else if ($confidence > 75) { $color = 'lean_legit'; }
    ?>  
        <tr>
            <td><small><?php echo $data["review_id"] ?></small></td>
            <td>
                <small><a href="/entity/?id=<?php echo $data["entity_id"] ?>"> <strong><?php echo $data["entity_name"] ?></strong>
                <br><span class="color-gray">(<?php echo $data["symbol"] ?>)</span></a></small>
            </td>
            <td><small><a href="/user/?id=<?php echo $data["user_id"] ?>"><strong><?php echo $data["username"] ?></strong></a></small></td>
            <td class="color-gray" title="<?php echo $review ?>">
                <a href="/entity/?id=<?php echo $data["entity_id"] ?>#<?php echo $data["review_id"]?>"><small><i>"<?php echo $review ?>"</i></small></a>
            </td>
            <td style="width: 75px"><small><?php echo date_format(date_create($data['create_time']), "M d") ?></small></td>
            <td><small style="font-size: 12px"><?php echo (!empty($data["comment_date"]))? gmdate("M d",$data["comment_date"]) : "" ?></small></td>
            <td>
                 <a href="/review/?id=<?php echo $data["review_id"] ?>&type=entity"><button class="review-action-btn" title="Edit" style="font-size: 20px; color: #FFA401"><i class="fas fa-edit"></i></button></a>
                <button class="delete-review-btn" data-toggle="modal" data-target="#deleteReview" id="<?php echo $data["review_id"] ?>" title="Hard Delete" style="font-size: 20px; color: #C40606"><i class="fas fa-trash-alt"></i></button>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<hr>
<!-- pagination -->
<div class="pages text-center">
    <?php 

        $pagewidth = 10;
        $pagemax = ceil($datasource["max_size"] / $filter["size"]);
        $selected = ($page / $filter["size"]) + 1;
        $pagestart = 1;
        $pageend = $pagewidth;
        while ($pageend < $selected) {
            $pagestart = $pageend + 1;
            $pageend = ($pagestart + ($pagewidth-1));
            if ($pageend > $pagemax) {
                $pageend = $pagemax;
            }
        }


        if ($selected > $pagewidth) {
            $startlink = $pagination . "&page=1";
            echo "<a href='$startlink'><button title='Back to start'>&lt;&lt;</button></a>";
            $backlink = $pagination . "&page=" . ($pagestart-1);
            echo "<a href='$backlink'><button title='Back'>&lt;</button></a>  ";
        }

        if (empty($search)) {
            for($i = $pagestart; $i<=$pageend; $i++) {
                $page_link = $pagination . "&page=$i";
                echo "<a href='$page_link'><button ".(($selected == $i )? 'disabled' : '').">$i</button></a>";
            }
        }
        
        if ($pageend < $pagemax && empty($search)) {
            $forwardlink = $pagination . "&page=" . ($pageend+1);
            echo "  <a href='$forwardlink'><button title='Next'>&gt;</button></a>";
            $endlink = $pagination . "&page=$pagemax";
            echo "<a href='$endlink'><button title='To end'>&gt;&gt;</button></a>";
        }

    ?>
</div>
