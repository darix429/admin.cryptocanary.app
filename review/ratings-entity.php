
<span id="team_quality_<?php echo $review["review_id"] ?>" title="Team Quality (<?php echo number_format($team_quality) ?>)"></span>
<span id="info_quality_<?php echo $review["review_id"] ?>" title="Info Quality (<?php echo number_format($info_quality) ?>)"></span>
<span id="track_record_<?php echo $review["review_id"] ?>" title="Track Record (<?php echo number_format($track_record) ?>)"></span>
<script type="text/javascript">
    $(function () {
        $("#team_quality_<?php echo $review["review_id"] ?>").rateYo({
            rating: <?php echo $team_quality ?>, readOnly: true, ratedFill: "#007bff", normalFill: "#ddd", starWidth: "12px"
        });
        $("#info_quality_<?php echo $review["review_id"] ?>").rateYo({
            rating: <?php echo $info_quality ?>, readOnly: true, ratedFill: "#007bff", normalFill: "#ddd", starWidth: "12px"
        });
        $("#track_record_<?php echo $review["review_id"] ?>").rateYo({
            rating: <?php echo $track_record ?>, readOnly: true, ratedFill: "#007bff", normalFill: "#ddd", starWidth: "12px"
        });
    });
</script>