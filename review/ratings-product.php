
<span id="rating_<?php echo $review["review_id"] ?>" title="Rating (<?php echo number_format($review["rating"]) ?>)"></span>
<script type="text/javascript">
    $(function () {
        $("#rating_<?php echo $review["review_id"] ?>").rateYo({
            rating: <?php echo $review["rating"] ?>, readOnly: true, ratedFill: "#007bff", normalFill: "#ddd", starWidth: "12px"
        });
    });
</script>