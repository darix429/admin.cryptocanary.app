<?php
    require_once __CONTROLLERS . 'entity.php';   
    require_once __CONTROLLERS . 'user.php';    
    require_once __CONTROLLERS . 'product.php';    

    switch($review_type) {
        case "entity": {
            $review = Review::getInstance()->getReviewById($id);
            $entity = Entity::getInstance()->getEntityById($review["entity_id"]);
            $reviewed_id = $entity["entity_id"];
            $reviewed_name = $entity["entity_name"];
            $reviewed_img = Entity::getInstance()->getIconLink($entity);
            $reviewed_link = "/entity/?id=$reviewed_id";
            $form = "form-review.php";
            break;
        }
        case "product": {
            $review = ProductReview::getInstance()->getReviewById($id);
            $product = Product::getInstance()->getProductById($review["product_id"]);
            $reviewed_id = $product["product_id"];
            $reviewed_name = $product["product_name"];
            $reviewed_img = product_img_src . $review["product_id"]. "/".$review["product_img"];
            $reviewed_link = "/product/?id=$reviewed_id";
            $form = "form-product-review.php";
            break;
        }
        default: {
            $review = null;
            break;
        }
    }

    if (empty($review)) {
        header("Location: /review/");
    }

    $user = User::getInstance()->getUserById($review["user_id"]);
    $user_img = User::getInstance()->getAvatarLink($user);

    $rating = $review["rating"];

    if ($review_type == "entity") {
        $team_quality = $review["team_quality"];
        $info_quality = $review["info_quality"];
        $track_record = $review["track_record"];
    }
?>

<a href="/review" class="color-0"><strong>&#x2a1e; Back to review list</strong></a>

<div class="row">
    <div class="col">
    <h2>Review</h2>
    </div>
    <div class="col text-right">
        <button class="submit-btn" form="update-review-form">Save</button>
        <!-- <button class=" review-action-btn" id="<?php echo $review["review_id"] ?>" action="remove" >Remove Permanently</button> -->
        <button class="delete-review-btn" data-toggle="modal" data-target="#deleteReview" id="<?php echo $review["review_id"] ?>" review-type="<?php echo $review_type ?>">Remove Permanently</button>
    </div>
</div>
<hr>
<!-- RETURN MESSAGES -->
<?php if(isset($_GET['success'])) { ?>
    <a href="/review/?id=<?php echo $review['review_id'] ?>&type=<?php echo $review_type ?>" style="text-decoration: none;">
        <div class="status-msg very_legit">
            <?php echo "<strong>". $_GET['success'] ."</strong>" ?>
        </div>
    </a>
    <br>
<?php } ?>
<?php if(isset($_GET['error'])) { ?>
    <a href="/review/?id=<?php echo $review['review_id'] ?>&type=<?php echo $review_type ?>" style="text-decoration: none;">
        <div class="status-msg very_shady">
            <?php echo "<strong>". $_GET['error'] ."</strong>" ?>
        </div>
    </a>
    <br>
<?php } ?>
<!-- /RETURN MESSAGES -->

<div class="row">
    <div class="col">
        <h5><?php echo ucfirst($review_type) ?> Reviewed</h5>
        <a href="<?php echo $reviewed_link ?>">
        <img src="<?php echo $reviewed_img ?>" alt="reviewed_image" style="height: 100px; border: solid 2px #eee; border-radius: 50px;">
        <strong><?php echo $reviewed_name ?> <?php echo $review_type == "entity"? "(".$entity["symbol"].")":"" ?></strong>
        </a>
    </div>

    <div class="col">
        <h5>Reviewer</h5>
        <a href="/user/?id=<?php echo $user["user_id"]?>">
        <img src="<?php echo $user_img ?>" alt="user_image" style="height: 100px; border: solid 2px #eee; border-radius: 50px;">
        <strong><?php echo $user["username"] ?></strong> (<?php echo $user["score"] ?>)
        </a>
    </div>
</div>

<hr>
<h5>Rating</h5>
<?php

include_once $form;

?>