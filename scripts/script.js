
$(document).on("click", ".delete-review-btn", function() {
    var id = $(this).attr("id");
    var type = $(this).attr("review-type");
    var action = "/review/?action=remove";

    if (type == 'product') {
        var action = "/product-review/?action=remove";

    }

    $("#delete-review-form .review-id-disp").html(id);
    $("#delete-review-form [name=review_id]").val(id);
    $("#delete-review-form").attr("action", action);
    
});

$(document).ready(function(){

    // entity action
    $(".entity-action-btn").on("click", function(e) {
        e.preventDefault();
        $(this).attr('disabled', true);
        var btn = $(this);
        var id = btn.attr("id");
        var action = btn.attr("action");
        $.ajax({
            url: "/entity/index.php?action="+action,
            type: "post",
            data: {id: id}
        }).done(function(res,status,xhr) {
            location.reload();
        }).fail(function(xhr, status, er) {
            console.log(action,er);
            location.reload();
        });
    });

    // product action
    $(".product-action-btn").on("click", function(e) {
        e.preventDefault();
        $(this).attr('disabled', true);
        var btn = $(this);
        var id = btn.attr("id");
        var action = btn.attr("action");
        $.ajax({
            url: "/product/index.php?action="+action,
            type: "post",
            data: {id: id}
        }).done(function(res,status,xhr) {
            location.reload();
        }).fail(function(xhr, status, er) {
            console.log(action,er);
            // location.reload();
        });
    });

    // user action
    $(".user-action-btn").on("click", function(e) {
        e.preventDefault();
        $(this).attr('disabled', true);
        var btn = $(this);
        var id = btn.attr("id");
        var action = btn.attr("action");
        $.ajax({
            url: "/user/index.php?action="+action,
            type: "post",
            data: {id: id}
        }).done(function(res,status,xhr) {
            location.reload();
        }).fail(function(xhr, status, er) {
            console.log(action,er);
            location.reload();
        });
    });

    // comment action
    $(".comment-action-btn").on("click", function(e) {
        e.preventDefault();
        $(this).attr('disabled', true);
        var btn = $(this);
        var id = btn.attr("id");
        var review_type = btn.attr("review_type");
        var review_id = btn.attr("review_id");
        var action = btn.attr("action");
        var url = "/review/index.php?action="+action;
        if (review_type == "product") {
            url = "/product-review/index.php?action="+action;
        }
        
        $.ajax({
            url: url,
            type: "post",
            data: {id: id, review_id: review_id}
        }).done(function(res,status,xhr) {
            location.reload();
        }).fail(function(xhr, status, er) {
            console.log(action,er);
            location.reload();
        });
    });

    // review action
    $('#hidedeleted').change(function() {
        if ($(this).is(":checked")) {
            $(".deleted-bg").attr("hidden", true);
        } else {
            $(".deleted-bg").attr("hidden", false);
        }
    });
});
