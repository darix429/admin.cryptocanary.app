

<!-- bootstrap -->
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!-- rateyo -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>

<!-- fontawesome -->
<script src="https://kit.fontawesome.com/e94e85d680.js" crossorigin="anonymous"></script>

<!-- custom -->
<script src="../scripts/script.js?v=<?php echo time() ?>"></script>