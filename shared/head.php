
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>CryptoCanary Admin site</title>
	<meta name="description" content="CryptoCanary Admin site">
	<meta name="robots" content="noindex, nofollow">

	<!-- jquery -->
	<script type="text/javascript"	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type="text/javascript" src="/scripts/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/scripts/jquery-ui-1.12.1/jquery-ui.css">
	<link rel="stylesheet" href="/scripts/jquery-ui-1.12.1/jquery-ui.structure.min.css">
	<link rel="stylesheet" href="/scripts/jquery-ui-1.12.1/jquery-ui.theme.css">

	<!-- bootstrap -->
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- rateyo -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
		
	<!--custom-->
	<link rel="stylesheet" type="text/css" href="../styles/style.css?v=<?php echo time() ?>">
</head>