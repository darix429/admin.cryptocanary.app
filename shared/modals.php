
<!-- Delete Review -->
<div class="modal fade" id="deleteReview" tabindex="-1" role="dialog" aria-labelledby="deleteReview" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
    <form action="" method="post" id="delete-review-form">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteReviewLabel">Delete Review</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div>
        <input type="hidden" name='review_id'>
        <div class="form-group">
            Review <span class='review-id-disp'></span> will be deleted.
        </div>
        <div class="form-group notify-group"><label for="sendNotification"><input type="checkbox" name="sendNotification" id="sendNotification"> Notify Reviewer?</label></div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary delete-review-submit" form="delete-review-form">Delete</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- Import CSV -->
<div class="modal fade" id="importCSV" tabindex="-1" role="dialog" aria-labelledby="importCSV" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
    <form action="/product/index.php?action=importCSV" method="post" id="importCSV-form" enctype="multipart/form-data">
      <div class="modal-header">
        <h5 class="modal-title" id="importCSVLabel">Import From File</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group"><input type="file" name="csv" id="csv" required></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary change-image-submit" form="importCSV-form">Submit</button>
      </div>
    </form>
    </div>
  </div>
</div>
