<?php include_once "loading.php" ?>
<div class="nav">
    <div><i class="fas fa-chart-line"></i>&nbsp;<a href="/summary" draggable=false>Summary</a></div>
    <div><a href="/product" draggable=false>Products</a></div>
    <div><a href="/entity" draggable=false>Projects</a></div>
    <div><i class="fas fa-users"></i>&nbsp;<a href="/user" draggable=false>Users</a></div>
    <div><i class="far fa-comments"></i>&nbsp;<a href="/review" draggable=false>Reviews</a></div>
</div>