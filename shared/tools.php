<div class="widget text-right">

    <div class="menu" id='menu-tools' style='display: none'>
        <ul>
            <li><a href="/tool/product/pending.descriptions.check.php"><button>Pending Products > Descriptions checker</button></a></li>
        </ul>
    </div>
    <button class="menu-btn" target='menu-tools'><i class="fas fa-tools"></i> TOOLS</button>
</div>


<script>

$(document).on("click", ".menu-btn", function() {
    var target = $(this).attr("target");
    $("#" + target).toggle();
})

</script>