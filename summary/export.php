<?php
require_once '../config.php';

$STATS = $_SESSION["STATS"];

if (!empty($STATS)) {

    for($i = 0 ; $i < count($STATS) ; $i++) {
        $STATS[$i]['date'] = date("M d, Y" , strtotime($STATS[$i]['date']));
    }

    require_once __CONTROLLERS . 'util.php';
    array_unshift($STATS, ["Date", "Projects", "Users", "Reviews"]);
    Util::getInstance()->exportCsv($STATS, "export.csv", ",");
} else {
    echo "Nothing to Export";
}