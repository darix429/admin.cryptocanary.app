<?php
	include_once '../config.php';
?>

<?php include __SHARED . "head.php" ?>

<!--charts-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

<body>
    
	<div id="header" class="bg-orange">
		<div class="container">
				<div><img src="https://cryptocanary.app/img/logo.svg" alt="logo" draggable=false></div>
				<div class="text-right">
					<?php include __SHARED . "usermenu.php" ?>
				</div>
			</div>
		</div>
	</div>

	<div id="nav">
		<div class="container"><?php include __SHARED . "nav.php" ?></div>
	</div>

	<div id="body">
		<div class="container">
            <?php
                $_view = './summary.php';
                

                include $_view;
            ?>
		</div>
	</div>

	<div id="footer">

	</div>

</body>    
</html>
<?php

include_once "../shared/end.php";

?>