<?php
    // require_once __CONTROLLERS . 'user.php';
    // require_once __CONTROLLERS . 'entity.php';
    require_once __CONTROLLERS . 'review.php';
    require_once __CONTROLLERS . 'product-review.php';
    require_once __CONTROLLERS . 'util.php';

    $Util = Util::getInstance();
    $Entity = $Util->getEntityStats();
    $User = $Util->getUserStats();
    $Reviews = Review::getInstance()->getAll();
    $Product = $Util::getProductStats();
    $ProductReviews = ProductReview::getInstance()->getAll();

    $start = filter_input(INPUT_POST,'from',FILTER_SANITIZE_STRING);
    $end = filter_input(INPUT_POST,'to',FILTER_SANITIZE_STRING);

    if (empty($start))
    $start = '2018-08-17';
    if (empty($end)) 
    $end = date('Y-m-d');

    unset($_POST['from']);
    unset($_POST['to']);
    
    $entity = $Util->getEntityDateEntries($start, $end)["filtered"];
    $entityAll = $Util->getEntityDateEntries($start, $end)["all"];
    $user = $Util->getUserDateEntries($start, $end);
    $review = $Util->getReviewDateEntries($start, $end);
    $product = $Util->getProductDateEntries($start, $end);
    $product_review = $Util->getProductReviewDateEntries($start, $end);
    
    foreach(array_merge($entity,$user,$review,$product,$product_review) as $entry) {
        if(!isset($result[$entry['date']])) {
            $result[$entry['date']] = $entry;
        } else {
            foreach($entry as $key=>$value) {
                $result[$entry['date']][$key] = $value;
            }
        }
        if (!isset($result[$entry['date']]['entity'])) {
            $result[$entry['date']]['entity'] = 0;
        }
        if (!isset($result[$entry['date']]['user'])) {
            $result[$entry['date']]['user'] = 0;
        }
        if (!isset($result[$entry['date']]['review'])) {
            $result[$entry['date']]['review'] = 0;
        }
        if (!isset($result[$entry['date']]['product'])) {
            $result[$entry['date']]['product'] = 0;
        }
        if (!isset($result[$entry['date']]['product_review'])) {
            $result[$entry['date']]['product_review'] = 0;
        }
    }
    ksort($result);
    $STATS = array();
    foreach($result as $entry) {
        array_push($STATS, $entry);
    }

    $userCount = $entityCount = $reviewCount = $productCount = $productReviewCount = 0;
    foreach($entityAll as $entity) {
        $entityCount += $entity["entity"];
    }
    foreach($STATS as $stat) {
        $userCount += $stat["user"];
        $reviewCount += $stat["review"];
        $productCount += $stat["product"];
        $productReviewCount += $stat["product_review"];
    }


    $entityAccumEnd = $Util->getAccumTotalFromDate("entity", $end);
    $userAccumEnd = $Util->getAccumTotalFromDate("user", $end);
    $productAccumEnd = $Util->getAccumTotalFromDate("product", $end);
    $reviewAccumEnd = $Util->getAccumTotalFromDate("review", $end);
    $productReviewAccumEnd = $Util->getAccumTotalFromDate("product_review", $end);

    
    $entityGrowth = !empty($entityAccumEnd)? ceil($entityCount / $entityAccumEnd * 100 ) : 0;
    $userGrowth = !empty($userAccumEnd)? ceil($userCount / $userAccumEnd * 100) : 0;
    $reviewGrowth = !empty($reviewAccumEnd)? ceil($reviewCount / $reviewAccumEnd * 100) : 0;
    $productGrowth = !empty($productAccumEnd)? ceil($productCount / $productAccumEnd * 100) : 0;
    $productReviewGrowth = !empty($productReviewCount)? ceil($productReviewCount / $productReviewCount * 100) : 0;


?>
<h2>Summary</h2>
<hr>
<div class="row">
    <div class="col text-center summary-header">
        <h4>Projects</h4>
        <div>
            <a href="/entity"><span class='total color-1'><?php echo $Entity['total'] ?></span></a><br>
            <small>TOTAL</small>
        </div>
        <div class="summary-details">
            <div class="row">
                <div class="col text-right"><small>APPROVED</small>:</div><div class="col text-left color-1"><strong><?php echo $Entity['total']-($Entity['pending'] + $Entity['deleted']) ?></strong></div>
            </div>
            <div class="row">
                <div class="col text-right"><small>PENDING</small>:</div><div class="col text-left <?php echo $Entity['pending']>0? 'color-1':'color-gray' ?>"><strong><?php echo $Entity['pending'] ?></strong></div>
            </div>
            <div class="row">
                <div class="col text-right"><small>DELETED</small>:</div><div class="col text-left <?php echo $Entity['deleted']>0? 'color-3':'color-gray' ?>"><strong><?php echo $Entity['deleted'] ?></strong></div>
            </div>
        </div>  
    </div>
    <div class="col text-center summary-header">
        <h4>Products</h4>
        <div>
            <a href="/product"><span class='total color-1'><?php echo $Product['total'] ?></span></a><br>
            <small>TOTAL</small>
        </div>
        <div class="summary-details">
            <div class="row">
                <div class="col text-right"><small>APPROVED</small>:</div><div class="col text-left color-1"><strong><?php echo $Product['total']-($Product['pending'] + $Product['deleted']) ?></strong></div>
            </div>
            <div class="row">
                <div class="col text-right"><small>PENDING</small>:</div><div class="col text-left <?php echo $Product['pending']>0? 'color-1':'color-gray' ?>"><strong><?php echo $Product['pending'] ?></strong></div>
            </div>
            <div class="row">
                <div class="col text-right"><small>DELETED</small>:</div><div class="col text-left <?php echo $Product['deleted']>0? 'color-3':'color-gray' ?>"><strong><?php echo $Product['deleted'] ?></strong></div>
            </div>
        </div>
    </div>
    <div class="col text-center summary-header">
        <h4>Users</h4>
        <div>
            <a href="/user"><span class='total color-1'><?php echo $User['total'] ?></span></a><br>
            <small>TOTAL</small>
        </div>
        <div class="summary-details">
            <div class="row">
                <div class="col text-right"><small>BLOCKED</small>:</div><div class="col text-left color-gray"><strong><?php echo $User['blocked'] ?></strong></div>
            </div>
            <div class="row">
                <div class="col text-right"><small>DELETED</small>:</div><div class="col text-left <?php echo $User['deleted']>0? 'color-3':'color-gray' ?>"><strong><?php echo $User['deleted'] ?></strong></div>
            </div>
        </div>
    </div>
    <div class="col text-center summary-header">
        <h4>Reviews</h4>
        <div>
            <a href="/review"><span class='total color-1'><?php echo count($Reviews) ?></span></a><br>
            <small>TOTAL</small>
        </div>
    </div>
    <div class="col text-center summary-header">
        <h4>Product Reviews</h4>
        <div>
            <a href="/product"><span class='total color-1'><?php echo count($ProductReviews) ?></span></a><br>
            <small>TOTAL</small>
        </div>
    </div>
</div>

<hr>
<div class="row">
    <div class="col"><small>MoM Growth: </small>&nbsp;<strong class="color-2"><?php echo !empty($entityGrowth)? $entityGrowth . "%":"N/A" ?></strong></div>
    <div class="col"><small>MoM Growth: </small>&nbsp;<strong class="color-2"><?php echo !empty($productGrowth)? $productGrowth . "%":"N/A" ?></strong></div>
    <div class="col"><small>MoM Growth: </small>&nbsp;<strong class="color-2"><?php echo !empty($userGrowth)? $userGrowth . "%":"N/A" ?></strong></div>
    <div class="col"><small>MoM Growth: </small>&nbsp;<strong class="color-2"><?php echo !empty($reviewGrowth)? $reviewGrowth . "%":"N/A" ?></strong></div>
    <div class="col"><small>MoM Growth: </small>&nbsp;<strong class="color-2"><?php echo !empty($productReviewGrowth)? $productReviewGrowth . "%":"N/A" ?></strong></div>
</div>
<hr>
<div>
    <small>Engagement Metric:</small> <strong><?php echo $entityCount + $productCount + $userCount + $reviewCount + $productReviewCount ?></strong>
</div>
<hr>
<div class="flex-container">
    <div class="col">
        <form action="" method="POST" autocomplete="off">
            <span>From: </span> 
            <input type="text" name="from" id="dateFrom" placeholder="yyyy-mm-dd" value="<?php echo $start ?>">
            <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span>To: </span> 
            <input type="text" name="to" id="dateTo" placeholder="yyyy-mm-dd" value="<?php echo $end ?>">
            <button type="submit">Set range</button>
        </form>
    </div>
</div>

<hr>

<div class="charts">
    <canvas id="chart"></canvas>
</div>

<hr>

<div class="tables">
    <div class="text-right">
        <form action="/summary/export.php" method="POST">
            <button type="submit" style='border: none; background: none; text-decoration: underline; font-weight: 600;' class='color-0'>Export to CSV</button>
        </form>
    </div>
    <table class="table" style="table-layout: fixed;">
        <thead>
            <tr>
                <th>Date</th>
                <th class='text-center'>Projects</th>
                <th class='text-center'>Products</th>
                <th class='text-center'>Users</th>
                <th class='text-center'>Reviews</th>
                <th class='text-center'>Product Reviews</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        
        $reversedStats = array_reverse($STATS);
        foreach($reversedStats as $data) { ?>
            <tr>
                <td><?php echo date("M d, Y", strtotime($data['date'])) ?></td>
                <td class='text-center'><span class='<?php echo $data['entity']==0? 'color-light-gray':'strong' ?>'><?php echo $data['entity'] ?></span></td>
                <td class='text-center'><span class='<?php echo $data['product']==0? 'color-light-gray':'strong' ?>'><?php echo $data['product'] ?></span></td>
                <td class='text-center'><span class='<?php echo $data['user']==0? 'color-light-gray':'strong' ?>'><?php echo $data['user'] ?></span></td>
                <td class='text-center'><span class='<?php echo $data['review']==0? 'color-light-gray':'strong' ?>'><?php echo $data['review'] ?></span></td>
                <td class='text-center'><span class='<?php echo $data['product_review']==0? 'color-light-gray':'strong' ?>'><?php echo $data['product_review'] ?></span></td>
            </tr>

        <?php } ?>
        </tbody>
    </table>
</div>

<hr>


<script>
  $( function() {
    $( "#dateFrom" ).datepicker({
        dateFormat: "yy-mm-dd",
        minDate: new Date(2018, 9, 17),
        maxDate: "-1m"
    });
    $( "#dateTo" ).datepicker({
        dateFormat: "yy-mm-dd",
        minDate: new Date(2018, 9, 17),
        maxDate: "0"
    });
  });

    var ctx = document.getElementById('chart');
    var projects=[], users=[], reviews=[], dates=[], products=[], product_reviews=[];
    var stats = <?php echo json_encode($STATS) ?>;
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    
    var proj_sum = 0, user_sum = 0, review_sum = 0, prod_sum = 0, prod_review_sum = 0;
    stats.forEach(data => {
        var date = new Date(data['date'])
        date = monthNames[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
        dates.push(date);

        proj_sum += parseInt(data['entity']);
        prod_sum += parseInt(data['product']);
        user_sum += parseInt(data['user']);
        review_sum += parseInt(data['review']);
        prod_review_sum += parseInt(data['product_review']);
        
        projects.push(proj_sum);
        products.push(prod_sum);
        users.push(user_sum);
        reviews.push(review_sum);
        product_reviews.push(prod_review_sum);
    });


    var chart_data = {
        labels: dates,
        datasets: [
            {
                label: "projects",
                fill: false,
                lineTension: 0.02,
                backgroundColor: "#00a2ee",
                borderColor: "#00a2ee",
                pointHoverBackgroundColor: "#00a2ee",
                pointHoverBorderColor: "#00a2ee",
                data: projects
            },     
            {
                label: "products",
                fill: false,
                lineTension: 0.02,
                backgroundColor: "#ffa500",
                borderColor: "#ffa500",
                pointHoverBackgroundColor: "#ffa500",
                pointHoverBorderColor: "#ffa500",
                data: products
            },     
            {
                label: "users",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "#00ee4c",
                borderColor: "#00ee4c",
                pointHoverBackgroundColor: "#00ee4c",
                pointHoverBorderColor: "#00ee4c",
                data: users
            },
            {
                label: "reviews",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "#ee002b",
                borderColor: "#ee002b",
                pointHoverBackgroundColor: "#ee002b",
                pointHoverBorderColor: "#ee002b",
                data: reviews
            },
            {
                label: "product reviews",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "#7f00ff ",
                borderColor: "#7f00ff ",
                pointHoverBackgroundColor: "#7f00ff ",
                pointHoverBorderColor: "#7f00ff ",
                data: product_reviews
            }
        ]
    };
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: chart_data,
        options: {}
    });
</script>