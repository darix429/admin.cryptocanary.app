
<a href="/user"><strong>Back</strong></a>
<div class="row">
    <div class="col">
    <h2>Add new user</h2>
    </div>
    <div class="col text-right">
        <button class="btn bg-1 color-white" form="new-user-form">Submit</button>
    </div>
</div>

<br>


<?php if(isset($_GET['success']) || isset($_GET['error'])) { ?>
    <a href="/user/?new" style="text-decoration: none;">
        <div class="status-msg <?php echo isset($_GET['success'])? 'very_legit':'very_shady' ?>">
            <?php echo isset($_GET['success'])? "<strong>User created successfully</strong>": "<strong>Error:</strong> ".$_GET['error'] ?>
        </div>
    </a>
    <br>
<?php } ?>

<div class="add-form">
<form id="new-user-form" action="/user/index.php?action=create" method="post" autocomplete="off">
    <div class="form-group">
        <label>Email: *</label>
        <input class="form-control" type="email" name="email" minlength=6 required>
        <small>(minimum 6 characters)</small>
    </div>
    <div class="form-group">
        <label>Name:</label>
        <input class="form-control" type="text" name="name" minlength=3>
        <small>(minimum 3 characters)</small>
    </div>
    <div class="form-group">
        <label>Username: *</label>
        <input class="form-control" type="text" name="username" minlength=3 required>
        <small>(minimum 3 characters)</small>
    </div>
    <div class="form-group">
        <label>Password: *</label>
        <input class="form-control" type="password" name="password" minlength=6 required>
        <small>(minimum 6 characters)</small>
    </div>
</form>
</div>