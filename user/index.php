<?php

    require_once '../config.php';
    require_once __CONTROLLERS . 'user.php';   
    require_once __CONTROLLERS . 'mailchimp.php';   

    
    $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
    $action = filter_input(INPUT_GET,'action',FILTER_SANITIZE_STRING);
    if (!empty($action)) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        User::getInstance()->$action($_POST);
        else
        User::getInstance()->$action();
    }
    
?>

<?php include __SHARED . "head.php" ?>


<body>
    
	<div id="header" class="bg-orange">
		<div class="container">
				<div><img src="https://cryptocanary.app/img/logo.svg" alt="logo" draggable=false></div>
				<div class="text-right">
					<?php include __SHARED . "usermenu.php" ?>
				</div>
			</div>
		</div>
	</div>

	<div id="nav">
		<div class="container"><?php include __SHARED . "nav.php" ?></div>
	</div>

	<div id="body">
		<div class="container">
        
        <?php
            
            $_view = './list.php';
            if (!empty($id)) {
                $_view = './user.php';
            } else if (isset($_GET["new"])) {
                $_view = './add.php';
            }
            include $_view;
        
        ?>

        </div>
    </div>

    <div id="footer">

    </div>

</body>    
</html>
<?php

include_once "../shared/modals.php";
include_once "../shared/end.php";

?>