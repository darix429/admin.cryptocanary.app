<?php 

$filter = [
    "page"=>0,
    "size"=>25,
    "sort"=>'create_time',
    "direction"=>"desc",
    "search"=>""
];

$page = filter_input(INPUT_GET,'page',FILTER_SANITIZE_STRING);
$size = filter_input(INPUT_GET,'size',FILTER_SANITIZE_STRING);
$sort = filter_input(INPUT_GET,'sort',FILTER_SANITIZE_STRING);
$direction = filter_input(INPUT_GET,'direction',FILTER_SANITIZE_STRING);
$search = filter_input(INPUT_GET,'search',FILTER_SANITIZE_STRING);

    
$toggle_direction = $filter["direction"];
$pagination = "/user?";
if(!empty($page)) { 
    $page --;
    if ($page > 0) { $page = $page * $filter["size"]; }
    $filter['page'] = $page;
}
if(!empty($size)) { 
    $filter['size'] = $size;
    $pagination .= "&size=$size";
}
if(!empty($sort)) { 
    $filter['sort'] = $sort; 
    $pagination .= "&sort=$sort";}
if(!empty($direction)) { 
    $filter['direction'] = $direction; 
    $pagination .= "&direction=$direction";

    if (strtolower($direction) == 'asc')
        $toggle_direction = "desc";
    else
        $toggle_direction = "asc";
} else {
    $toggle_direction = ($toggle_direction == 'asc')? 'desc': 'asc';
}
if(!empty($search)) { 
    $filter['search'] = $search; 
}

$datasource = User::getInstance()->getCollection($filter);
$collection = $datasource["collection"];


?>


<h2>Users</h2>
<br>
<div class="row">
    <div class="col">
        <form action="/user">
            <div class="search-group">
                <input type="text" name="search" autocomplete="off" value="<?php echo $search ?>"><button type="submit">Search</button>
            </div>
            
        </form>
    </div>
    <div class="col text-right">
        <a href="/user/?new"><button>Add User</button></a>
    </div>
</div>
<div class="text-center">
    <?php 
        if (!empty($search)) { 
            if (count($collection) > 0)
            echo "<i>". count($collection) ." results found for '<span class='color-1'>$search</span>'</i>";
            else 
            echo "<i>nothing found for '<span class='color-1'>$search</span>'</i>";

            echo !empty($search)? "<br><a href='/user' class='color-0'><strong>clear results</strong></a>": "";
        }  
    ?>
</div>
<br>
<table class="table">
    <thead>
        <tr>
            <th><a href="?sort=user_id&direction=<?php echo $toggle_direction ?>" draggable=false>ID</a></th>
            <th>Avatar</th>
            <th><a href="?sort=username&direction=<?php echo $toggle_direction ?>" draggable=false>Username</a></th>
            <th><a href="?sort=email&direction=<?php echo $toggle_direction ?>" draggable=false>Email</a></th>
            <th class="text-center"><a href="?sort=score&direction=<?php echo $toggle_direction ?>" draggable=false>Canary chirps</a></th>
            <th><a href="?sort=create_time&direction=<?php echo $toggle_direction ?>" draggable=false>Date joined</a></th>
            <th class="text-center">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($collection as $data) { 
            $status = "";
            if (!empty($data['delete_time'])) {
                $status = "Deleted";
            }
        ?>
            <tr>
                <td><?php echo $data['user_id'] ?></td>
                <td><img src="<?php echo User::getInstance()->getAvatarLink($data) ?>" alt="avatar"></td>
                <td><a href="/user/?id=<?php echo $data['user_id'] ?>" draggable=false><strong><?php echo $data['username'] ?></strong></a></td>
                <td><a href="mailto:<?php echo $data['email'] ?>"><?php echo $data['email'] ?></a></td>
                <td class="text-center"><strong><?php echo $data['score'] ?></strong></td>
                <td><?php echo date_format(date_create($data['create_time']), "M d, Y") ?></td>
                <td class="text-center">
                    <?php if($status == "Deleted") { ?>
                        <!-- <i class="restore-btn user-action-btn" id="<?php echo $data['user_id'] ?>" action="restore" title="Restore"></i>
                        <i class="permanent-x-btn user-action-btn" id="<?php echo $data['user_id'] ?>" action="permanent_remove" title="Hard delete"></i> -->
                        <button class="user-action-btn" id="<?php echo $data['user_id'] ?>" action="restore" title="Restore" style="font-size: 20px; color: #05BEA6"><i class="fas fa-redo"></i></button>
                        <button class="user-action-btn" id="<?php echo $data['user_id'] ?>" action="permanent_remove" title="Hard delete" style="font-size: 20px; color: #C40606"><i class="fas fa-trash-alt"></i></button>
                    <?php } else { ?>
                        <!-- <i class="x-btn user-action-btn" id="<?php echo $data['user_id'] ?>" action="remove" title="Soft delete"></i> -->
                        <button class="user-action-btn" id="<?php echo $data['user_id'] ?>" action="remove" title="Soft delete" style="font-size: 20px; color: #E76E54"><i class="fas fa-backspace"></i></button>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>


</table>

<div class="pages text-center">
    <?php 
        $pagewidth = 10;
        $pagemax = ceil($datasource["max_size"] / $filter["size"]);
        $selected = ($page / $filter["size"]) + 1;
        $pagestart = 1;
        $pageend = $pagewidth;
        while ($pageend < $selected) {
            $pagestart = $pageend + 1;
            $pageend = ($pagestart + ($pagewidth-1));
            if ($pageend > $pagemax) {
                $pageend = $pagemax;
            }
        }


        if ($selected > $pagewidth) {
            $startlink = $pagination . "&page=1";
            echo "<a href='$startlink'><button title='Back to start'>&lt;&lt;</button></a>";
            $backlink = $pagination . "&page=" . ($pagestart-1);
            echo "<a href='$backlink'><button title='Back'>&lt;</button></a>  ";
        }

        if (empty($search)) {
            for($i = $pagestart; $i<=$pageend; $i++) {
                $page_link = $pagination . "&page=$i";
                echo "<a href='$page_link'><button ".(($selected == $i )? 'disabled' : '').">$i</button></a>";
            }
        }

        if ($pageend < $pagemax && empty($search)) {
            $forwardlink = $pagination . "&page=" . ($pageend+1);
            echo "  <a href='$forwardlink'><button title='Next'>&gt;</button></a>";
            $endlink = $pagination . "&page=$pagemax";
            echo "<a href='$endlink'><button title='To end'>&gt;&gt;</button></a>";
        }
    ?>
</div>
