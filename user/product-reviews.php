<?php
    require_once __CONTROLLERS . "product-review.php";
    $reviews = ProductReview::getInstance()->getReviewsByUserId($id);
?>
<h3>Product Reviews (<?php echo count($reviews) ?>)</h3>
<div class="activity-section">
    <?php
        include_once "../product-review/card-list.php";
    ?>
</div>