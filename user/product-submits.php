<?php
    require_once __CONTROLLERS . "product.php";
    $products = Product::getInstance()->getCollectionByUserId($id);
?>
<h3>Submitted Products (<?php echo count($products) ?>)</h3>
<br>
<div class="activity-section">
    <?php foreach($products as $product) { 
        $deleted = !empty($product["delete_time"]) ? true : false;
        $approved = $product['approved_flag'] ? true : false;
    ?>
        <div>
            <div class="row">
                <div class="col">
                    <?php echo $user['username']?>
                    submitted <a class="color-0" href="/product/?id=<?php echo $product['product_id'] ?>"><strong><?php echo $product['product_name'] ?></strong></a> 
                    on <i> <?php echo date_format(date_create($product['create_time']), "M d, Y") ?></i>
                </div>
                <div class="col text-right">
                    <?php if (!$approved) { ?>
                        <strong><a href="" class="product-action-btn" action="approve" id="<?php echo $product['product_id'] ?>">Approve</a></strong>
                    <?php } else { ?>
                        <a href="" class="product-action-btn <?php echo !$deleted? 'deleted':'' ?>" action="<?php echo $deleted? 'restore':'remove' ?>" id="<?php echo $product['product_id'] ?>">
                            <strong><?php echo $deleted? "Restore":"Remove" ?></strong>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <br>
    <?php } ?>
    <?php if (count($products) == 0) { ?>
        <div class="review-card deleted-bg">
            This user did not submit any product
        </div>
    <?php } ?>
</div>