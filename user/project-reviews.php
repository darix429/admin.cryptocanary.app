<?php
    require_once __CONTROLLERS . "review.php";
    $reviews = Review::getInstance()->getReviewsByUserId($user['user_id']);
?>
<h3>Project Reviews (<?php echo count($reviews) ?>)</h3>
<div class="activity-section">
    <?php
        $list_from = 'user';
        include_once "../review/card-list.php";

    ?>
</div>