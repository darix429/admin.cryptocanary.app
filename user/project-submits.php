<?php
    require_once __CONTROLLERS . "entity.php";
    $entities = Entity::getInstance()->getEntitiesByUserId($user['user_id']);
?>
<h3>Submitted Projects (<?php echo count($entities) ?>)</h3>
<br>
<div class="activity-section">
    <?php foreach($entities as $entity) { 
        $deleted = !empty($entity["delete_time"]) ? true : false;
        $approved = $entity['approved_flag'] ? true : false;
    ?>
        <div>
            <div class="row">
                <div class="col">
                    <?php echo $user['username']?>
                    submitted <a class="color-0" href="/entity/?id=<?php echo $entity['entity_id'] ?>"><strong><?php echo $entity['entity_name'] ?></strong></a> 
                    on <i> <?php echo date_format(date_create($entity['create_time']), "M d, Y") ?></i>
                </div>
                <div class="col text-right">
                    <?php if (!$approved) { ?>
                        <strong><a href="" class="entity-action-btn" action="approve" id="<?php echo $entity['entity_id'] ?>">Approve</a></strong>
                    <?php } else { ?>
                        <a href="" class="entity-action-btn <?php echo !$deleted? 'deleted':'' ?>" action="<?php echo $deleted? 'restore':'remove' ?>" id="<?php echo $entity['entity_id'] ?>">
                            <strong><?php echo $deleted? "Restore":"Remove" ?></strong>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <br>
    <?php } ?>
    <?php if (count($entities) == 0) { ?>
        <div class="review-card deleted-bg">
            This user did not submit any project
        </div>
    <?php } ?>
</div>