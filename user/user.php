<?php
    $user = User::getInstance()->getUserById($id);

    if (empty($user)) {
        header("Location: /user/");
    }

    $status = "";
    if (!empty($user["delete_time"])) {
        $status = "Deleted";
    }
?>

<div class="row">
    <div class="col">
        <a href="/user" class="color-0"><strong>&#x2a1e; Back to user list</strong></a>
    </div>
    <div class="col text-right">
        <?php if($status == 'Deleted') { ?>
            <button class="user-action-btn" id="<?php echo $user['user_id'] ?>" action="restore">Restore</button>
            <button class="user-action-btn" id="<?php echo $user['user_id'] ?>" action="permanent_remove">Remove permanently</button>
        <?php } else { ?>
            <button class="user-action-btn" id="<?php echo $user['user_id'] ?>" action="remove">Delete</button>
            <a href="<?php echo WEBSITE_URL . "user/?id=" . $user['user_id'] ?>" target="_blank"><button>View profile on website</button></a>
        <?php } ?>
    </div>
</div>
<hr>
<h3>User information</h3><br>
<div class="info-section flex-container">
    <div class="text-center" style="width: 150px;">
        <img style="height: 85px" src="<?php echo User::getInstance()->getAvatarLink($user) ?>" alt="avatar">
    </div>
    <div class="grow">
        <div class="row">
            <div class="col-sm-2">Nickname</div>
            <div class="col"><strong><?php echo $user['username'] ?></strong></div>
        </div>
        <div class="row">
            <div class="col-sm-2">Email</div>
            <div class="col"><a href="mailto:<?php echo $user['email'] ?>"><?php echo $user['email'] ?></a></div>
        </div>
        <div class="row">
            <div class="col-sm-2">Chirps</div>
            <div class="col"><strong><?php echo $user['score']?></strong></div>
        </div>
        <div class="row" style="align-items: center; ">
            <div class="col-sm-2">Joined Date</div>
            <div class="col"><?php echo date_format(date_create($user['create_time']), "M d, Y") ?></div>
        </div>
        <div class="row">
            <div class="col-sm-2">Bio</div>
            <div class="col"><i><?php echo !empty($user['bio'])? $user['bio'] : 'N/A' ?></i></div>
        </div>
        <div class="row">
            <div class="col-sm-2">Blocked</div>
            <div class="col"><input type="checkbox" class="checkbox_toggle" action="<?php echo !$user['block_flag']? 'block':'unblock' ?>" user_id="<?php echo $user['user_id'] ?>" <?php echo $user['block_flag']? 'checked': '' ?>></div>
        </div>
        <div class="row">
            <div class="col-sm-2">Deleted</div>
            <div class="col"><input type="checkbox" class="checkbox_toggle" action="<?php echo empty($user['delete_time'])? 'remove':'restore' ?>" user_id="<?php echo $user['user_id'] ?>" <?php echo !empty($user['delete_time'])? 'checked': '' ?>></div>
        </div>
    </div>
</div>

<br>
    <hr>
        <div class="text-center">
            <a class="color-0" href="/user/?id=<?php echo $user['user_id'] ?>">Project reviews</a> | 
            <a class="color-0" href="/user/?id=<?php echo $user['user_id'] ?>&tab=1">Product reviews</a> | 
            <a class="color-0" href="/user/?id=<?php echo $user['user_id'] ?>&tab=2">Submitted projects</a> |
            <a class="color-0" href="/user/?id=<?php echo $user['user_id'] ?>&tab=3">Submitted products</a>
        </div>
    <hr>
<br>

<?php 
    $tab = filter_input(INPUT_GET,'tab',FILTER_SANITIZE_STRING);
    switch ($tab) {
        case "1" : $sub = './product-reviews.php'; break;
        case "2" : $sub = './project-submits.php'; break;
        case "3" : $sub = './product-submits.php'; break;
        default : $sub = './project-reviews.php'; break;
    }
    include_once $sub;
?>


<!-- JScripts -->
<script type="text/javascript">
    $(".checkbox_toggle").on("change",function() {   
        var checked = $(this).is(":checked");
        var action = $(this).attr("action");
        var id = $(this).attr("user_id");

        $.ajax({
            url: "/user/index.php?action="+action,
            type: "post",
            data: {id: id}
        }).done(function(res,status,xhr) {
            location.reload();
        }).fail(function(xhr, status, er) {
            alert(er);
        });
    });
</script>